function toggleSelector(){
	if ($("#status").val() === "5" || $("#status").val() === "12"){
		$("#cmb_res_id").show();
	} else {
		$("#cmb_res_id").hide();
		$("#cmb_res_id").val("0");
	}
}
function redirectWorkPackageListPage(){
	window.location.replace("work-package-list.html?apikey=" + getParameterByName("apikey"));
}

function redirectDashboardPage(){
	window.location.replace("/dashboard.html?apikey=" + getParameterByName("apikey"));
}

function redirectWorkloadPage(){
	window.location.replace("workpackage-workload.html?apikey=" + getParameterByName("apikey"));
}

function logout()
{
	$.ajax({
		url: "rest/users/logout/" + getParameterByName("apikey"),
		cache: false,
		success: function(data) {
			window.location.replace("login.html");
		},
		error: function(error) {
			window.location.replace("login.html");
			//console.log("error updating table -" + error.status);
		}
	});
}

function projectManager(){
	var result = $.ajax({
		async:true,
		url: "rest/users/user/" + getParameterByName("apikey"),
		cache: false,
		success: function(data) {
			if (data.userGroupType.groupName == "Project Manager"){
				$("#assigned").prop("disabled", false);
			} else {
				$("#assigned").prop("disabled", true);
			}

		},
		error: function(error) {
			console.log("error updating table -" + error.status);
			result = true;
		}
	});	
}


//function readOnlyUser(){
//	var result = $.ajax({
//		async:true,
//		url: "rest/users/user/" + getParameterByName("apikey"),
//		cache: false,
//		success: function(data) {
//			if (data.userGroupType.groupName == "ReadOnly"){
//				return true;
//			} else {
//				return false;
//			}
//
//		},
//		error: function(error) {
//			console.log("error updating table -" + error.status);
//			result = true;
//		}
//	});	
//	return result;
//}

function getUserFromAPI(apikey){
	var username = [];
	$.ajax({
		async:true,
		url: "rest/users/user/" + apikey,
		cache: false,
		success: function(data) {
			username = data.firstName;
			$("#userNameDropDown").html(username + "<b class='caret'></b>");
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
	return username;

}

function init() {


	$(document).ajaxStart(function(){
		$("#indicator").show();
	}).ajaxStop(function(){
		$("#indicator").hide();
	});

	var redirectFlag = false;
	var validateUserResult = $.ajax({
		async: true,
		url: "rest/users/validateApiKey/" + getParameterByName("apikey"),
		cache: false,
		success: function(data) {
			return;
		},
		error: function(error) {
			return;
		}
	});

	validateUserResult.done(
			processWorkpackageDetailPage
	).fail(function(){
		redirectFlag = true;
		window.location.replace("login.html");
	});
	if(redirectFlag == true)
		return;
}

function processWorkpackageDetailPage(){

	getUserFromAPI(getParameterByName("apikey"));
	$("#map-outer").dialog({
		autoOpen: false,
		my: "center",
		at: "center",
		resizeStop: function(event, ui) {google.maps.event.trigger(map, 'resize')  },
		open: function(event, ui) {google.maps.event.trigger(map, 'resize'); if (markers.length > 0) fitToMarkers(markers);},      
		height: 600,
		width: 800,
		modal: true,
		buttons: {
			Close: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			//clear form
		}

	});

	$( "#addressLink" ).on( 'click', function( e ) {
		//e.preventDefault();
		$("#map-outer").dialog( "open" );
	});

	$('select').change(function(event) {
		$("#btn_saveStatus").removeClass("btn-danger btn-success");
	});

	$('#status').change(function(event) {
		$.ajax({
			async:true,
			url: "rest/users/user/" + getParameterByName("apikey"),
			cache: false,
			success: function(data) {
				if (data.userGroupType.groupName != "ReadOnly"){
					event.preventDefault();
					toggleSelector();					
				}
			},
			error: function(error) {
				console.log("error updating table -" + error.status);
			}
		});	
	});

	$('#btn_saveStatus').click(function(event) {
		event.preventDefault();
		saveDetails();
	});

	$("#logoutButton").click(function(){
		logout();
	});

	$("#workPackageNav").click(function(){
		redirectWorkPackageListPage();
	});

	$("#dashboardLink").click(function(){
		redirectDashboardPage();
	});

	$("#workloadLink").click(function(){
		redirectWorkloadPage();
	});

//	$("#addNoteButton").click(function(){
//	$('#notesData1').val("");	   	
//	});

	$("#saveNotesButton").click(function(){

		$.ajax({
			async:true,
			url: "rest/users/user/" + getParameterByName("apikey"),
			cache: false,
			success: function(data) {
				if (data.userGroupType.groupName != "ReadOnly"){

					var newNotes = $( "#notesData1" );
					var mangtug = {
							workpackageID: getParameterByName("workPackageID"),
							apikey: getParameterByName("apikey"),
							note: newNotes.val()
					};

					$.ajax({
						async: true,
						contentType: "application/json",
						//			dataType: "json",
						type: "GET",
						data:  mangtug ,
						url: "rest/workpackages/addNote",  // NOT UPDATE NOTES (PLURAL)
						cache: false,
						success: function(data) {
							//				$("#txt_response").empty().append('Add note complete');
							retrieveWorkPackageNotesTable(getParameterByName("workPackageID"));
							$('#notesData1').val("");
						},
						error: function(error) {
							console.log('Add note failed');
						}
					});
				}
			},
			error: function(error) {
				console.log("error updating table -" + error.status);
			}
		});	
	});

	fillMap("map");

	reloadWorkpackage(getParameterByName("workPackageID"), getParameterByName("apikey"));



	getNewAttachmentTemplate(getParameterByName("workPackageID"), "#attachments",".wplbox_pg_3");  //"#wp_gallery_pg_3",".wplbox_pg_3");
	getScorecardTemplate(getParameterByName("workPackageID"));

	initAttachments(getParameterByName("workPackageID"), getParameterByName("apikey"));

	$(".fancybox").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none',
		iframe : {
			preload: false
		}
	});

	$("#tbl_notes").tablesorter({
//		theme : 'ice',
		sortInitialOrder: "desc",
		widgets        : ['columns','filter'],
		usNumberFormat : false,
//		sortReset      : true,
//		sortRestart    : true,

		widgetOptions : {
			filter_external : '.search',
			filter_columnFilters: true,
			filter_placeholder: { search : 'Search...' },
			// extra css class applied to the table row containing the filters & the inputs within that row
			filter_cssFilter   : '',

			// If there are child rows in the table (rows with class name from "cssChildRow" option)
			// and this option is true and a match is found anywhere in the child row, then it will make that row
			// visible; default is false
			filter_childRows   : false,

			// Set this option to false to make the searches case sensitive
			filter_ignoreCase  : true,

			// jQuery selector string of an element used to reset the filters
			filter_reset : '.reset',

			// Delay in milliseconds before the filter widget starts searching; This option prevents searching for
			// every character while typing and should make searching large tables faster.
			filter_searchDelay : 300,

			// Set this option to true to use the filter to find text from the start of the column
			// So typing in "a" will find "albert" but not "frank", both have a's; default is false
			filter_startsWith  : false,

			// if false, filters are collapsed initially, but can be revealed by hovering over the grey bar immediately
			// below the header row. Additionally, tabbing through the document will open the filter row when an input gets focus
			filter_hideFilters : false,

			// Add select box to 4th column (zero-based index)
			// each option has an associated function that returns a boolean
			// function variables:
			// e = exact text from cell
			// n = normalized value returned by the column parser
			// f = search filter input value
			// i = column index
			filter_functions : {

				// Add select menu to this column
				// set the column value to true, and/or add "filter-select" class name to header
				// 0 : true,

				// Exact match only
//				8 : {
//				"Open" : function(e, n, f, i, $r) { return e === f; },
//				"Complete" : function(e, n, f, i, $r) { return e === f; }
//				}
			}
		}
	});

	var filters = [];

	filters[6] = '';
	// using "table.hasFilters" here to make sure we aren't targetting a sticky header
	$("table").trigger("update");

	$.tablesorter.setFilters( $('table.hasFilters'), filters, true ); // new v2.9


	$("tr.tablesorter-filter-row").toggle();

	$("#filter").click(function() {
		$("tr.tablesorter-filter-row").toggle();
	});

}


function initAttachments(workPackageId, apikey){

	$("#attachmentUploadDialog").on("dialogopen", function( event, ui ) {
		$('#fileupload table tbody tr.template-download').remove();

		//Load existing attachments
		$('#fileupload').addClass('fileupload-processing');
		$.ajax({
			url: 'rest/file/' + workPackageId + '/' + apikey + '/attachments',
			dataType: 'json',
			context: $('#fileupload')[0],
			cache: false,
//			deleteUrl:'rest/file/' + workPackageId + '/' + apikey + '/attachments',
			data: {  }
		}).always(function (e, data) {
			$(this).removeClass('fileupload-processing');
		}).done(function (result) {
			$(this).fileupload('option', 'done')
			.call(this, $.Event('done'), { result: result} );
		});      
	});

	//used to set the form data for each row to send to the server 
	$('#fileupload').bind('fileuploadsubmit', function (e, data) {
		var inputs = data.context.find(':input');
		$('input[name=h_wpid]').val(workPackageId);
		data.formData = inputs.serializeArray();
		var form = $("#fileupload");
    	
    	jQuery.validator.setDefaults({
    		  debug: true,
    		  success: "valid"
    		});
    	
    	form.validate();
    	
    	if (!form.valid()){
    		var buttons = $("#fileupload :input").each(function(){
    			$(this).prop('disabled', false);
    		});
			return false;
		}
	});


	$( "#attachmentUploadDialog" ).dialog({
		autoOpen: false,
		height: 600,
		width: 800,
		modal: true,
		buttons: {
			Close: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
//			getAttachmentTemplate2(workPackageId, "#wp_gallery_pg_2",".wplbox_pg_3");
			getNewAttachmentTemplate(workPackageId, "#attachments",".wplbox_pg_3");  //"#wp_gallery_pg_3",".wplbox_pg_3");

		}
	});

	$( "#btn_manage_attach" )
	.click(function() {
		$.ajax({
			async:true,
			url: "rest/users/user/" + getParameterByName("apikey"),
			cache: false,
			success: function(data) {
				if (data.userGroupType.groupName != "ReadOnly"){
					$( "#attachmentUploadDialog" ).dialog( "open" );
				}
			},
			error: function(error) {
				console.log("error updating table -" + error.status);
			}
		});			
	});	
}


//Get value of tag from query string...
function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


function reloadWorkpackage(wpid, apikey){
	//Fetches the initial member data and populates the table using jquery templates
	if (!isNaN(parseFloat(wpid)) && isFinite(wpid)){
		populateWorkPackage(wpid, apikey);
		retrieveWorkPackageNotesTable(wpid);
		getNewAttachmentTemplate(wpid, "#attachments",".wplbox_pg_3");  //"#wp_gallery_pg_3",".wplbox_pg_3");

	}
}

//Formats all dates to the following values...

/*
 * 
 * 
token:     description:             example:
#YYYY#     4-digit year             1999
#YY#       2-digit year             99
#MMMM#     full month name          February
#MMM#      3-letter month name      Feb
#MM#       2-digit month number     02
#M#        month number             2
#DDDD#     full weekday name        Wednesday
#DDD#      3-letter weekday name    Wed
#DD#       2-digit day number       09
#D#        day number               9
#th#       day ordinal suffix       nd
#hhh#      military/24-based hour   17
#hh#       2-digit hour             05
#h#        hour                     5
#mm#       2-digit minute           07
#m#        minute                   7
#ss#       2-digit second           09
#s#        second                   9
#ampm#     "am" or "pm"             pm
#AMPM#     "AM" or "PM"             PM
 */

Date.prototype.customFormat = function(formatString){
	var YYYY,YY,MMMM,MMM,MM,M,DDDD,DDD,DD,D,hhh,hh,h,mm,m,ss,s,ampm,AMPM,dMod,th;
	var dateObject = this;
	YY = ((YYYY=dateObject.getFullYear())+"").slice(-2);
	MM = (M=dateObject.getMonth()+1)<10?('0'+M):M;
	MMM = (MMMM=["January","February","March","April","May","June","July","August","September","October","November","December"][M-1]).substring(0,3);
	DD = (D=dateObject.getDate())<10?('0'+D):D;
	DDD = (DDDD=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][dateObject.getDay()]).substring(0,3);
	th=(D>=10&&D<=20)?'th':((dMod=D%10)==1)?'st':(dMod==2)?'nd':(dMod==3)?'rd':'th';
	formatString = formatString.replace("#YYYY#",YYYY).replace("#YY#",YY).replace("#MMMM#",MMMM).replace("#MMM#",MMM).replace("#MM#",MM).replace("#M#",M).replace("#DDDD#",DDDD).replace("#DDD#",DDD).replace("#DD#",DD).replace("#D#",D).replace("#th#",th);

	h=(hhh=dateObject.getHours());
	if (h==0) h=24;
	if (h>12) h-=12;
	hh = h<10?('0'+h):h;
	AMPM=(ampm=hhh<12?'am':'pm').toUpperCase();
	mm=(m=dateObject.getMinutes())<10?('0'+m):m;
	ss=(s=dateObject.getSeconds())<10?('0'+s):s;
	return formatString.replace("#hhh#",hhh).replace("#hh#",hh).replace("#h#",h).replace("#mm#",mm).replace("#m#",m).replace("#ss#",ss).replace("#s#",s).replace("#ampm#",ampm).replace("#AMPM#",AMPM);
}

//THIS IS ALL DUPLICATE OF THE SAME FUNCTIONS IN work-package-list.js!!!
//DUE TO TIME ISSUES I AM REPLICATING IT HERE.

function populateWorkPackage(workPackageID, apikey){

	var groupName = ""; 
	var firstname = "";
	var lastname = ""; 

	$.ajax({
		async: true,
		url: "rest/workpackages/workpackage/" + workPackageID,
		cache: false,
		success: function(workpackage) {

			if (workpackage.assignedUser) {firstname = workpackage.assignedUser.firstName;}
			if (workpackage.assignedUser) {lastname = workpackage.assignedUser.lastName;}
			if (workpackage.groupType) {groupName = workpackage.groupType.groupName;}
			var daysOpen = Math.round(Math.abs(new Date().getTime() - workpackage.creationDate) / (1000 * 60 *60 * 24));

			populateAddress(workpackage);
			$("#createDate").text(new Date(workpackage.creationDate).customFormat("#MM#/#DD#/#YYYY#"));	
			$("#priority").text(workpackage.priority.priorityName);
			$("#caseNumber").text(workpackage.caseNumber);
			$("#siteName").text((workpackage.site !== null ? workpackage.site.siteName : " "));
			$("#sectorName").text((workpackage.sector !== null ? workpackage.sector.sectorName :  " " ));
			$("#opsZone").text(workpackage.opsTurf);
			$("#technology").text((workpackage.technology !== null ? workpackage.technology.technologyName : " "));
			$("#frequency").text(workpackage.frequency !== null ? workpackage.frequency : " ");
			$("#uplink").text(workpackage.uplinkChFreq !== null ? workpackage.uplinkChFreq : " ");
			$("#rssi").text(workpackage.rSSI !== null ? workpackage.rSSI : " ");
			$("#hidden-workpackageID").val(workpackage.workpackageID);
			$("#rfengineer").text(workpackage.rfEngineer);
			$("#rfengineernum").text(workpackage.rfEngineerNum !== null ? workpackage.rfEngineerNum : " ");

			// Populate the Address

			var lastCloseDate = null;
			retrieveGroupTypes(apikey, workpackage);	
			retrieveStatuses(apikey, workpackage);
			retrieveUsers(apikey, workpackage);
			projectManager();
			retrieveResolutions(apikey, workpackage);		
//			if(workpackage.status.statusID == 5)
//				lastCloseDate = getLastWorkPackageCompletionDate(workpackage.workPackageID);

//			result += '<p class="Table-Body TC-P2Dark">Status</p>';

//			result += '<select name="cmb_update_status" id="cmb_update_status" onchange="toggleSelector();">';
//			if(statuses && statuses.length > 0) {
//			_.each(statuses, function(status) {
//			result += '<option value="' + status.statusID +'"';
//			if(workpackage.status.statusID == status.statusID){ 
//			result += ' selected '; 
//			}
//			result += '>' + status.statusName + '</option>';
//			});
//			} else {
//			result += '<option value"0">No Statuses Found</option>';
//			}
//			result += '</select>';

//			result += '<p class="Table-Body TC-P2Dark">Assigned</p>';
//			result += '<select name="cmb_user_id" id="cmb_user_id">';
//			if(users && users.length > 0) {
//			_.each(users, function(user) {
//			var usr = workpackage.assignedUser;
//			result += '<option value="' + user.userID +'"';
//			if (usr){
//			if(usr.userID == user.userID){ 
//			result += ' selected '; 
//			}
//			}
//			result += '>' + user.lastName + ', ' + user.firstName + '</option>';
//			});
//			} else {
//			result += '<option value"0">No Users Found</option>';
//			}

//			result += '</select>';

//			result += '<p class="Table-Body TC-P2Dark">Resolution</p>';

//			result += '<select name="cmb_res_id" id="cmb_res_id" onchange="clearResolution();">';
//			if(resolutions && resolutions.length > 0) {
//			result += '<option value="0">Select Resolution</option>';
//			_.each(resolutions, function(res) {
//			result += '<option value="' + res.resolutionID +'"';
//			var wpResolution = workpackage.resolution;
//			if (wpResolution){
//			wpResolution = workpackage.resolution.resolutionID;
//			}
//			if(wpResolution == res.resolutionID){ 
//			result += ' selected '; }
//			result += '>' + res.resolutionName + '</option>';
//			});
//			} else {
//			result += '<option value"0">No Resolutions Found</option>';
//			}
//			result += '</select>';

			// Clear Resolution, if necessary...
			toggleSelector();
			populateMap(workpackage);	
		},
		error: function(error) {
			//console.log("error updating table -" + error.status);
		}

	});
}

function populateAddress(workpackage){

	var address = workpackage.site.address;
	var city = address.city;
	var province = address.province;
	var postalCode = address.postal_code;
	$("#addressLink").text(address.address + " " + city.city + ", " + province + " " + postalCode);

}

function getNewAttachmentTemplate(wpkey, divName, boxName) {
	$.ajax({
		url: "tmpl/attachment.tmpl",
		dataType: "html",
		success: function( data ) {
			$( "head" ).append( data );
			updateWorkPackageListAttachmentTable(wpkey, divName, boxName);
		}
	}); 
}

/* Uses JAX-RS GET to retrieve current member list */
function updateWorkPackageListAttachmentTable(wpkey, divName, boxName) {
	$.ajax({
		//url: "rest/workpackages/" + wpkey + "/attachments/",
		url: "rest/file/" + wpkey + "/" + getParameterByName("apikey") + "/attachments",
		async: true,
		cache: false,
		success: function(data) {
			// wp_gallery_pg_3
			$(divName).empty().append(buildNewWorkPackageAttachmentsRows(data));
//			$(boxName).wplightbox(
//			{"loadBtnSrc":"images/lightbox_load.gif","border_e":"images/lightbox_e_6.png","border_n":"images/lightbox_n_6.png","border_w":"images/lightbox_w_6.png","border_s":"images/lightbox_s_6.png","border_ne":"images/lightbox_ne_6.png","border_se":"images/lightbox_se_6.png","border_nw":"images/lightbox_nw_6.png","border_sw":"images/lightbox_sw_6.png","closeBtnSrc":"images/lightbox_close_2.png","closeOverBtnSrc":"images/lightbox_close_over_2.png","nextBtnSrc":"images/lightbox_next_2.png","nextOverBtnSrc":"images/lightbox_next_over_2.png","prevBtnSrc":"images/lightbox_prev_2.png","prevOverBtnSrc":"images/lightbox_prev_over_2.png","blankSrc":"scripts/blank.gif","bBkgrndClickable":true,"strBkgrndCol":"#000000","nBkgrndOpacity":0.0,"strContentCol":"#ffffff","nContentOpacity":0.8,"strCaptionCol":"#555555","nCaptionOpacity":1.0,"nCaptionType":1,"bCaptionCount":false,"strCaptionFontType":"Tahoma,Serif","strCaptionFontCol":"#ffffff","nCaptionFontSz":15,"bShowPlay":false,"bAnimateOpenClose":true,"nPlayPeriod":2000}
//			);
//			$('.wplbox_pg_3').wplightbox({
//			"loadBtnSrc":"images/lightbox_load.gif",
//			"border_e":"images/lightbox_e_6.png",
//			"border_n":"images/lightbox_n_6.png",
//			"border_w":"images/lightbox_w_6.png",
//			"border_s":"images/lightbox_s_6.png",
//			"border_ne":"images/lightbox_ne_6.png",
//			"border_se":"images/lightbox_se_6.png",
//			"border_nw":"images/lightbox_nw_6.png",
//			"border_sw":"images/lightbox_sw_6.png",
//			"closeBtnSrc":"images/lightbox_close_2.png",
//			"closeOverBtnSrc":"images/lightbox_close_over_2.png",
//			"nextBtnSrc":"images/lightbox_next_2.png",
//			"nextOverBtnSrc":"images/lightbox_next_over_2.png",
//			"prevBtnSrc":"images/lightbox_prev_2.png",
//			"prevOverBtnSrc":"images/lightbox_prev_over_2.png",
//			"blankSrc":"scripts/blank.gif",
//			"bBkgrndClickable":true,
//			"strBkgrndCol":"#000000",
//			"nBkgrndOpacity":0.0,
//			"strContentCol":"#ffffff",
//			"nContentOpacity":0.8,
//			"strCaptionCol":"#555555",
//			"nCaptionOpacity":1.0,
//			"nCaptionType":1,
//			"bCaptionCount":false,
//			"strCaptionFontType":"Verdana,Serif",
//			"strCaptionFontCol":"#ffffff",
//			"nCaptionFontSz":15,
//			"bShowPlay":false,
//			"bAnimateOpenClose":true,
//			"nPlayPeriod":2000
//			});
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
}

/* Builds the updated table for the member list */
function buildNewWorkPackageAttachmentsRows(files) {
	jsonFiles = JSON.parse(files);
	return _.template( $( "#attachment-tmpl" ).html(), {"files": jsonFiles.files});
}


function retrieveWorkPackageNotesTable(workPackageID) {

	$.ajax({
		async: true,
		url: "rest/workpackages/" + workPackageID +"/notes",
		cache: false,
		success: function(data) {
			var table = $("#tbl_notes tbody");
			table.empty();
			$.each(data, function(idx, elem){
				table.append("<tr><td>"+ elem.user.firstName + " " + elem.user.lastName +"</td><td>"+ new Date(elem.creationDate).customFormat( "#MM#/#DD#/#YYYY#" ) +"</td><td>" + elem.note + "</td></tr>");
			});
			$("table").trigger("update");
		},
		error: function(error) {
			//console.log("error updating table -" + error.status);
		}

	});
}

function fillMap(divName){
	var mapDiv =  document.getElementById(divName);
	var latlng = new google.maps.LatLng(33.9405, -84.2255);
	var options =
	{                                 
			zoomControl: true,
			scaleControl: true,                        
			center: latlng,
			zoom: 15,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true,
			draggable: true,
			draggableCursor: 'move'
	};

	map = new google.maps.Map(mapDiv, options);
	map.set("streetViewControl", false);

	google.maps.event.addListener(map, 'zoom_changed', function() {
		zoomChangeBoundsListener = 
			google.maps.event.addListener(map, 'bounds_changed', function(event) {
				if (this.getZoom() > 15 && this.initialZoom == true) {

					// Change max/min zoom here
					this.setZoom(15);
					this.initialZoom = false;

				}

				google.maps.event.removeListener(zoomChangeBoundsListener);

			});
	});
	map.initialZoom = true;

}



function populateMap(workpackage){

	deleteMarkers();
	addMarker(workpackage.site.latitude, workpackage.site.longitude, workpackage.site.siteName, 'map', null);
	fitToMarkers(markers);
}



/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
///////////                   GOOGLE MAP FUNCTIONS                        ///////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

function addMarker(lat, lng, title, mapDiv, image){

	var options =
	{
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true,
			draggableCursor: 'move'
	};

//	map = new google.maps.Map( document.getElementById("map_canvas"), options);

	var markerR = new google.maps.Marker({
		position: new google.maps.LatLng(lat, lng),
		map: map,
		title: title,
		icon: 'images/markers/circles.png'
	});

	markers.push(markerR);
	var infoContents = '<h3>Directions</h>';
	var infowindowR = new google.maps.InfoWindow({content: infoContents});        
	google.maps.event.addListener(markerR, 'click', function() {
		infowindowR.open(window.map, markerR);
	});
}

function fitToMarkers(markers){

	map.initialZoom = true;
	var bounds = new google.maps.LatLngBounds();
//	for ( var i in markers) {
	for(var i = 0; i < markers.length; i++) {
		var latlong = markers[i].getPosition();
		bounds.extend(latlong);
	}

//	Don't zoom in too far on only one marker
	if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
		var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.50, bounds.getNorthEast().lng() + 0.50);
		var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.50, bounds.getNorthEast().lng() - 0.50);
		bounds.extend(extendPoint1);
		bounds.extend(extendPoint2);
	}
	map.fitBounds(bounds);

}




//Sets the map on all markers in the array.
function setAllMap(map) {
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
	}
}

//Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
	setAllMap(null);
}

//Deletes all markers in the array by removing references to them.
function deleteMarkers() {
	clearMarkers();
	markers = [];
}

function clearMarkers(){
	setAllMap(null);
}

//Shows any markers currently in the array.
function showMarkers() {
	setAllMap(map);
}


function centerOnMarkers(){
//	Make an array of the LatLng's of the markers you want to show

	var LatLngList = [];
	for (var i = 0; i < markers.length; i++ ) {
		LatLngList.push(markers[i].getPosition());
	}

	var bounds = new google.maps.LatLngBounds ();
//	Go through each...
	for (var i = 0, LtLgLen = LatLngList.length; i < LtLgLen; i++) {
//		And increase the bounds to take this point
		bounds.extend (LatLngList[i]);
	}
//	Fit these bounds to the map
	map.fitBounds (bounds);
}

/* Builds the updated table for the member list */
function buildNewWorkPackageAttachmentsRows(files) {
	jsonFiles = JSON.parse(files);
	return _.template( $( "#attachment-tmpl" ).html(), {"files": jsonFiles.files});
}


function getNewAttachmentTemplate(wpkey, divName, boxName) {
	$.ajax({
		url: "tmpl/attachment.tmpl",
		dataType: "html",
		success: function( data ) {
			$( "head" ).append( data );
			updateWorkPackageListAttachmentTable(wpkey, divName, boxName);
		}
	}); 
}

/* Uses JAX-RS GET to retrieve current member list */
function updateWorkPackageListAttachmentTable(wpkey, divName, boxName) {
	$.ajax({
		//url: "rest/workpackages/" + wpkey + "/attachments/",
		url: "rest/file/" + wpkey + "/" + getParameterByName("apikey") + "/attachments",
		async: true,
		cache: false,
		success: function(data) {
			// wp_gallery_pg_3
			$(divName).empty().append(buildNewWorkPackageAttachmentsRows(data));
//			$(boxName).wplightbox(
//			{"loadBtnSrc":"images/lightbox_load.gif","border_e":"images/lightbox_e_6.png","border_n":"images/lightbox_n_6.png","border_w":"images/lightbox_w_6.png","border_s":"images/lightbox_s_6.png","border_ne":"images/lightbox_ne_6.png","border_se":"images/lightbox_se_6.png","border_nw":"images/lightbox_nw_6.png","border_sw":"images/lightbox_sw_6.png","closeBtnSrc":"images/lightbox_close_2.png","closeOverBtnSrc":"images/lightbox_close_over_2.png","nextBtnSrc":"images/lightbox_next_2.png","nextOverBtnSrc":"images/lightbox_next_over_2.png","prevBtnSrc":"images/lightbox_prev_2.png","prevOverBtnSrc":"images/lightbox_prev_over_2.png","blankSrc":"scripts/blank.gif","bBkgrndClickable":true,"strBkgrndCol":"#000000","nBkgrndOpacity":0.0,"strContentCol":"#ffffff","nContentOpacity":0.8,"strCaptionCol":"#555555","nCaptionOpacity":1.0,"nCaptionType":1,"bCaptionCount":false,"strCaptionFontType":"Tahoma,Serif","strCaptionFontCol":"#ffffff","nCaptionFontSz":15,"bShowPlay":false,"bAnimateOpenClose":true,"nPlayPeriod":2000}
//			);
//			$('.wplbox_pg_3').wplightbox({
//			"loadBtnSrc":"images/lightbox_load.gif",
//			"border_e":"images/lightbox_e_6.png",
//			"border_n":"images/lightbox_n_6.png",
//			"border_w":"images/lightbox_w_6.png",
//			"border_s":"images/lightbox_s_6.png",
//			"border_ne":"images/lightbox_ne_6.png",
//			"border_se":"images/lightbox_se_6.png",
//			"border_nw":"images/lightbox_nw_6.png",
//			"border_sw":"images/lightbox_sw_6.png",
//			"closeBtnSrc":"images/lightbox_close_2.png",
//			"closeOverBtnSrc":"images/lightbox_close_over_2.png",
//			"nextBtnSrc":"images/lightbox_next_2.png",
//			"nextOverBtnSrc":"images/lightbox_next_over_2.png",
//			"prevBtnSrc":"images/lightbox_prev_2.png",
//			"prevOverBtnSrc":"images/lightbox_prev_over_2.png",
//			"blankSrc":"scripts/blank.gif",
//			"bBkgrndClickable":true,
//			"strBkgrndCol":"#000000",
//			"nBkgrndOpacity":0.0,
//			"strContentCol":"#ffffff",
//			"nContentOpacity":0.8,
//			"strCaptionCol":"#555555",
//			"nCaptionOpacity":1.0,
//			"nCaptionType":1,
//			"bCaptionCount":false,
//			"strCaptionFontType":"Verdana,Serif",
//			"strCaptionFontCol":"#ffffff",
//			"nCaptionFontSz":15,
//			"bShowPlay":false,
//			"bAnimateOpenClose":true,
//			"nPlayPeriod":2000
//			});
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
}

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
///////////            WORKPACKAGE SCORECARD FUNCTIONS                    ///////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

function getScorecardSummaryTemplate(wpkey) {
	$.ajax({
		url: "tmpl/scorecardsummary.tmpl",
		dataType: "html",
		success: function( data ) {

			$( "head" ).append( data );
			updateScorecardSummaryTable(wpkey);
		}
	});
}

/* Uses JAX-RS GET to retrieve current member list */
function updateScorecardSummaryTable(wpkey) {
	$.ajax({
		url: "rest/workpackages/score/" + wpkey,
		cache: false,
		success: function(data) {
			$('#wp_scorecard_src').empty().append(buildScorecardSummaryRows(data));
		},
		error: function(error) {
//			console.log("error updating table -" + error.status);
		}
	});
}

function buildScorecardSummaryRows(scorecards) {
	return _.template( $( "#scorecardsummary-tmpl" ).html(), {"scores": scorecards});
}

function getScorecardTemplate(wpkey) {
	$.ajax({
		url: "tmpl/scorecard.tmpl",
		dataType: "html",
		success: function( data ) {

			$( "head" ).append( data );
			updateScorecardTable(wpkey);
		}
	});
}

/* Uses JAX-RS GET to retrieve current member list */
function updateScorecardTable(wpkey) {
	$.ajax({
		url: "rest/workpackages/score/" + wpkey,
		cache: false,
		success: function(data) {
			$('#wp_id_scorecard_src').empty().append(buildScorecardRows(data));
		},
		error: function(error) {
//			console.log("error updating table -" + error.status);
		}
	});
}


function buildScorecardRows(scorecards) {
	return _.template( $( "#scorecard-tmpl" ).html(), {"scores": scorecards});
}

function retrieveGroupTypes(apikey, workpackage){
	$.ajax({
		url: "rest/utility/jobtypesByCompanyProduct/" + apikey,
		cache: false,
		async: true,
		success: function(localJobTypes) {
			var _select = $('<select>');
			if(localJobTypes && localJobTypes.length > 0) {
				$.each(localJobTypes, function(indx, job) {
					var selected = false;
					if(workpackage.groupType.groupName == job.groupName){
						selected = true;
					}
					_select.append(
							$('<option></option>').val(job.groupTypeID).html(job.groupName).attr("selected", selected)
					);
				});
				$('#cmb_group_id').empty().append(_select.html());
			}
		},
		error: function(error) {
			//console.log("error updating table -" + error.status);
		}
	});
}

function retrieveUsers(apikey, workpackage){
	$.ajax({
		url: "rest/users/all/" + apikey,
		cache: false,
		async: true,
		success: function(users) {
			_select = $('<select>');
			_select.append($('<option></option>').val(0).html("Select User"));

			if(users && users.length > 0) {
				$.each(users, function(indx, usr) {
					var user = workpackage.assignedUser;
					var selected = false;
					if (user)
					{
						if(usr.userID == user.userID){
							selected = true;
						}
					}
					_select.append($('<option></option>').val(usr.userID).html(usr.firstName + " " + usr.lastName).attr('selected', selected)							
					);
				});
				$('#assigned').empty().append(_select.html());
			}

		},
		error: function(error) {
			//console.log("error updating table -" + error.status);
		}
	});
}


function retrieveStatuses(apikey, workpackage){
	var comp = 0;
	var prod = 0;
	$.ajax({
		url: "rest/users/user/" + apikey,
		cache: false,
		async: false,
		success: function(data) {
			comp = data.company.companyID;
			prod = data.product.productID;

			$.ajax({
				url: "rest/utility/statuses/" + comp + "/" + prod,
				cache: false,
				async: false,
				success: function(statuses) {

					_select = $('<select>');
					if(statuses && statuses.length > 0) {
						$.each(statuses, function(indx, sts) {
							var selected = false;
							if(workpackage.status.statusID == sts.statusID){
								selected = true;
							} 					
							_select.append(
									$('<option></option>').val(sts.statusID).html(sts.statusName).attr("selected", selected)
							);
						});
						$('#status').empty().append(_select.html());
					}
				},
				error: function(error) {
					//console.log("error updating table -" + error.status);
				}
			});
		},
		error: function(error) {
			return;
		}
	});
}

function retrieveResolutions(apikey, workpackage){
	var prod = 0;
	$.ajax({
		url: "rest/users/user/" + apikey,
		cache: false,
		async: false,
		success: function(data) {
			prod = data.product.productID;

			$.ajax({
				url: "rest/utility/resolutions/" + prod,
				cache: false,
				async: false,
				success: function(resolutions) {
					_select = $('<select>');
					_select.append(
							$('<option></option>').val(0).html("Select Resolution"));
					if(resolutions && resolutions.length > 0) {
						$.each(resolutions, function(indx, res) {
							var wpResolution = workpackage.resolution;
							if (wpResolution){
								wpResolution = workpackage.resolution.resolutionID;
							}
							var selected = false;
							if(wpResolution == res.resolutionID){
								selected = true;
							} 					

							_select.append(
									$('<option></option>').val(res.resolutionID).html(res.resolutionName).attr("selected", selected)
							);
						});
						$('#cmb_res_id').empty().append(_select.html());
					}

				},
				error: function(error) {
					//console.log("error updating table -" + error.status);
				}
			});
		},
		error: function(error) {
			return;
		}
	});
}

function getLastWorkPackageCompletionDate(wpkey) {
	var returnValue = [];

	$.ajax({
		async:false,
		url: "rest/workpackages/workpackage/lastCompletionDate/" + wpkey,
		cache: false,
		//dataType: "json",
		success: function(data) {
			returnValue = data.currentStatusCreationDate;
		},
		error: function(error) {
//			console.log("error updating table -" + error.status);
		}
	});
	return returnValue;
}

function saveDetails(){
	$.ajax({
		async:true,
		url: "rest/users/user/" + getParameterByName("apikey"),
		cache: false,
		success: function(data) {
			if (data.userGroupType.groupName != "ReadOnly"){
				if (($("#status").val() === "5" || $("#status").val() === "12") &&
						$("#cmb_res_id").val() === "0") 
				{
					alert("Please choose a resolution.");
					return;
				}

				if (jQuery('#status').val() == null || jQuery('#assigned').val() == null)
					return;


				var mangtug = "workpackageID="+ getParameterByName("workPackageID") +"&statusID=" + jQuery('#status').val() +"&techID=" + jQuery('#assigned').val() + "&jobTypeID=" + jQuery('#cmb_group_id').val() + "&resolutionID=" + jQuery('#cmb_res_id').val();

				$.ajax({
					//contentType: "application/json",
					//dataType: "json",
					type: "POST",
					data:  mangtug ,
					url: "rest/workpackages/update/" + getParameterByName("apikey"),
					cache: false,
					success: function(data) {
						//	updateUsers(getParameterByName("userID"));
						//	updateStatuses(getParameterByName("userID"));
						//	chooseCorrectUserAndStatus(getParameterByName("workpackageID"));
						reloadWorkpackage(getParameterByName("workPackageID"),getParameterByName("apikey"));
						$("#btn_saveStatus").addClass("btn-success");
					},
					error: function(error) {

						$("#btn_saveStatus").addClass("btn-danger");
						console.log("error updating table -" + error.status);
					}
				});
			}

		},
		error: function(error) {
			console.log("error updating table -" + error.status);
			result = true;
		}
	});	

}