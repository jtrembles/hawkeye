function GetFrameUrl(frameid){
  var docframe=document.getElementById(frameid);
  if(docframe===null){
    return;}
  var sURL=window.location.href;
  if(sURL.indexOf("?")>0){
    var arrParams=sURL.split("?");
    var arrURLParams=arrParams[1].split("&");
    var i,sParam,sValue;
    for(i=0;i<arrURLParams.length;i++){
      sParam=arrURLParams[i].split("=");
      sValue=decodeURIComponent(sParam[1]);
      if(decodeURIComponent(sParam[0])==docframe.id&&docframe.src!=sValue){
        docframe.src=sValue;
        return;
      }
    }
  }
}