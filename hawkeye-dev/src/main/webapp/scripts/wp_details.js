function deleteAttachment(attachmentName){

    var mangtug = {
            workpackageID:  getParameterByName("workpackageID"),
            attachmentName: attachmentName
    };

	var dump = JSON.stringify(mangtug);
    
	$.ajax({
		contentType: "application/json",
//		dataType: "json",
		type: "GET",
		data:  mangtug ,
		url: "rest/workpackages/" + getParameterByName("workpackageID") + "/deleteAttachment",
		cache: false,
		success: function(data) {
			$("#txt_response").empty().append('Delete attachment complete');
			deleteAttachmentTemplate(getParameterByName("workpackageID"));
			getAttachmentTemplate2(getParameterByName("workpackageID"));
		},
		error: function(error) {
			$("#txt_response").empty().append('Delete attachment failed');
			deleteAttachmentTemplate(getParameterByName("workpackageID"));
			getAttachmentTemplate2(getParameterByName("workpackageID"));
		}
	});
}

function openDeleteDialog(dialogName, btnName){
$( dialogName ).dialog({
    autoOpen: false,
    height: 300,
    width: 350,
    modal: true,
    buttons: {
      Close: function() {
        $( this ).dialog( "close" );
      }
    },
    close: function() {
        
    }
  });

  $(btnName)
    .click(function() {
      $( dialogName ).dialog( "open" );
    });
}