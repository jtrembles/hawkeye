function redirectPage(data){
	if ((data !== undefined) && (data !== ""))
		window.location.replace("work-package-detail.html?workPackageID=" + data + "&apikey=" + getParameterByName("apikey"));
}

function logout()
{
	$.ajax({
		url: "rest/users/logout/" + getParameterByName("apikey"),
		cache: false,
		success: function(data) {
			window.location.replace("login.html");
		},
		error: function(error) {
			window.location.replace("login.html");
			//console.log("error updating table -" + error.status);
		}
	});
}

function readOnlyUser(){
	var result = true;
	$.ajax({
		async:false,
		url: "rest/users/user/" + getParameterByName("apikey"),
		cache: false,
		success: function(data) {
			if (data.userGroupType.groupName == "ReadOnly"){
				result =  true;
			} else {
				result = false;
			}
			
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
			result = true;
		}
	});	
	return result;
}

function redirectRtcReportPage(){
	window.location.replace("/rptRTC.html?apikey=" + getParameterByName("apikey"));
}

function redirectResReportPage(){
	window.location.replace("/rptRes.html?apikey=" + getParameterByName("apikey"));
}

function redirectDashboardPage(){
	window.location.replace("/dashboard.html?apikey=" + getParameterByName("apikey"));
}

function redirectWorkloadPage(){
	window.location.replace("workpackage-workload.html?apikey=" + getParameterByName("apikey"));
}

function getUserFromAPI(apikey){
	var username = [];
	$.ajax({
		async: true,
		url: "rest/users/user/" + apikey,
		cache: false,
		success: function(data) {
			username = data.firstName;
			$("#userNameDropDown").html(username + "<b class='caret'></b>");

		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
	return username;
	
}

function redirectWorkpackageInsertPage(){
	window.location.replace("work-package-insert.html?apikey=" + getParameterByName("apikey"));
}


//function filterVisible(){
//	
//	$( "div.tumble" ).toggleClass( "tablesorter-filter-row" );
//}

function reloadWorkpackage(wpid){
    //Fetches the initial member data and populates the table using jquery templates
    if (!isNaN(parseFloat(wpid)) && isFinite(wpid)){
    	populateWorkPackage(wpid);
    	retrieveWorkPackageNotesTable(wpid);
    	getNewAttachmentTemplate(wpid, "#attachments",".wplbox_pg_3");  //"#wp_gallery_pg_3",".wplbox_pg_3");
    }
}

function init(){

	$(document).ajaxStart(function(){
		$("#indicator").show();
	}).ajaxStop(function(){
		$("#indicator").hide();
	});

	
	var redirectFlag = false;
	var apikey = getParameterByName("apikey");
	$.ajax({
		async: true,
		url: "rest/users/validateApiKey/" + apikey,
		cache: false,
		success: function(data) {
			processWorkpackagePage();
		},
		error: function(error) {
			redirectFlag = true;
			window.location.replace("login.html");
		}
	});
}


function processWorkpackagePage(){
	
	var apikey = getParameterByName("apikey");
	
	getUserFromAPI(apikey);
	
	  $("#map-outer").dialog({
	      autoOpen: false,
	      my: "center",
	      at: "center",
          resizeStop: function(event, ui) {google.maps.event.trigger(map, 'resize')  },
          open: function(event, ui) {google.maps.event.trigger(map, 'resize'); if (markers.length > 0) fitToMarkers(markers);},      
	      height: 600,
	      width: 800,
	      modal: true,
	      buttons: {
	        Close: function() {
	            $( this ).dialog( "close" );
	          }
	      },
	      close: function() {
	          //clear form
	      }
	  
	    });

    $("#tbl_wp_list").tablesorter({
//        theme : 'ice',
        sortInitialOrder: "desc",
        widthFixed: true,
        widgets        : ['columns','filter'],
        usNumberFormat : false,
//        sortReset      : true,
//        sortRestart    : true,

        widgetOptions : {
        	filter_external : '.search',
        	filter_columnFilters: true,
        	filter_placeholder: { search : 'Search...' },
            // extra css class applied to the table row containing the filters & the inputs within that row
            filter_cssFilter   : '',
            filter_hideEmpty	: true,
            // If there are child rows in the table (rows with class name from "cssChildRow" option)
            // and this option is true and a match is found anywhere in the child row, then it will make that row
            // visible; default is false
            filter_childRows   : false,

            // Set this option to false to make the searches case sensitive
            filter_ignoreCase  : true,

            // jQuery selector string of an element used to reset the filters
            filter_reset : '.reset',

            // Delay in milliseconds before the filter widget starts searching; This option prevents searching for
            // every character while typing and should make searching large tables faster.
            filter_searchDelay : 300,

            // Set this option to true to use the filter to find text from the start of the column
            // So typing in "a" will find "albert" but not "frank", both have a's; default is false
            filter_startsWith  : false,

            // if false, filters are collapsed initially, but can be revealed by hovering over the grey bar immediately
            // below the header row. Additionally, tabbing through the document will open the filter row when an input gets focus
            filter_hideFilters : false,
            
            filter_saveFilters : true,

            // Add select box to 4th column (zero-based index)
            // each option has an associated function that returns a boolean
            // function variables:
            // e = exact text from cell
            // n = normalized value returned by the column parser
            // f = search filter input value
            // i = column index
            filter_functions : {

              // Add select menu to this column
              // set the column value to true, and/or add "filter-select" class name to header
              // 0 : true,

              // Exact match only
//           8 : {
//        	"Open" : function(e, n, f, i, $r) { return e === f; },
//        	   	"Complete" : function(e, n, f, i, $r) { return e === f; }
//           	 }
            }
        }
      });

    $.tablesorter.setFilters( $('#tbl_wp_list'), filters, true ); // new v2.9
    
    $("#selStatusType").change(function() {
    	var sTypeID = $("#selStatusType").children(':selected').text();
    	updateNewWorkPackageListByStatusType(apikey, sTypeID);
//        $("table").trigger("update");
    });
    
    $("#logoutButton").click(function() {
        logout();
    });
    
    $("#filter").click(function() {
        $("tr.tablesorter-filter-row").toggle();
    });
    
    $("#detailButton").click(function(){
    	redirectPage($("#hidden-workpackageID").val());
    });
    
    $("#dashboardLink").click(function(){
    	redirectDashboardPage();
    });

    $("#rptRTC").click(function(){
    	redirectRtcReportPage();
    });
    
    $("#rptRES").click(function(){
    	redirectResReportPage();
    });

    
    $("#workloadLink").click(function(){
    	redirectWorkloadPage();
    });
    
    
	$(".clickableCell").click(function() {
        window.document.location = $(this).attr("href");
    });

	$("#insertWP").click(function() {
		$.ajax({
			async:false,
			url: "rest/users/user/" + getParameterByName("apikey"),
			cache: false,
			success: function(data) {
				if (data.userGroupType.groupName != "ReadOnly"){
					redirectWorkpackageInsertPage();
				}
			},
			error: function(error) {
				console.log("error updating table -" + error.status);
				result = true;
			}
		});			
    });

	$("#importCSV").click(function() {
		$.ajax({
			async:false,
			url: "rest/users/user/" + getParameterByName("apikey"),
			cache: false,
			success: function(data) {
				if (data.userGroupType.groupName != "ReadOnly"){
					$( "#import-dialog-form" ).dialog( "open" );
				}
			},
			error: function(error) {
				console.log("error updating table -" + error.status);
				result = true;
			}
		});
    });
	
	
    //Fetches the initial member data and populates the table using jquery templates
    var tablename = "tbl_wp_list";
    //updateNewWorkPackageList(getParameterByName("apikey"));  //, tablename);
    //$("table").trigger("update");

    $( document ).on('click', '#tbl_wp_list tbody tr', function(event) {
        $(this).addClass('highlight').siblings().removeClass('highlight');
        
    });
		 
    var filters = [];
    
  filters[6] = '';
// using "table.hasFilters" here to make sure we aren't targetting a sticky header
//  $.tablesorter.setFilters( $('table.hasFilters'), filters, false ); // new v2.9
  
    
//Need to get data from the tr for the workpackageID...then onclick call function reloadWP(data.workpackage);
//BLAH BLAH BLAH BLAH...
//$("loadDetails").onclick=\"reloadWorkpackage('" + workpackage.workPackageID +"');\"
  
//Register an event listener on the sumbit action
  $('#frm_wplist_entry').submit(function() {
      
	var fname = document.getElementById("inp_wp").value;
	 // check if fname has the desired extension
	 if (fname.indexOf(".csv") === -1 ) {
    	 alert("File must be of type csv.");
	        return false;
	   }
	
  	// prepare Options Object 
  	var options = { 
  		   url: 'rest/wplist/upload?apikey=' + apikey,
             type: 'POST',
             async: false,
             cache: false,
//             contentType: "application/json",
             processData: false,
             enctype: 'multipart/form-data', 
             success: function (statuses) {
//				 statuses = myHTMLString.match(/<pre>.*?<\/pre>/ims).substr(5, -6);
			 var val = statuses.indexOf("</PRE>");
			 if (val !== -1){
			 	statuses = statuses.substr(5, val - 5);
			 }
			 var html = "";
			 $("#tbl_imp_list_body > tr").remove();
			 var newStatuses = JSON.parse(statuses);
			 _.each(newStatuses.statuses, function(status) { 
            		html = "<tr>";   
                    html += "<td>";
                    html += "<p>" + (status.caseNumber !== null ? status.caseNumber :  " " ) +"</p>";
                    html +="</td>";
                    html += "<td>";
                    html += "<p>" + (status.status !== null ? status.status :  " " ) +"</p>";
                    html +="</td>";
                    html += "<td>";
                    html += "<p>" + (status.reason !== null ? status.reason :  " " ) +"</p>";
                    html +="</td>";
                    html += "<td>";
                    html += "<p>" + (status.data !== null ? status.data :  " " ) +"</p>";
                    html +="</td>";
                    html +="</tr>";
            		$("#tbl_imp_list_body").append(html);
            	 });
//            	if ( statuses !== "undefined" ){
//                	$("#tbl_imp_list").append();
//            		$("#tbl_imp_list_body").html(statuses);
//            	}

                //Fetches the initial member data and populates the table using jquery templates
            	var sTypeID = $("#selStatusType").children(':selected').text();
            	updateNewWorkPackageListByStatusType(apikey, sTypeID);
            	 
             }, 
           	error: function(error) {
           		$("#txt_response").empty().append('Error uploading file');
 				console.log("error uploading file -" + error.status);
   			}
  	}; 
  	
  	$(this).ajaxSubmit(options); 
	return false;
  }); 
  
	fillMap("map");
	setupImportDialog(apikey);
	$(".fancybox").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none',
		iframe : {
		        preload: false
		}
	});

    updateNewWorkPackageListByStatusType(apikey, "OPEN");

}

function setupImportDialog(apikey){
	   $( "#import-dialog-form" ).dialog({
		      autoOpen: false,
		      height: 500,
		      width: 700,
		      modal: true,
		      buttons: {
		    	Load: function() {
		        	$("#frm_wplist_entry").submit();
		    		//submitWkpkgCsvFile();
		      	},
		        Close: function() {
		            $( this ).dialog( "close" );
		          }
		      },
		      close: function() {
		          //clear form
		          $("#tbl_imp_list_body").empty();
		          $("#inp_wp").val("");
		      }
		  
		    });
		 
//			  	comp = 0;
//			    prod = 0;
//			 	var user;
//			    $.ajax({
//		    	async: true,
//				url: "rest/users/user/" + apikey,
//				cache: false,
//				success: function(data) {
//		    		user = data;
//				},
//			    		error: function(error) {
//			    			console.log("error updating table -" + error.status);
//			   	},
//				error: function(error) {
//					return;
//				}
//			});

//			if (user.userGroupType.groupName == "ReadOnly"){
//		  		$("#insertWP").attr('href', "#");		
//				$("#importCSV").prop("disabled", true);
//			//	$("#nav_btn_wpins").prop("disabled", true);
//			} else {
//				   $( "#imp_btn_wpl" )
//				      .click(function() {
//				        $( "#import-dialog-form" ).dialog( "open" );
//				      });
//			}
}

function retrieveWorkPackageNotesTable(workPackageID) {
	
	$.ajax({
		async: true,
		url: "rest/workpackages/" + workPackageID +"/notes",
		cache: false,
		success: function(data) {
			var table = $("#tbl_notes tbody");
			table.empty();
			$.each(data, function(idx, elem){
				table.append("<tr><td>"+ elem.user.firstName + " " + elem.user.lastName +"</td><td>"+ new Date(elem.creationDate).customFormat( "#MM#/#DD#/#YYYY#" ) +"</td><td>" + elem.note + "</td></tr>");
			});
		},
		error: function(error) {
			//console.log("error updating table -" + error.status);
		}
		
	});
}

function calculateAndPopulateDaysOpen(item, wpkey, creationDate){

	$.ajax({
		async:true,
		url: "rest/workpackages/workpackage/lastCompletionDate/" + wpkey,
		cache: false,
		//dataType: "json",
		success: function(data) {
			var days = Math.round(Math.abs(data.currentStatusCreationDate - creationDate) / (1000 * 60 *60 * 24));
			$(item).text(days);
			//$("table").trigger("update");
		},
		error: function(error) {
			$(item).text("FAIL");
			$("#indicator").hide();
//			console.log("error updating table -" + error.status);
		}
	});
}


function retrieveWorkPackageItems(workPackageID) {
	
	var workPackage = [];
	$.ajax({
		async: true,
		url: "rest/workpackages/workpackage/" + workPackageID,
		cache: false,
		success: function(workpackage) {
			var groupName = ""; 
			var firstname = "";
			var lastname = ""; 

			if (workpackage.assignedUser) {firstname = workpackage.assignedUser.firstName;}
			if (workpackage.assignedUser) {lastname = workpackage.assignedUser.lastName;}
			if (workpackage.groupType) {groupName = workpackage.groupType.groupName;}
			$("#caseNumber").text(workpackage.caseNumber);
			$("#siteName").text((workpackage.site !== null ? workpackage.site.siteName : " "));
			$("#sectorName").text((workpackage.sector !== null ? workpackage.sector.sectorName :  " " ));
//			$("#opsZone").text(workpackage.opsTurf);
			$("#technology").text((workpackage.technology !== null ? workpackage.technology.technologyName : " "));
			$("#frequency").text(workpackage.frequency !== null ? workpackage.frequency : " ");
			$("#uplink").text(workpackage.uplinkChFreq !== null ? workpackage.uplinkChFreq : " ");
			$("#rssi").text(workpackage.rSSI !== null ? workpackage.rSSI : " ");
			$("#hidden-workpackageID").val(workpackage.workPackageID);
			populateMap(workpackage);

			populateAddress(workpackage);

		},
		error: function(error) {
			//console.log("error updating table -" + error.status);
		}
		
	});
	return workPackage;
}
	
function populateWorkPackage(workPackageID){
	
	var workpackage = retrieveWorkPackageItems(workPackageID);

	
}
	
function populateAddress(workpackage){
	
	var address = workpackage.site.address;
	var city = address.city;
	var province = address.province;
	var postalCode = address.postal_code;
	$("#addressLink").text(address.address + " " + city.city + ", " + province + " " + postalCode);
	
}

function gatherWorkListWithDaysOpenData(workpackages){
	
	var wpids = [];
	
	_.each(workpackages, function(workpackage) { 
		if (workpackage.status.group.groupTypeID === 6){
			wpids.push(workpackage.workPackageID);
		}
	});
	
	if (wpids.length === 0){
		var html = buildNewWorkPackageRows(workpackages, []);
		$('#tbl_wp_list tbody').empty().append(html);
		$("table").trigger("update");
		$("tr.tablesorter-filter-row").css("display", "none");
		return;
	}

	var someData =  JSON.stringify( wpids );
	
	$.ajax({
//		headers: { 
//	        'Accept': 'application/json',
//	        'Content-Type': 'application/json' 
//	    },
		async:true,
		url: "rest/workpackages/workpackage/lastCompletionDates",
		cache: false,
		data: someData,
		dataType: 'json',
		contentType: 'application/json',
		type: 'POST',
		success: function(data) {
			dates = data;
			var html = buildNewWorkPackageRows(workpackages, dates);
			$('#tbl_wp_list tbody').empty().append(html);
			$("table").trigger("update");
			$("tr.tablesorter-filter-row").css("display", "none");
		},
		error: function(error) {
			$("#indicator").hide();
			console.log("error retrieving close dates for workpackages " + error.status);
		}

	});
}

function buildNewWorkPackageRows(workpackages, dates) {
	var result = "";
	_.each(workpackages, function(workpackage) { 

		var groupName = ""; 
		var firstname = "";
		var lastname = "";
		var daysOpen  = -1;
		var closedate;
		if (workpackage.status.group.groupTypeID === 6){
			closedate = -1;
			for (ii = 0; ii < dates.length; ii++) {
			    if (dates[ii].workPackageID === workpackage.workPackageID){
					closedate = dates[ii].creationDate;
					break;
			    } 
			}
			daysOpen = Math.round(Math.abs(closedate - workpackage.creationDate) / (1000 * 60 *60 * 24));
		} else {
			daysOpen = Math.round(Math.abs(new Date().getTime() - workpackage.creationDate) / (1000 * 60 *60 * 24));
		}
		if (workpackage.assignedUser) {firstname = workpackage.assignedUser.firstName;}

		if (workpackage.assignedUser) {lastname = workpackage.assignedUser.lastName;}

		if (workpackage.groupType) {groupName = workpackage.groupType.groupName;}



		result = result + "<tr>";   
		result += "<td onclick=\"redirectPage('" + workpackage.workPackageID + "');\" class=\"clickableCell\">";
		result += "<p>" + workpackage.site.siteName +"</p>";
		result +="</td>";

		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID +"');\">";
		result +="<p>" +  (workpackage.caseNumber !== null ? workpackage.caseNumber :  " " ) + "</p>";
		result +="</td>";

		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID +"');\">";
		result +="<p>" + new Date(workpackage.creationDate).customFormat( "#MM#/#DD#/#YY#" ) + "</p>";
		result +="</td>";
		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID +"');\">";
		result +="<p>" + groupName + "</p>";
		result +="</td>";
		result +="<td id = 'daysOpen-" + workpackage.workPackageID + "' onclick=\"reloadWorkpackage('" + workpackage.workPackageID +"');\">";
		result +="<p>" + daysOpen + "</p>";
		result +="</td>";
		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID + "');\">";
		result += "<p>" + workpackage.opsTurf + "</p>";
		result +="</td>";
		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID + "');\">";
		result +="<p>" + (workpackage.site.usid !== null ? workpackage.site.usid.usid :  " " ) + "</p>";
		result +="</td>";
		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID + "');\">";
		result +="<p>" + workpackage.status.statusName +"</p>";
		result +="</td>";
//		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID + "');\">";
//		result +="<p>" + workpackage.status.group.groupName +"</p>";
//		result +="</td>";
		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID +"');\">";
		result +="<p>" + firstname + " " + lastname + "</p>";
		result +="</td>";
		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID +"');\">";
		result +="<p>" +  workpackage.priority.priorityName + "</p>";
		result +="</td>";
//		result +="<td>";
//		if (workpackage.engNotes){
//			var title = workpackage.engNotes.replace("\"","\\\"");
//			result +="<p style=\"cursor:Pointer;\" class=\"Table-Body TB-Ctr\" title=\"" + title + "\">";
//			if (workpackage.engNotes.length > 20){
//				result += workpackage.engNotes.substring(0,20) + "...";
//			} else {
//				result += workpackage.engNotes;
//			}
//		}
//		result += "</p>";
//		result += "</td>";
		result += "</tr>";
	});

	return result;
}

/* Uses JAX-RS GET to retrieve current member list */
function updateNewWorkPackageList(apikey) {
	$.ajax({
		async:true,
		url: "rest/workpackages/" + apikey + "/jobType?statusTypeID=0",
		cache: false,
		success: function(data) {
			gatherWorkListWithDaysOpenData(data);
			//$('#tbl_wp_list tbody').append(html);
		},
		error: function(error) {
//			console.log("error updating table -" + error.status);
		}
	});
}

/* Uses JAX-RS GET to retrieve current member list */
function updateNewWorkPackageListByStatusType(apikey, statusType) {

	$.ajax({
		async: true,
		contentType: "application/json",
		type: "GET",
		url: "rest/workpackages/" + apikey + "/jobType?statusType=" + statusType,
		cache: false,
		success: function(data) {
			gatherWorkListWithDaysOpenData(data);
		},
		error: function(error) {
			console.log('List failed');
		}
	});
}

function getNewAttachmentTemplate(wpkey, divName, boxName) {
	$.ajax({
		url: "tmpl/attachment.tmpl",
		dataType: "html",
		success: function( data ) {
			$( "head" ).append( data );
			updateWorkPackageListAttachmentTable(wpkey, divName, boxName);
		}
	}); 
}

/* Uses JAX-RS GET to retrieve current member list */
function updateWorkPackageListAttachmentTable(wpkey, divName, boxName) {
	$.ajax({
		//url: "rest/workpackages/" + wpkey + "/attachments/",
		url: "rest/file/" + wpkey + "/" + getParameterByName("apikey") + "/attachments",
		async: true,
		cache: false,
		success: function(files) {
			// wp_gallery_pg_3
			
			jsonFiles = JSON.parse(files);
			
			$(divName).empty().append(_.template( $( "#attachment-tmpl" ).html(), {"files": jsonFiles.files}));
//			$(boxName).wplightbox(
//					{"loadBtnSrc":"images/lightbox_load.gif","border_e":"images/lightbox_e_6.png","border_n":"images/lightbox_n_6.png","border_w":"images/lightbox_w_6.png","border_s":"images/lightbox_s_6.png","border_ne":"images/lightbox_ne_6.png","border_se":"images/lightbox_se_6.png","border_nw":"images/lightbox_nw_6.png","border_sw":"images/lightbox_sw_6.png","closeBtnSrc":"images/lightbox_close_2.png","closeOverBtnSrc":"images/lightbox_close_over_2.png","nextBtnSrc":"images/lightbox_next_2.png","nextOverBtnSrc":"images/lightbox_next_over_2.png","prevBtnSrc":"images/lightbox_prev_2.png","prevOverBtnSrc":"images/lightbox_prev_over_2.png","blankSrc":"scripts/blank.gif","bBkgrndClickable":true,"strBkgrndCol":"#000000","nBkgrndOpacity":0.0,"strContentCol":"#ffffff","nContentOpacity":0.8,"strCaptionCol":"#555555","nCaptionOpacity":1.0,"nCaptionType":1,"bCaptionCount":false,"strCaptionFontType":"Tahoma,Serif","strCaptionFontCol":"#ffffff","nCaptionFontSz":15,"bShowPlay":false,"bAnimateOpenClose":true,"nPlayPeriod":2000}
//			);
//			 $('.wplbox_pg_3').wplightbox({
//			      "loadBtnSrc":"images/lightbox_load.gif",
//			      "border_e":"images/lightbox_e_6.png",
//			      "border_n":"images/lightbox_n_6.png",
//			      "border_w":"images/lightbox_w_6.png",
//			      "border_s":"images/lightbox_s_6.png",
//			      "border_ne":"images/lightbox_ne_6.png",
//			      "border_se":"images/lightbox_se_6.png",
//			      "border_nw":"images/lightbox_nw_6.png",
//			      "border_sw":"images/lightbox_sw_6.png",
//			      "closeBtnSrc":"images/lightbox_close_2.png",
//			      "closeOverBtnSrc":"images/lightbox_close_over_2.png",
//			      "nextBtnSrc":"images/lightbox_next_2.png",
//			      "nextOverBtnSrc":"images/lightbox_next_over_2.png",
//			      "prevBtnSrc":"images/lightbox_prev_2.png",
//			      "prevOverBtnSrc":"images/lightbox_prev_over_2.png",
//			      "blankSrc":"scripts/blank.gif",
//			      "bBkgrndClickable":true,
//			      "strBkgrndCol":"#000000",
//			      "nBkgrndOpacity":0.0,
//			      "strContentCol":"#ffffff",
//			      "nContentOpacity":0.8,
//			      "strCaptionCol":"#555555",
//			      "nCaptionOpacity":1.0,
//			      "nCaptionType":1,
//			      "bCaptionCount":false,
//			      "strCaptionFontType":"Verdana,Serif",
//			      "strCaptionFontCol":"#ffffff",
//			      "nCaptionFontSz":15,
//			      "bShowPlay":false,
//			      "bAnimateOpenClose":true,
//			      "nPlayPeriod":2000
//			    });
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
}

function gotoWorkPackageInsert(userID){
	getUser(userID);
}

function getUser(userID){
  	comp = 0;
    prod = 0;
 	var user;
    $.ajax({
    	async: true,
    	url: "rest/users/" + getParameterByName("userID"),
    	cache: false,
    	success: function(data) {
    		user = data;
    		window.location.replace('wp_insert.html?userID='+ userID + '&companyID=' + user.company.companyID + '&productID=' + user.product.productID + '&apikey=' + user.apikey);
    	},
    	error: function(error) {
    		console.log("error updating table -" + error.status);
    	}
    });
    return user;
}

function fillMap(divName){
	var mapDiv =  document.getElementById(divName);
    var latlng = new google.maps.LatLng(33.9405, -84.2255);
    var options =
    {                                 
    		zoomControl: true,
    		scaleControl: true,                        
    	    center: latlng,
    	    zoom: 15,
    	    mapTypeId: google.maps.MapTypeId.ROADMAP,
    	    disableDefaultUI: true,
    	    draggable: true,
    	    draggableCursor: 'move'
    };
    
    map = new google.maps.Map(mapDiv, options);
    map.set("streetViewControl", false);

    google.maps.event.addListener(map, 'zoom_changed', function() {
        zoomChangeBoundsListener = 
            google.maps.event.addListener(map, 'bounds_changed', function(event) {
                if (this.getZoom() > 15 && this.initialZoom == true) {
                	
                    // Change max/min zoom here
                    this.setZoom(15);
                    this.initialZoom = false;
                    
                }
                
            google.maps.event.removeListener(zoomChangeBoundsListener);
            
        });
    });
    map.initialZoom = true;
    
}

$("#mapButton").click(function() {
    $( "#map-outer" ).dialog( "open" );
});


function populateMap(workpackage){
    	
	deleteMarkers();
	addMarker(workpackage.site.latitude, workpackage.site.longitude, workpackage.site.siteName, 'map_canvas', null);
	fitToMarkers(markers);
}



/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
///////////                   GOOGLE MAP FUNCTIONS                        ///////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

function addMarker(lat, lng, title, mapDiv, image){

//	var options =
//	{
//	mapTypeId: google.maps.MapTypeId.ROADMAP,
//	disableDefaultUI: true,
//	draggableCursor: 'move'
//	};

//	map = new google.maps.Map( document.getElementById("map_canvas"), options);

	var markerR = new google.maps.Marker({
		position: new google.maps.LatLng(lat, lng),
		map: map,
		title: title,
		icon: 'images/markers/circles.png'
	});

	markers.push(markerR);
	var infoContents = '<h3>Directions</h>';
	var infowindowR = new google.maps.InfoWindow({content: infoContents});        
	google.maps.event.addListener(markerR, 'click', function() {
		infowindowR.open(window.map, markerR);
	});
}

function fitToMarkers(markers){

	map.initialZoom = true;
	var bounds = new google.maps.LatLngBounds();
//	for ( var i in markers) {
	for(var i = 0; i < markers.length; i++) {
		var latlong = markers[i].getPosition();
		bounds.extend(latlong);
	}

//	Don't zoom in too far on only one marker
	if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
		var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.50, bounds.getNorthEast().lng() + 0.50);
		var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.50, bounds.getNorthEast().lng() - 0.50);
		bounds.extend(extendPoint1);
		bounds.extend(extendPoint2);
	}
	map.fitBounds(bounds);

}




//Sets the map on all markers in the array.
function setAllMap(map) {
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
	}
}

//Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
	setAllMap(null);
}

//Deletes all markers in the array by removing references to them.
function deleteMarkers() {
	clearMarkers();
	markers = [];
}

function clearMarkers(){
	setAllMap(null);
}

//Shows any markers currently in the array.
function showMarkers() {
	setAllMap(map);
}


function centerOnMarkers(){
//	Make an array of the LatLng's of the markers you want to show

	var LatLngList = [];
	for (var i = 0; i < markers.length; i++ ) {
		LatLngList.push(markers[i].getPosition());
	}

	var bounds = new google.maps.LatLngBounds ();
//	Go through each...
	for (var i = 0, LtLgLen = LatLngList.length; i < LtLgLen; i++) {
//		And increase the bounds to take this point
		bounds.extend (LatLngList[i]);
	}
//	Fit these bounds to the map
	map.fitBounds (bounds);
}
