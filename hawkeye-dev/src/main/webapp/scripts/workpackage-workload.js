
//Get value of tag from query string...
function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function redirectRtcReportPage(){
	window.location.replace("/rptRTC.html?apikey=" + getParameterByName("apikey"));
}

function redirectResReportPage(){
	window.location.replace("/rptRes.html?apikey=" + getParameterByName("apikey"));
}

function logout()
{
	$.ajax({
		url: "rest/users/logout/" + getParameterByName("apikey"),
		cache: false,
		success: function(data) {
			window.location.replace("login.html");
		},
		error: function(error) {
			
			window.location.replace("login.html");
			//console.log("error updating table -" + error.status);
		}
	});
}

function redirectWorkPackageListPage(){
	window.location.replace("work-package-list.html?apikey=" + getParameterByName("apikey"));
}

function redirectDashboardPage(){
	window.location.replace("/dashboard.html?apikey=" + getParameterByName("apikey"));
}

function redirectWorkloadPage(){
	window.location.replace("workpackage-workload.html?apikey=" + getParameterByName("apikey"));
}


function getUserFromAPI(apikey){
	var username = [];
	$.ajax({
		async: true,
		url: "rest/users/user/" + apikey,
		cache: false,
		success: function(data) {
			$("#userNameDropDown").html(data.firstName + "<b class='caret'></b>");
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
	return username;
	
}
 
function init () {

	var apikey = getParameterByName("apikey");
// If the Ajax calls are set to async: true, then this next function will display an indicator,
//	representing the browser being busy...
	
	$(document).ajaxStart(function(){
		$("#indicator").show();
	}).ajaxStop(function(){
		$("#indicator").hide();
	});

	getUserFromAPI(apikey);
	
//	var localJobTypes = retrieveGroupTypes(getParameterByName("apikey"));	
	
	$("#selectJobType").change(function(){

		var jobtype = $("#selectJobType").val();
		updateWorkloadTechSummaryTable(apikey,0,5, jobtype);
		updateWorkloadMktSummaryTable(apikey,0,5, jobtype);
		updateWorkloadClosedTable(apikey, jobtype);
	});
	
// These two pieces of code should be added to refresh the detail page 
//    when the tab for each section is hit...  However, more testing needs to be done...

//	$("#feLink").click(function(){
//		updateWorkloadDetailForTechTable(apikey,0,5, 'ALL', jobtype);
//	});
	
//	$("#marketLink").click(function(){
//		updateWorkloadDetailForMktTable(apikey, 0, 5, 'All', + jobtype);
//    });
	
	$("#logoutButton").click(function(){
    	logout();
    });
	
    $("#workPackageNav").click(function(){
    	redirectWorkPackageListPage();
    });

    $("#dashboardLink").click(function(){
    	redirectDashboardPage();
    });
    
    $("#workloadLink").click(function(){
    	redirectWorkloadPage();
    });


    $("#rptRTC").click(function(){
    	redirectRtcReportPage();
    });
    
    $("#rptRES").click(function(){
    	redirectResReportPage();
    });

	var redirectFlag = false;
	var validateUserResult = $.ajax({
		async: true,
		url: "rest/users/validateApiKey/" + apikey,
		cache: false,
		success: function(data) {
			return;
		},
		error: function(error) {
			return;
		}
	});

	validateUserResult.done(
		processWorkloadPage
		).fail(function(){
		redirectFlag = true;
		window.location.replace("login.html");
	});
	if(redirectFlag == true)
		return;
}

function processWorkloadPage(){
	var apikey = getParameterByName("apikey");
	var jobSelected = retrieveGroupTypes(apikey);
	
	jobSelected.done(function(data){
		var jobtype = $("#selectJobType").val();
		updateWorkloadTechSummaryTable(apikey,0,5,0, jobtype);
		updateWorkloadMktSummaryTable(apikey,0,5,0, jobtype);
		updateWorkloadClosedTable(apikey,0, jobtype);
	});
	


//	var loc = "dashboard.html?userID=" + getParameterByName("userID") + "&companyID=" + comp + "&productID=" + prod;
//	$("#nav_btn_db").attr('href', loc);

//	var loc = "workload.html?userID=" + getParameterByName("userID") + "&companyID=" + comp + "&productID=" + prod +  "&apikey=" + getParameterByName("apikey");
//	$("#nav_btn_wl").attr('href', loc);

//	var hrf = "wp_list.html?userID=" + getParameterByName("userID") +  "&apikey=" + getParameterByName("apikey");
//	$("#nav_btn_wpl").attr('href', hrf);

	var mapDiv =  document.getElementById("map_canvas");
	var latlng = new google.maps.LatLng(33.9405, -84.2255);
	var options =
	{         
			zoomControl: true,
			scaleControl: true,                        
			center: latlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true,
			draggable: true,
			draggableCursor: 'move'
	};
	map = new google.maps.Map(mapDiv, options);
	map.set("streetViewControl", false);

	google.maps.event.addListener(map, 'zoom_changed', function() {
		zoomChangeBoundsListener = 
			google.maps.event.addListener(map, 'bounds_changed', function(event) {
				if (this.getZoom() > 15 && this.initialZoom == true) {
					// Change max/min zoom here
					this.setZoom(15);
					this.initialZoom = false;
				}
				//google.maps.event.removeListener(zoomChangeBoundsListener);
			});
	});

	map.initialZoom = true;
	
}


function retrieveGroupTypes(apikey){
	var results = $.ajax({
				url: "rest/utility/jobtypesByCompanyProduct/" + apikey,
				cache: false,
				async: true,
				success: function(datas) {
					var _select = $('<select>');
					_select.append(
					$('<option></option>').val(0).html("All"));
					if(datas && datas.length > 0) {
						$.each(datas, function(indx, job) {
							_select.append(
									$('<option></option>').val(job.groupTypeID).html(job.groupName));
						});
						$('#selectJobType').empty().append(_select.html());
					}
				},
				error: function(error) {
					//console.log("error updating table -" + error.status);
				}
			});
	return results;
}

function validateUser() {
	$.ajax({
		async: true,
		url: "rest/users/validateApiKey/" + getParameterByName("apikey"),
		cache: false,
		success: function(data) {
			return;
		},
		error: function(error) {
			redirectFlag = true;
			window.location.replace("login.html");
		}
	});
}

function redirectPage(data){
	var userID = 0;
	$.ajax({
		async: true,
		url: "rest/users/user/" + getParameterByName("apikey"),
		cache: false,
		success: function(data) {
			userID = data;
		}
	});

	window.location.replace("work-package-detail.html?workPackageID=" + data + "&apikey=" + getParameterByName("apikey"));
}

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
///////////                     WORKLOAD FUNCTIONS                        ///////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

/* Uses JAX-RS GET to retrieve current member list */
function updateWorkloadClosedTable(apikey, jobtype) {
	
	var inputs = {
			jobTypeID: jobtype
	};
	
	
	$.ajax({
		async: true,
		url: "rest/workload/closeSummary/" + apikey,
		data: inputs,
		cache: false,
		success: function(data) {

			var html = buildWorkloadClosedDashboard(data);

			$('#tbl_wl_ytd').empty().append(html);
		},
		error: function(error) {
//			console.log("error updating table -" + error.status);
		}
	});
}

function buildWorkloadClosedDashboard(closes) {

	var result1 = '<tr><th><p>Close - Year To Date</p></th><th><p>Cases</p></th></tr>';
	var result2 = '<tr><th><p>Average - Receipt To Clear</p></th><th><p>Days</p></th></tr>';

	_.each(closes, function(close) { 
		result1 += '<tr>';
		result1 += '<td>' + close.lastName + ", " + close.firstName + '</td>';
		result1 += '<td>' + close.cases     + '</td>';
		result1 += '</tr>';

		result2 += '<tr>';
		result2 += '<td>' + close.lastName + ", " + close.firstName + '</td>';
		result2 += '<td>' + close.days     + '</td>';
		result2 += '</tr>';
	});

	result1 += '<tr><td></td><td></td></tr>';
	result2 += '<tr><td></td><td></td></tr>';

	return result1 + result2;

}

/* Uses JAX-RS GET to retrieve current member list */
function updateWorkloadTechSummaryTable(apikey, userID, groupID, jobtype) {

	if (jobtype == undefined || jobtype == null){
		jobtype = 0;
	}
	
	var inputs = {
			userID: userID,
			groupTypeID: groupID,
			jobTypeID: jobtype
	}

	$.ajax({
		async: true,
		url: "rest/workload/summaryTech/" + apikey,
		data: inputs,
		cache: false,
		success: function(data) {

			var html = buildWorkloadTechSummaryDashboard(data, apikey, groupID, jobtype);

			$('#tbl_wl_tech').empty().append(html);
			updateWorkloadDetailForTechTable(apikey,0,5, 'ALL', jobtype);
		},
		error: function(error) {
//			console.log("error updating table -" + error.status);
		}
	});
}

function buildWorkloadTechSummaryDashboard(summaries, apikey, groupID, jobtype) {

	var result1 = '';  //<tr  class="td_blue"><th><p>Assigned</p></th><th><p></p></th><th><p>Work</p></th></tr>';

	var isHead = true;
	var currentID = 0;
	var count = 0;

	_.each(summaries, function(summary) {

		if (currentID != summary.userid){

			result1 += '<tr class="td_blue">';
			currentID = summary.userid;

			isHead = false;
			result1 += '<td colspan="2"><a style="text-decoration: underline;cursor:pointer;" onclick="updateWorkloadDetailForTechTable(\'' + apikey + '\',' + summary.userid + ',' + groupID + ', \'ALL\', ' + jobtype + ')"><p>' + summary.lastName + ", " + summary.firstName + '</p></a></td><td></td></tr>';
			result1 += '<tr class="td_gray">';
			result1 += '<td colspan="2" class="ta_center"><p>    ' + summary.hierarchyName     + '</p></td>';
			result1 += '<td class="ta_center"><p>' + summary.numberRecords     + '</p></td>';    	
			result1 += '</tr>';
		} else
		{
			result1 += '<tr class="td_gray">';
			result1 += '<td colspan="2" class="ta_center"><p>    ' + summary.hierarchyName     + '</p></td>';
			result1 += '<td class="ta_center"><p>' + summary.numberRecords     + '</p></td>';    	
			result1 += '</tr>';
			//count++;
		}
		count = count + summary.numberRecords;
	});

	result1 += '<tr class="td_white"><td colspan="2"><p><a style="text-decoration: underline;cursor:pointer;" onclick="updateWorkloadDetailForTechTable(\'' + apikey + '\', 0,' + groupID + ', \'ALL\', ' + jobtype + ')">Total</a></p></td><td class="ta_center">' + count + '</td></tr>';
	return result1;

}

/* Uses JAX-RS GET to retrieve current member list */
function updateWorkloadMktSummaryTable(apikey, userID, groupID, jobtype) {

	var inputs = {
			userID: userID,
			groupTypeID: groupID,
			jobTypeID: jobtype
	}

	$.ajax({
		async: true,
		url: "rest/workload/summaryMkt/" + apikey,
		data: inputs,
		cache: false,
		success: function(data) {

			var html = buildWorkloadMktSummaryDashboard(data, apikey, groupID, jobtype);

			$('#tbl_wl_market').empty().append(html);
		},
		error: function(error) {
//			console.log("error updating table -" + error.status);
		}
	});
}

function buildWorkloadMktSummaryDashboard(summaries, apikey, groupID, jobtype) {

	var result1 = '';  //<tr  class="td_blue"><th><p>Assigned</p></th><th><p></p></th><th><p>Work</p></th></tr>';

	var isHead = true;
	var currentID = 0;
	var count = 0;

	_.each(summaries, function(summary) {

		if (currentID != summary.hierarchyName){

//			if ((!isHead) && (count > 3)){
//			result1 += '<tr class="td_white">';
//			result1 += '<td></td>';
//			result1 += '<td></td>';
//			result1 += '<td><p>' + count + '</p></td>';    	
//			result1 += '</tr>';
//			count = 0;
//			}

			result1 += '<tr class="td_blue">';
			currentID = summary.hierarchyName;

			isHead = false;
			result1 += '<td colspan="2"><a style="text-decoration: underline;cursor:pointer;" onclick="updateWorkloadDetailForMktTable(\'' + apikey + '\', 0,' + groupID + ', \'' + summary.hierarchyName + '\', ' + jobtype + ')"><p>' + summary.hierarchyName + '</p></a></td><td></td></tr>';
			result1 += '<tr class="td_gray">';
			result1 += '<td colspan="2" class="ta_center"><p>    ' + summary.lastName + ", " + summary.firstName + '</p></td>';
			result1 += '<td class="ta_center"><p>' + summary.numberRecords     + '</p></td>';    	
			result1 += '</tr>';
		} else
		{
			result1 += '<tr class="td_gray">';
			result1 += '<td colspan="2" class="ta_center"><p>    ' + summary.lastName + ", " + summary.firstName + '</p></td>';
			result1 += '<td class="ta_center"><p>' + summary.numberRecords     + '</p></td>';    	
			result1 += '</tr>';
			//count++;
		}
		count = count + summary.numberRecords;
	});

	result1 += '<tr class="td_white"><td colspan="2"><p><a style="text-decoration: underline;cursor:pointer;" onclick="updateWorkloadDetailForMktTable(\'' + apikey + '\', 0,' + groupID +', \'All\', ' + jobtype + ')">Total</a></p></td><td class="ta_center"><p>' + count + '</p></td></tr>';

	return result1;

}

/* Uses JAX-RS GET to retrieve current member list */
function updateWorkloadDetailForTechTable(apikey, userID, groupID, marketName, jobtype) {

	var inputs = {
			userID: userID,
			groupTypeID: groupID,
			marketName: '',
			jobTypeID: jobtype
	};

	$.ajax({
		async: true,
		url: "rest/workload/detail/" + apikey,
		data: inputs,
		cache: false,
		success: function(data) {

			var html = buildWorkloadDetailForTechDashboard(data);

			$('#wl_indiv_cont').empty().append(html);
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
}

function buildTechHeader(){
	var result1 = '';

	result1 += '<table  class="table-condensed table table-hover table-striped">';
	result1 += '<thead>';
	result1 += '<tr class="tablesorter-headerRow">';
	result1 += '<th data-column="0" tabindex="0">Ticket #</th>';
	result1 += '<th data-column="2"  tabindex="0">Site</th>';
	result1 += '<th data-column="1" tabindex="0">Date</th>';
	result1 += '<th data-column="4" tabindex="0">Status</th>';
	result1 += '<th data-column="3"  tabindex="0">Market</th>';
//    result1 += '<th data-column="5" class="tablesorter-header" tabindex="0">Assigned To</th>';
    result1 += '<th data-column="6"  tabindex="0">Days Open</th>';
    result1 += '</tr>';
    result1 += '</thead>';
	
	return result1;
}

function buildMktHeader(){
	var result1 = '';

	result1 += '<table  class="table-condensed table table-hover table-striped">';
	result1 += '<thead>';
	result1 += '<tr class="tablesorter-headerRow">';
	result1 += '<th data-column="0" tabindex="0">Ticket #</th>';
	result1 += '<th data-column="2" tabindex="0">Site</th>';
	result1 += '<th data-column="1" tabindex="0">Date</th>';
	result1 += '<th data-column="4" tabindex="0">Status</th>';
    result1 += '<th data-column="5" tabindex="0">Assigned</th>';
    result1 += '<th data-column="6" tabindex="0">Days Open</th>';
    result1 += '</tr>';
    result1 += '</thead>';
	
	return result1;
}


function buildWorkloadDetailForTechDashboard(summaries) {

	var result1 = '';
	var  count = 0;
	var currentID = "";
	var firstLine = true;

// THIS IS OLD CODE FROM VERSION 0.9	
//	result1 += '<table id="tbl_wl_indiv" class="wl_tbl wl_indiv_tbl">';
//	result1 += '<col style="width:8%;"><col style="width:10%;"><col style="width:6%;"><col style="width:22%;"><col style="width:47%;">';
//	result1 += '<thead><tr class="td_lightblue ta_center"><th><p>Case</p></th><th><p>Site</p></th><th>';
//	result1 += '<p>Date</p></th><th><p>Status</p></th><th><p>Ops Turf</p></th>';
//	result1 += '<th><p>Notes</p></th><th><p>Days Open</p></th></tr></thead>';

	result1 = buildTechHeader();
	
	deleteMarkers();

	_.each(summaries, function(summary) {
		count++;
		if (currentID != summary.userid){
			if (!firstLine){
				result1 += '</tbody>';
			}
			firstLine = false;
			result1 += '<tbody>';
			result1 += '<tr>';
			result1 += '<td colspan="6"><h3>' + summary.lastName + ', ' + summary.firstName + '<h3></td>';
			result1 += '</tr>';

			currentID = summary.userid;
		}
		result1 += '<tr>';
		result1 += '<td onclick=\"redirectPage(' + summary.workpackageid + ');\"><p>' + summary.caseNumber + '</p></td>';
		result1 += '<td><p>' + summary.siteName + '</p></td>';
		result1 += '<td><p>' + new Date(summary.creationdate).customFormat( "#MM#/#DD#/#YYYY#" ) + '</p></td>';
		result1 += '<td><p>' + summary.status + '</p></td>';
		result1 += '<td><p>' + summary.turf + '</p></td>';

//		result1 +="<td>";
//		result1 +="<p style=\"cursor:Pointer;\" title=\"" + summary.notes + "\">";
//		if (summary.notes.length > 20){
//			var str = summary.notes;
//			result1 += str.substring(0,20) + "...";
//		} else {
//			result1 += summary.notes;
//		}
//		result1 += "</p>";
//		result1 += "</td>";


//		result1 += '<td><p>' + summary.notes + '</p></td>';
		result1 += '<td><p>' + summary.daysOpen + '</p></td>';
		result1 += '</tr>';

		addMarker(summary.latitude, summary.longitude, summary.siteName, 'map_canvas', null);
		fitToMarkers(markers);
	});
	if (count > 0)
		result1 += '</tbody></table>';

	centerOnMarkers();
	return result1;
}


/* Uses JAX-RS GET to retrieve current member list */
function updateWorkloadDetailForMktTable(apikey, userID, groupID, marketName, jobtype) {

	var inputs = {
			userID: userID,
			groupTypeID: groupID,
			marketName: marketName,
			jobTypeID: jobtype
	};

	$.ajax({
		async: true,
		url: "rest/workload/detail/" + apikey,
		data: inputs,
		cache: false,
		success: function(data) {

			var html = buildWorkloadDetailForMktDashboard(data);

			$('#wl_indiv_cont').empty().append(html);
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
}

function buildWorkloadDetailForMktDashboard(summaries) {

	var result1 = '';
	var  count = 0;
	var currentID = "";
	var firstLine = true;
	result1 = buildMktHeader();
	deleteMarkers();

	_.each(summaries, function(summary) {
		count++;
		if (currentID != summary.turf){
			if (!firstLine){
				result1 += '</tbody>';
			}
			firstLine = false;
			result1 += '<tbody>';
			result1 += '<tr class="td_blue">';
			result1 += '<td colspan="6"><h3>' + summary.turf + '<h3></td>';
			result1 += '</tr>';

			currentID = summary.turf;
		}
		result1 += '<tr>';
		result1 += '<td onclick=\"redirectPage(' + summary.workpackageid + ');\"><p>' + summary.caseNumber + '</p></td>';
		result1 += '<td><p>' + summary.siteName + '</p></td>';
		result1 += '<td><p>' + new Date(summary.creationdate).customFormat( "#MM#/#DD#/#YYYY#" ) + '</p></td>';
		result1 += '<td><p>' + summary.status + '</p></td>';
		result1 += '<td><p>'  + summary.lastName + ', ' + summary.firstName +  '</p></td>';
		result1 += '<td><p>' + summary.daysOpen + '</p></td>';
		result1 += '</tr>';

		addMarker(summary.latitude, summary.longitude, summary.siteName, 'map_canvas', null);
		fitToMarkers(markers);
	});
	if (count > 0)
		result1 += '</tbody></table>';

	centerOnMarkers();
	return result1;
}

//Formats all dates to the following values...

/*
 * 
 * 
token:     description:             example:
#YYYY#     4-digit year             1999
#YY#       2-digit year             99
#MMMM#     full month name          February
#MMM#      3-letter month name      Feb
#MM#       2-digit month number     02
#M#        month number             2
#DDDD#     full weekday name        Wednesday
#DDD#      3-letter weekday name    Wed
#DD#       2-digit day number       09
#D#        day number               9
#th#       day ordinal suffix       nd
#hhh#      military/24-based hour   17
#hh#       2-digit hour             05
#h#        hour                     5
#mm#       2-digit minute           07
#m#        minute                   7
#ss#       2-digit second           09
#s#        second                   9
#ampm#     "am" or "pm"             pm
#AMPM#     "AM" or "PM"             PM
 */

Date.prototype.customFormat = function(formatString){
	var YYYY,YY,MMMM,MMM,MM,M,DDDD,DDD,DD,D,hhh,hh,h,mm,m,ss,s,ampm,AMPM,dMod,th;
	var dateObject = this;
	YY = ((YYYY=dateObject.getFullYear())+"").slice(-2);
	MM = (M=dateObject.getMonth()+1)<10?('0'+M):M;
	MMM = (MMMM=["January","February","March","April","May","June","July","August","September","October","November","December"][M-1]).substring(0,3);
	DD = (D=dateObject.getDate())<10?('0'+D):D;
	DDD = (DDDD=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][dateObject.getDay()]).substring(0,3);
	th=(D>=10&&D<=20)?'th':((dMod=D%10)==1)?'st':(dMod==2)?'nd':(dMod==3)?'rd':'th';
	formatString = formatString.replace("#YYYY#",YYYY).replace("#YY#",YY).replace("#MMMM#",MMMM).replace("#MMM#",MMM).replace("#MM#",MM).replace("#M#",M).replace("#DDDD#",DDDD).replace("#DDD#",DDD).replace("#DD#",DD).replace("#D#",D).replace("#th#",th);

	h=(hhh=dateObject.getHours());
	if (h==0) h=24;
	if (h>12) h-=12;
	hh = h<10?('0'+h):h;
	AMPM=(ampm=hhh<12?'am':'pm').toUpperCase();
	mm=(m=dateObject.getMinutes())<10?('0'+m):m;
	ss=(s=dateObject.getSeconds())<10?('0'+s):s;
	return formatString.replace("#hhh#",hhh).replace("#hh#",hh).replace("#h#",h).replace("#mm#",mm).replace("#m#",m).replace("#ss#",ss).replace("#s#",s).replace("#ampm#",ampm).replace("#AMPM#",AMPM);
}


/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
///////////                   GOOGLE MAP FUNCTIONS                        ///////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

function addMarker(lat, lng, title, mapDiv, image){

	var options =
	{
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true,
			draggableCursor: 'move'
	};

//	map = new google.maps.Map( document.getElementById("map_canvas"), options);

	var markerR = new google.maps.Marker({
		position: new google.maps.LatLng(lat, lng),
		map: map,
		title: title,
		icon: 'images/markers/circles.png'
	});

	markers.push(markerR);
	var infoContents = '<h3>Cell Tower</h>';
	var infowindowR = new google.maps.InfoWindow({content: infoContents});        
	google.maps.event.addListener(markerR, 'click', function() {
		infowindowR.open(window.map, markerR);
	});
}

function fitToMarkers(markers){

	map.initialZoom = true;
	var bounds = new google.maps.LatLngBounds();
//	for ( var i in markers) {
	for(var i = 0; i < markers.length; i++) {
		var latlong = markers[i].getPosition();
		bounds.extend(latlong);
	}

//	Don't zoom in too far on only one marker
	if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
		var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.50, bounds.getNorthEast().lng() + 0.50);
		var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.50, bounds.getNorthEast().lng() - 0.50);
		bounds.extend(extendPoint1);
		bounds.extend(extendPoint2);
	}
	map.fitBounds(bounds);

}

//Sets the map on all markers in the array.
function setAllMap(map) {
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
	}
}

//Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
	setAllMap(null);
}

//Deletes all markers in the array by removing references to them.
function deleteMarkers() {
	clearMarkers();
	markers = [];
}


function centerOnMarkers(){
//Make an array of the LatLng's of the markers you want to show

var LatLngList = [];
for (var i = 0; i < markers.length; i++ ) {
LatLngList.push(markers[i].getPosition());
}

var bounds = new google.maps.LatLngBounds ();
//  Go through each...
for (var i = 0, LtLgLen = LatLngList.length; i < LtLgLen; i++) {
//  And increase the bounds to take this point
bounds.extend (LatLngList[i]);
}
//  Fit these bounds to the map
map.fitBounds (bounds);
}
