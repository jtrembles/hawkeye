//Get value of tag from query string...
function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function logout()
{
	$.ajax({
		url: "rest/users/logout/" + getParameterByName("apikey"),
		cache: false,
		success: function(data) {
			window.location.replace("login.html");
		},
		error: function(error) {
			window.location.replace("login.html");
			//console.log("error updating table -" + error.status);
		}
	});
}

function getUserFromAPI(apikey){
	var username = [];
	$.ajax({
		async:false,
		url: "rest/users/user/" + apikey,
		cache: false,
		success: function(data) {
			username = data.firstName;
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
	return username;
	
}

function redirectDashboardPage(){
	window.location.replace("/dashboard.html?apikey=" + getParameterByName("apikey"));
}

function redirectWorkloadPage(){
	window.location.replace("workpackage-workload.html?apikey=" + getParameterByName("apikey"));
}

function redirectWorkPackageListPage(){
	window.location.replace("work-package-list.html?apikey=" + getParameterByName("apikey"));
}


function init(){
	$("#h_txt_apiKey").val(getParameterByName("apikey"));
	
	$("#userNameDropDown").html(getUserFromAPI(getParameterByName("apikey")) + "<b class='caret'></b>");
	
    $("#workPackageNav").click(function(){
    	redirectWorkPackageListPage();
    });
    
    $("#dashboardLink").click(function(){
    	redirectDashboardPage();
    });
    
    $("#workloadLink").click(function(){
    	redirectWorkloadPage();
    });
    
	var redirectFlag = false;
	
	$.ajax({
    	async: false,
		url: "rest/users/validateApiKey/" + getParameterByName("apikey"),
		cache: false,
		success: function(data) {
			return;
		},
	    error: function(error) {
		    redirectFlag = true;
//	    	window.location.replace("login.html");
	   	}
	});
	
	if(redirectFlag == true)
		return;
	
	var comp = 0;
    var prod = 0;
    var user;
	$.ajax({
    	async: false,
		url: "rest/users/user/" + getParameterByName("apikey"),
		cache: false,
		success: function(data) {
    		user = data;
			comp = data.company.companyID;
			prod = data.product.productID;
		},
	    		error: function(error) {
	    			console.log("error updating table -" + error.status);
	   	},
		error: function(error) {
			return;
		}
	});

	$("#txt_technologyID_h").val();
	
	$("#site").on("focus", function (event) {

		var response = [];
		var value = $("#site").val();
		
		if (value === ""){
			return;
			
		}
		
        $.ajax({
            url: "rest/utility/sites/autocomplete/" + comp + "/" + prod,
            dataType: "json",
            data: {
              //featureClass: "P",
              //style: "full",
              //maxRows: 20,
              term: value
            },
            success: function( data ) {
              response = $.map( data, function( item ) {
                return {
                  label: item.siteName,
                  value: item.siteName,
                  id:	 item.siteID
                };
              });
              if (response.length === 1)
              $("#txt_siteID_h").val(response[0].id);
         }
        });	
    });
	
    $( "#site" ).autocomplete({
        source: function( request, response ) {
          $.ajax({
            url: "rest/utility/sites/autocomplete/" + comp + "/" + prod,
            dataType: "json",
            data: {
              //featureClass: "P",
              //style: "full",
              //maxRows: 20,
              term: request.term
            },
            success: function( data ) {
              response( $.map( data, function( item ) {
                return {
                  label: item.siteName,
                  value: item.siteName,
                  id:	 item.siteID
                }
          }));
         }
        });
        },
        minLength: 4,
        change: function(event, ui) {
            if(ui.item == null) {
            	$("#txt_siteID_h").val("");
			} else {
				$("#txt_siteID_h").val(ui.item.id);
			}
			
        },
        select: function( event, ui ) {
        	$("#txt_siteID_h").val(ui.item.id);
        	//$("#site").val(ui.item.value);
        }
      });
    
    var apikey = getParameterByName("apikey");
	getTechnologiesForDropDown(apikey, "#cmb_technology");
	getJobTypesForDropDown(apikey, "#cmb_jobType");
	getPrioritiesForDropDown(apikey, "#cmb_priority");
	$("#h_txt_userID").val(0);
	$("#h_txt_apiKey").val(getParameterByName("apikey"));
	
	$('#frm_wpdetails_entry').submit(function() {
    	if(redirectFlag == true)
    		return;

    	var success = false;
    	var wkpkgID = -1;
    	var memberData = $(this).serializeObject();

//    	if(document.getElementById("txt_siteID_h").value == "") {
//        	$("#txt_response").empty().append('Error: Invalid Site "' + document.getElementById("txt_siteID_h").value + '"');
//        	return false;
//    	}
    	
    	
    	// prepare Options Object 
    	var options = { 
    		   url: 'rest/workpackages/insert',
               type: 'POST',
               async: false,
               cache: false,
               data: JSON.stringify(memberData),
               processData: false,
               clearForm: true,
               success: function (returndata) {
            	$("#txt_response").empty().append(returndata);
            	success = true;
            	wkpkgID = returndata.workPackageID;

            	window.location.replace("work-package-detail.html?workPackageID=" + wkpkgID + "&apikey="+ getParameterByName("apikey"));
            	
//            	var url = 'wp_list.html?workPackageID=' + wkpkgID + 'userID=' +  getParameterByName("userID");
//            	var form = $('<form action="' + url + '" method="GET">' +
//            	  '<input type="text" name="api_url" value="' + Return_URL + '" />' +
//            	  '</form>');
//            	$('body').append(form);
//            	$(form).submit();
               }, 
             	error: function(error) {
             		$("#txt_response").empty().append('Error inserting WorkPackage:' + error.responseText );
     			}
    	}; 
		jQuery.validator.setDefaults({
			debug:true,
        	onsubmit: false});
    	var form = $("#frm_wpdetails_entry");
    	form.validate();

    	if(document.getElementById("txt_siteID_h").value == "") {
        	$("#txt_response").empty().append('Error: Unknown Site "' + document.getElementById("site").value + '"');
    	}
    	else if (form.valid()){
       		$(this).ajaxSubmit(options);
		}
    	return false;           		
    });
    
    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
}



function getTechnologiesForDropDown(apikey, dropDownControl){

	$.ajax({
		url: "rest/utility/technologies/" + apikey,
		cache: false,
		async: false,
		success: function(datas) {
			var $el = $(dropDownControl);
			$el.empty(); // remove old options
			$.each(datas, function() {
				$el.append($("<option></option>")
						.attr("value", this.technologyID).text(this.technologyName + " - " + this.type));
			});
		},
		error: function(error) {
			//	console.log("error updating table -" + error.status);
		}
	});
}

function getPrioritiesForDropDown(apikey, dropDownControl){

	$.ajax({
		url: "rest/utility/priorities/" + apikey,
		cache: false,
		async: false,
		success: function(datas) {
			var $el = $(dropDownControl);
			$el.empty(); // remove old options
			$.each(datas, function() {
				$el.append($("<option></option>")
						.attr("value", this.priorityID).text(this.priorityName));
			});
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
}

function getJobTypesForDropDown(apikey, dropDownControl){

	$.ajax({
		url: "rest/utility/jobtypesByCompanyProduct/" + apikey,
		cache: false,
		async: false,
		success: function(datas) {
			var $el = $(dropDownControl);
			$el.empty(); // remove old options
			$.each(datas, function() {
				$el.append($("<option></option>")
						.attr("value", this.groupTypeID).text(this.groupName));
			});
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
}