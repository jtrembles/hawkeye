  function getEngNotes(str)
{
  if (str=="")
  {
    document.getElementById("engNotes").innerHTML="";
    return;
  }
  xmlhttp=new XMLHttpRequest();
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      document.getElementById("engNotes").innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","docs/getEngNotes.php?q="+str,true);
  xmlhttp.send();
}

