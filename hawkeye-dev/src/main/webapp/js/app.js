/*
 * JBoss, Home of Professional Open Source
 * Copyright 2012, Red Hat, Inc., and individual contributors
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
Core JavaScript functionality for the application.  Performs the required
Restful calls, validates return values, and populates the member table.
 */

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
///////////                   WORKPACKAGE FUNCTIONS                       ///////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

/* Builds the updated table for the member list */
function buildWorkPackageRows(workpackages) {
	var result = "";

	_.each(workpackages, function(workpackage) { 

		var groupName = ""; 
		var firstname = "";
		var lastname = ""; 

		if (workpackage.assignedUser) {firstname = workpackage.assignedUser.firstName;}

		if (workpackage.assignedUser) {lastname = workpackage.assignedUser.lastName;}

		if (workpackage.groupType) {groupName = workpackage.groupType.groupName;}

		var daysOpen = Math.round(Math.abs(new Date().getTime() - workpackage.creationDate) / (1000 * 60 *60 * 24));

		result = result + "<tr>";   
		result += "<td onclick=\"redirectPage('" + workpackage.workPackageID + "');\" class=\"clickableCell\">";
		result += "<p>" + workpackage.caseNumber +"</p>";
		result +="</td>";
		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID + "');\">";
		result +="<p>" + workpackage.site.siteName + "</p>";
		result +="</td>";
		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID +"');\">";
		result +="<p>" + new Date(workpackage.creationDate).customFormat( "#MM#/#DD#/#YYYY#" ) + "</p>";
		result +="</td>";
		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID +"');\">";
		result +="<p>" + workpackage.priority.priorityName + "</p>";
		result +="</td>";
		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID +"');\">";
		var usid = workpackage.site.usid;
		if (usid){
			result +="<p>" + workpackage.site.usid.usid + "</p>";
		} else {
			result +="<p></p>";
		}
		
		result +="</td>";
		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID +"');\">";
		result +="<p>" + groupName + "</p>";
		result +="</td>";
		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID +"');\">";
		result +="<p>" + firstname + " " + lastname + "</p>";
		result +="</td>";
		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID +"');\">";
		result +="<p>" + daysOpen + "</p>";
		result +="</td>";
		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID + "');\">";
		result += "<p>" + workpackage.opsTurf + "</p>";
		result +="</td>";

		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID + "');\">";
		result +="<p>" + workpackage.status.statusName +"</p>";
		result +="</td>";
		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID + "');\">";
		result +="<p>" + workpackage.status.group.groupName +"</p>";
		result +="</td>";
		result +="<td onclick=\"reloadWorkpackage('" + workpackage.workPackageID +"');\">";
		result +="<p>" +  workpackage.rSSI + "</p>";
		result +="</td>";
		result += "</tr>";
	});

	return result;
}

/*
Attempts to get Work List using a JAX-RS GET.  The callbacks
the refresh the workpackages table, or process JAX-RS response codes to update
the validation errors.
 */

/* Uses JAX-RS GET to retrieve current member list */
function updateWorkPackageDetailTable(wpkey, userid) {
	$.ajax({
		url: "rest/workpackages/workpackage/" + wpkey,
		cache: false,
		success: function(data) {
			$('#wp_details_src').empty().append(buildWorkPackageDetailRows(data, userid));
			// Call code to place markers on map
			deleteMarkers();
			addMarker(data.site.latitude, data.site.longitude, data.site.siteName, 'map_canvas', null);
			fitToMarkers(markers);
			toggleSelector();
		},
		error: function(error) {
			//console.log("error updating table -" + error.status);
		}
	});
}

/* Uses JAX-RS GET to retrieve current member list */
function retrieveWorkPackageNotes(wpkey, textfield) {
	$.ajax({
		async: false,
		url: "rest/workpackages/workpackage/" + wpkey,
		cache: false,
		success: function(data) {
			var newResult = data.engNotes;
			if (newResult){
//				newResult = newResult.replace (/((0[1-9]|1[012])[- \/.]([1-9]|0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d)/g, "\r\n$1");
				newResult = newResult.replace (/((\r\n)(0[1-9]|1[012])[- \/.]([1-9]|0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d)/g, "\r\n$1");
			}
			$(textfield).val(newResult);
		},
		error: function(error) {
			//console.log("error updating table -" + error.status);
		}
	});
}

function getWorkpackageList(userData) {
	//clear existing  msgs
	$('span.invalid').remove();
	$('span.success').remove();

	$.ajax({
		url: 'rest/workpackage',
		contentType: "application/json",
		dataType: "json",
		type: "GET",
		data: JSON.stringify(userData),
		success: function(data) {

			//mark success on the registration form
			$('#formMsgs').append($('<span class="success">Workpackage List</span>'));

			// get Workpackage List

		},
		error: function(error) {
			if ((error.status == 409) || (error.status == 400)) {
				//console.log("Validation error registering user!");

				var errorMsg = $.parseJSON(error.responseText);

				$.each(errorMsg, function(index, val) {
					$('#formMsgs').append($('<span class="invalid">No packages found</span>'));
					$('<span class="invalid">' + val + '</span>').insertAfter($('#' + index));
				});
			} else {
				//console.log("error - unknown server issue");
				$('#formMsgs').append($('<span class="invalid">Unknown server error</span>'));
			}
		}
	});
}


function updateWorkpackage(data) {
	$.ajax({
		contentType: "application/json",
		dataType: "json",
		type: "POST",
		data: JSON.stringify(data),
		url: "rest/workpackages/update",
		cache: false,
		success: function(data) {
			$('#txt_response').empty().append("Successfully updated.");
		},
		error: function(error) {
			alert("Failed updating package.");
			//console.log("error updating table -" + error.status);
		}
	});
}

function updateWorkpackageNotes(data) {
	$.ajax({
		contentType: "application/json",
//		dataType: "json",
		type: "POST",
		data: JSON.stringify(data),
		url: "rest/workpackages/updateNotes",
		cache: false,
		success: function(data) {
			$('#txt_response').empty().append("Successfully updated notes.");
		},
		error: function(error) {
			alert("Failed updating notes.");
			//console.log("error updating table -" + error.status);
		}
	});
}

function getWorkPackageDetailTemplate(wpkey, userid) {
	$.ajax({
		url: "tmpl/workPackageDetail.tmpl",
		dataType: "html",
		success: function( data ) {
			$( "head" ).append( data );
			updateWorkPackageDetailTable(wpkey, userid);
		}
	});
}

/* Builds the updated table for the member list 
function buildWorkPackageDetailRows2(workpackage, userid) {
	var lastCloseDate = getLastWorkPackageCompletionDate(workpackage.workPackageID);
	jobTypes = retrieveGroupTypes(userid);	
	console.log(jobTypes);
	return _.template( $( "#workPackageDetail-tmpl" ).html(), {"workpackage": workpackage, "lastCloseDate" :  lastCloseDate, "jobTypes" : jobTypes});
}
*/

function toggleSelector(){
		if ($("#cmb_update_status").val() === "5" || $("#cmb_update_status").val() === "12"){
			$("#cmb_res_id").show();
		} else {
			$("#cmb_res_id").hide();
			$("#cmb_res_id").val("0");
		}
}

function clearResolution(){
	$("#txt_response").empty();
}


function buildWorkPackageDetailRows(workpackage, userid) {

	var lastCloseDate = null;
	var localJobTypes = retrieveGroupTypes(userid);	

	var users = retrieveUsers(userid);
	var statuses = retrieveStatuses(userid);
	var resolutions = retrieveResolutions(userid);
	var daysOpen = Math.round(Math.abs(new Date().getTime() - workpackage.creationDate) / (1000 * 60 *60 * 24));
	var dateSubmitted = new Date(workpackage.creationDate).customFormat( "#MM#/#DD#/#YYYY#" );
	var lastCompletionDate = "";
	
	if(workpackage.status.statusID == 5)
		lastCloseDate = getLastWorkPackageCompletionDate(workpackage.workPackageID);

	if (lastCloseDate != null) {
		lastCompletionDate = new Date(lastCloseDate).customFormat( "#MM#/#DD#/#YYYY#" );
	}
	result = '<table id="wpd_table" border="1" class="TBL-1">';
	result += '<col style="width:110px;">';
	result += '<col style="width:173px;">';
	result += '<col style="width:157px;">';
	result += '<col style="width:174px;">';
	result += '<tr>';
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">Trouble #</p>';
	result += '</td>';
	result += '<td class="TC-Normal">';
	result += '<p class="Table-Body TC-P2Dark">' + workpackage.caseNumber + '</p>';
	result += '</td>';
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">Site</p>';
	result += '</td>';
	result += '<td class="TC-Normal">';
	result += '<p class="Table-Body TC-P2Dark">'+workpackage.site.siteName +'</p>';
	result += '</td>';
	result += '</tr>';
	
	result += '<tr>';
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">RF Engineer</p>';
	result += '</td>';
	result += '<td class="TC-Normal">';
	result += '<p class="Table-Body TC-P2Dark">' + workpackage.rfEngineer+ '</p>';
	result += '</td>';
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">Sector</p>';
	result += '</td>';
	result += '<td class="TC-Normal">';
	if (workpackage.sector) {
		result += '<p class="Table-Body TC-P2Dark">' + workpackage.sector.sectorName+ '</p>';
	} else {
		result += '<p class="Table-Body TC-P2Dark"></p>';
	} 
	result += '</td>';
	result += '</tr>';
	result += '<tr>';
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">Telephone #</p>';
	result += '</td>';
	result += '<td class="TC-Normal">';
	result += '<p class="Table-Body TC-P2Dark">' + workpackage.rfEngineerNum+ '</p>';
	result += '</td>';
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">Technology</p>';
	result += '</td>';
	result += '<td class="TC-Normal">';
	result += '<p class="Table-Body TC-P2Dark">' + workpackage.technology.technologyName+ '</p>';
	result += '</td>';
	result += '</tr>';
	result += '<tr>';
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">Date Submitted</p>';
	result += '</td>';
	result += '<td class="TC-Normal">';
	result += '<p class="Table-Body TC-P2Dark">' + dateSubmitted+ '</p>';
	result += '</td>';
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">Frequency</p>';
	result += '</td>';
	result += '<td class="TC-Normal">';
	result += '<p class="Table-Body TC-P2Dark">' + workpackage.frequency+ '</p>';
	result += '</td>';
	result += '</tr>';
	result += '<tr>';
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">Days Open</p>';
	result += '</td>';
	result += '<td class="TC-Normal">';
	result += '<p class="Table-Body TC-P2Dark">' + daysOpen+ '</p>';
	result += '</td>';
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">Uplink Channel &amp; Freq</p>';
	result += '</td>';
	result += '<td class="TC-Normal">';
	result += '<p class="Table-Body TC-P2Dark">' + workpackage.uplinkChFreq+ '</p>';
	result += '</td>';
	result += '</tr>';
	result += '<tr>';
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">Completion Date</p>';
	result += '</td>';
	result += '<td class="TC-Normal">';
	result += '<p class="Table-Body TC-P2Dark">' + lastCompletionDate+ '</p>';
	result += '</td>';
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">RSSI Level</p>';
	result += '</td>';
	result += '<td class="TC-Normal">';
	result += '<p class="Table-Body TC-P2Dark">' + workpackage.rSSI+ '</p>';
	result += '</td>';
	result += '</tr>';
	result += '<tr>';
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">Job Type</p>';
	result += '</td>';
	result += '<td class="TC-Normal">';
	result += '<p class="Table-Body TC-P2Dark">';
	result += '<select name="cmb_group_id" id="cmb_group_id">';
	if(localJobTypes && localJobTypes.length > 0) {
//		result += '<option value="0">Select Job</option>';
		_.each(localJobTypes, function(job) {
			result += '<option value="' + job.groupTypeID +'"';
			if(workpackage.groupType.groupName == job.groupName){ 
				result += ' selected '; }
			result += '>' + job.groupName + '</option>';
		});
	} else {
		result += '<option value"0">No Job Types Found</option>';
	}
	result += '</select>';
	result += '</p>';
	result += '</td>';
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">Status</p>';
	result += '</td>';
	result += '<td class="TC-Normal">';
	result += '<select name="cmb_update_status" id="cmb_update_status" onchange="toggleSelector();">';
	if(statuses && statuses.length > 0) {
		_.each(statuses, function(status) {
			result += '<option value="' + status.statusID +'"';
			if(workpackage.status.statusID == status.statusID){ 
				result += ' selected '; 
			}
			result += '>' + status.statusName + '</option>';
		});
	} else {
		result += '<option value"0">No Statuses Found</option>';
	}
	result += '</select>';
	result += '</td>';
	
	result += '</tr>';
	result += '<tr>';
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">Assigned</p>';
	result += '</td>';
	result += '<td>';
	result += '<select name="cmb_user_id" id="cmb_user_id">';
	if(users && users.length > 0) {
		_.each(users, function(user) {
			var usr = workpackage.assignedUser;
			result += '<option value="' + user.userID +'"';
			if (usr){
				if(usr.userID == user.userID){ 
					result += ' selected '; 
				}
			}
			result += '>' + user.lastName + ', ' + user.firstName + '</option>';
		});
	} else {
		result += '<option value"0">No Users Found</option>';
	}

	result += '</select>';
	result += '</td>';
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">Resolution</p>';
	result += '</td>';
	result += '<td class="TC-Normal">';
	result += '<p class="Table-Body TC-P2Dark">';
	result += '<select name="cmb_res_id" id="cmb_res_id" onchange="clearResolution();">';
	if(resolutions && resolutions.length > 0) {
		result += '<option value="0">Select Resolution</option>';
		_.each(resolutions, function(res) {
			result += '<option value="' + res.resolutionID +'"';
			var wpResolution = workpackage.resolution;
			if (wpResolution){
				wpResolution = workpackage.resolution.resolutionID;
			}
			if(wpResolution == res.resolutionID){ 
				result += ' selected '; }
			result += '>' + res.resolutionName + '</option>';
		});
	} else {
		result += '<option value"0">No Resolutions Found</option>';
	}
	result += '</select>';
	result += '</p>';
	result += '</td>';
	result += '</tr>';
	result += '<tr>';	
	result += '<td class="TC-Normal-Alt">';
	result += '<p class="Table-Body TC-P2Dark">Priority</p>';
	result += '</td>';
	result += '<td class="TC-Normal">';
	result += '<p class="Table-Body TC-P2Dark">' + workpackage.priority.priorityName + '</p>';
	result += '</td>';
	result += '</tr>';	
	result += '</table>';
	return result;
}

function getNotes(data) {
	$.ajax({
		url: 'rest/users',
		contentType: "application/json",
		dataType: "json",
		type: "POST",
		data: JSON.stringify(memberData),
		success: function(data) {
			//console.log("Member registered");

			//clear input fields
			$('#reg')[0].reset();

			//mark success on the registration form
			$('#formMsgs').append($('<span class="success">Member Registered</span>'));

			updateMemberTable();
		},
		error: function(error) {
			if ((error.status == 409) || (error.status == 400)) {
				//console.log("Validation error registering user!");

				var errorMsg = $.parseJSON(error.responseText);

				$.each(errorMsg, function(index, val) {
					$('<span class="invalid">' + val + '</span>').insertAfter($('#' + index));
				});
			} else {
				//console.log("error - unknown server issue");
				$('#formMsgs').append($('<span class="invalid">Unknown server error</span>'));
			}
		}
	});
}

/*
Attempts to get Work List using a JAX-RS GET.  The callbacks
the refresh the workpackages table, or process JAX-RS response codes to update
the validation errors.
 */
function getWorkPackageTemplate(apiKey, tablename) {
	$.ajax({
		async:false,
		url: "tmpl/workPackage.tmpl",
		dataType: "html",
		success: function( data ) {
//			$( "head" ).append( data );
			updateWorkPackageTable(apiKey);
			$("table").trigger("update"); 
		}
	});
}

/* Uses JAX-RS GET to retrieve current member list */
function updateWorkPackageTable(apiKey) {
	$.ajax({
		async:false,
		url: "rest/workpackages/" + apiKey + "/jobType",
		cache: false,
		success: function(data) {
			var html = buildWorkPackageRows(data);
			$('#tbl_wp_list').append(html);
		},
		error: function(error) {
//			console.log("error updating table -" + error.status);
		}
	});
}



/* Uses JAX-RS GET to retrieve current member list */
function getLastWorkPackageCompletionDate(wpkey) {
	var returnValue = [];

	$.ajax({
		async:false,
		url: "rest/workpackages/workpackage/lastCompletionDate/" + wpkey,
		cache: false,
		//dataType: "json",
		success: function(data) {
			returnValue = data.currentStatusCreationDate;
		},
		error: function(error) {
//			console.log("error updating table -" + error.status);
		}
	});
	return returnValue;
}


/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
///////////                     WORKLOAD FUNCTIONS                        ///////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

/* Uses JAX-RS GET to retrieve current member list */
function updateWorkloadClosedTable(comp, prod) {
	$.ajax({
		async:false,
		url: "rest/workload/closeSummary/" + comp + "/" + prod,
		cache: false,
		success: function(data) {

			var html = buildWorkloadClosedDashboard(data);

			$('#tbl_wl_ytd').append(html);
		},
		error: function(error) {
//			console.log("error updating table -" + error.status);
		}
	});
}

function buildWorkloadClosedDashboard(closes) {

	var result1 = '<tr  class="td_blue"><th><p>Close EIH - YTD</p></th><th><p>Cases</p></th></tr>';
	var result2 = '<tr  class="td_blue"><th><p>Avg EIH - RTC</p></th><th><p>Days</p></th></tr>';

	_.each(closes, function(close) { 
		result1 += '<tr class="td_gray">';
		result1 += '<td>' + close.lastName + ", " + close.firstName + '</td>';
		result1 += '<td class="ta_center">' + close.cases     + '</td>';
		result1 += '</tr>';

		result2 += '<tr class="td_gray">';
		result2 += '<td>' + close.lastName + ", " + close.firstName + '</td>';
		result2 += '<td class="ta_center">' + close.days     + '</td>';
		result2 += '</tr>';
	});

	result1 += '<tr class="td_white"><td></td><td></td></tr>';
	result2 += '<tr class="td_white"><td></td><td></td></tr>';

	return result1 + result2;

}

/* Uses JAX-RS GET to retrieve current member list */
function updateWorkloadTechSummaryTable(comp, prod, userID, groupID) {

	var inputs = {
			userID: userID,
			groupTypeID: groupID
	}

	$.ajax({
		async:false,
		url: "rest/workload/summaryTech/" + comp + "/" + prod,
		data: inputs,
		cache: false,
		success: function(data) {

			var html = buildWorkloadTechSummaryDashboard(data, comp, prod, groupID);

			$('#tbl_wl_tech').append(html);
		},
		error: function(error) {
//			console.log("error updating table -" + error.status);
		}
	});
}

function buildWorkloadTechSummaryDashboard(summaries, comp, prod, groupID) {

	var result1 = '';  //<tr  class="td_blue"><th><p>Assigned</p></th><th><p></p></th><th><p>Work</p></th></tr>';

	var isHead = true;
	var currentID = 0;
	var count = 0;

	_.each(summaries, function(summary) {

		if (currentID != summary.userid){

			result1 += '<tr class="td_blue">';
			currentID = summary.userid;

			isHead = false;
			result1 += '<td colspan="2"><a style="text-decoration: underline;cursor:pointer;" onclick="updateWorkloadDetailForTechTable(' + comp + ',' + prod + ',' + summary.userid + ',' + groupID + ', \'ALL\')"><p>' + summary.lastName + ", " + summary.firstName + '</p></a></td><td></td></tr>';
			result1 += '<tr class="td_gray">';
			result1 += '<td colspan="2" class="ta_center"><p>    ' + summary.hierarchyName     + '</p></td>';
			result1 += '<td class="ta_center"><p>' + summary.numberRecords     + '</p></td>';    	
			result1 += '</tr>';
		} else
		{
			result1 += '<tr class="td_gray">';
			result1 += '<td colspan="2" class="ta_center"><p>    ' + summary.hierarchyName     + '</p></td>';
			result1 += '<td class="ta_center"><p>' + summary.numberRecords     + '</p></td>';    	
			result1 += '</tr>';
			//count++;
		}
		count = count + summary.numberRecords;
	});

	result1 += '<tr class="td_white"><td colspan="2"><p><a style="text-decoration: underline;cursor:pointer;" onclick="updateWorkloadDetailForTechTable(' + comp + ',' + prod + ', 0,' + groupID + ', \'All\')">Total</a></p></td><td class="ta_center">' + count + '</td></tr>';
	return result1;

}

/* Uses JAX-RS GET to retrieve current member list */
function updateWorkloadMktSummaryTable(comp, prod, userID, groupID) {

	var inputs = {
			userID: userID,
			groupTypeID: groupID
	}

	$.ajax({
		async:false,
		url: "rest/workload/summaryMkt/" + comp + "/" + prod,
		data: inputs,
		cache: false,
		success: function(data) {

			var html = buildWorkloadMktSummaryDashboard(data, comp, prod, groupID);

			$('#tbl_wl_market').append(html);
		},
		error: function(error) {
//			console.log("error updating table -" + error.status);
		}
	});
}

function buildWorkloadMktSummaryDashboard(summaries, comp, prod, groupID) {

	var result1 = '';  //<tr  class="td_blue"><th><p>Assigned</p></th><th><p></p></th><th><p>Work</p></th></tr>';

	var isHead = true;
	var currentID = 0;
	var count = 0;

	_.each(summaries, function(summary) {

		if (currentID != summary.hierarchyName){

//			if ((!isHead) && (count > 3)){
//			result1 += '<tr class="td_white">';
//			result1 += '<td></td>';
//			result1 += '<td></td>';
//			result1 += '<td><p>' + count + '</p></td>';    	
//			result1 += '</tr>';
//			count = 0;
//			}

			result1 += '<tr class="td_blue">';
			currentID = summary.hierarchyName;

			isHead = false;
			result1 += '<td colspan="2"><a style="text-decoration: underline;cursor:pointer;" onclick="updateWorkloadDetailForMktTable(' + comp + ',' + prod + ', 0,' + groupID + ', \'' + summary.hierarchyName + '\')"><p>' + summary.hierarchyName + '</p></a></td><td></td></tr>';
			result1 += '<tr class="td_gray">';
			result1 += '<td colspan="2" class="ta_center"><p>    ' + summary.lastName + ", " + summary.firstName + '</p></td>';
			result1 += '<td class="ta_center"><p>' + summary.numberRecords     + '</p></td>';    	
			result1 += '</tr>';
		} else
		{
			result1 += '<tr class="td_gray">';
			result1 += '<td colspan="2" class="ta_center"><p>    ' + summary.lastName + ", " + summary.firstName + '</p></td>';
			result1 += '<td class="ta_center"><p>' + summary.numberRecords     + '</p></td>';    	
			result1 += '</tr>';
			//count++;
		}
		count = count + summary.numberRecords;
	});

	result1 += '<tr class="td_white"><td colspan="2"><p><a style="text-decoration: underline;cursor:pointer;" onclick="updateWorkloadDetailForMktTable(' + comp + ',' + prod + ', 0,' + groupID +', \'MAll\' )">Total</a></p></td><td class="ta_center"><p>' + count + '</p></td></tr>';

	return result1;

}

/* Uses JAX-RS GET to retrieve current member list */
function updateWorkloadDetailForTechTable(comp, prod, userID, groupID) {

	var inputs = {
			userID: userID,
			groupTypeID: groupID,
			marketName: ''
	};

	$.ajax({
		async:false,
		url: "rest/workload/detail/" + comp + "/" + prod,
		data: inputs,
		cache: false,
		success: function(data) {

			var html = buildWorkloadDetailForTechDashboard(data);

			$('#wl_indiv_cont').empty().append(html);
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
}

function buildWorkloadDetailForTechDashboard(summaries) {

	var result1 = '';
	var  count = 0;
	var currentID = "";
	var firstLine = true;
	result1 += '<table id="tbl_wl_indiv" class="wl_tbl wl_indiv_tbl">';
	result1 += '<col style="width:8%;"><col style="width:10%;"><col style="width:6%;"><col style="width:22%;"><col style="width:47%;">';
	result1 += '<thead><tr class="td_lightblue ta_center"><th><p>Case</p></th><th><p>Site</p></th><th>';
	result1 += '<p>Date</p></th><th><p>Status</p></th><th><p>Ops Turf</p></th>';
	result1 += '<th><p>Notes</p></th><th><p>Days Open</p></th></tr></thead>';

	deleteMarkers();

	_.each(summaries, function(summary) {
		count++;
		if (currentID != summary.userid){
			if (!firstLine){
				result1 += '</tbody>';
			}
			firstLine = false;
			result1 += '<tbody>';
			result1 += '<tr class="td_blue">';
			result1 += '<td colspan="7">' + summary.lastName + ', ' + summary.firstName + '</td>';
			result1 += '</tr>';

			currentID = summary.userid;
		}
		result1 += '<tr class="td_white">';
		result1 += '<td onclick=\"redirectPage(' + summary.workpackageid + ');\"><p>' + summary.caseNumber + '</p></td>';
		result1 += '<td><p>' + summary.siteName + '</p></td>';
		result1 += '<td><p>' + new Date(summary.creationdate).customFormat( "#MM#/#DD#/#YYYY#" ) + '</p></td>';
		result1 += '<td><p>' + summary.status + '</p></td>';
		result1 += '<td><p>' + summary.turf + '</p></td>';

		result1 +="<td>";
		result1 +="<p style=\"cursor:Pointer;\" class=\"Table-Body TB-Ctr\" title=\"" + summary.notes + "\">";
		if (summary.notes.length > 20){
			var str = summary.notes;
			result1 += str.substring(0,20) + "...";
		} else {
			result1 += summary.notes;
		}
		result1 += "</p>";
		result1 += "</td>";


//		result1 += '<td><p>' + summary.notes + '</p></td>';
		result1 += '<td><p>' + summary.daysOpen + '</p></td>';
		result1 += '</tr>';

		addMarker(summary.latitude, summary.longitude, summary.siteName, 'map_canvas', null);
		fitToMarkers(markers);
	});
	if (count > 0)
		result1 += '</tbody></table>';

	centerOnMarkers();
	return result1;
}


/* Uses JAX-RS GET to retrieve current member list */
function updateWorkloadDetailForMktTable(comp, prod, userID, groupID, marketName) {

	var inputs = {
			userID: userID,
			groupTypeID: groupID,
			marketName: marketName
	};

	$.ajax({
		async:false,
		url: "rest/workload/detail/" + comp + "/" + prod,
		data: inputs,
		cache: false,
		success: function(data) {

			var html = buildWorkloadDetailForMktDashboard(data);

			$('#wl_indiv_cont').empty().append(html);
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
}

function buildWorkloadDetailForMktDashboard(summaries) {

	var result1 = '';
	var  count = 0;
	var currentID = "";
	var firstLine = true;
	result1 += '<table id="tbl_wl_indiv" class="wl_tbl wl_indiv_tbl">';
	result1 += '<col style="width:8%;"><col style="width:10%;"><col style="width:6%;"><col style="width:22%;"><col style="width:47%;">';
	result1 += '<thead><tr class="td_lightblue ta_center"><th><p>Case</p></th><th><p>Site</p></th><th>';
	result1 += '<p>Date</p></th><th><p>Status</p></th><th><p>Assigned</p></th>';
	result1 += '<th><p>Notes</p></th><th><p>Days Open</p></th></tr></thead>';

	deleteMarkers();

	_.each(summaries, function(summary) {
		count++;
		if (currentID != summary.turf){
			if (!firstLine){
				result1 += '</tbody>';
			}
			firstLine = false;
			result1 += '<tbody>';
			result1 += '<tr class="td_blue">';
			result1 += '<td colspan="7">' + summary.turf + '</td>';
			result1 += '</tr>';

			currentID = summary.turf;
		}
		result1 += '<tr class="td_white">';
		result1 += '<td onclick=\"redirectPage(' + summary.workpackageid + ');\"><p>' + summary.caseNumber + '</p></td>';
		result1 += '<td><p>' + summary.siteName + '</p></td>';
		result1 += '<td><p>' + new Date(summary.creationdate).customFormat( "#MM#/#DD#/#YYYY#" ) + '</p></td>';
		result1 += '<td><p>' + summary.status + '</p></td>';
		result1 += '<td><p>'  + summary.lastName + ', ' + summary.firstName +  '</p></td>';
		result1 +="<td>";
		result1 +="<p style=\"cursor:Pointer;\" class=\"Table-Body TB-Ctr\" title=\"" + summary.notes + "\">";
		if (summary.notes.length > 20){
			var str = summary.notes;
			result1 += str.substring(0,20) + "...";
		} else {
			result1 += summary.notes;
		}
		result1 += "</p>";
		result1 += "</td>";


//		result1 += '<td><p>' + summary.notes + '</p></td>';
		result1 += '<td><p>' + summary.daysOpen + '</p></td>';
		result1 += '</tr>';

		addMarker(summary.latitude, summary.longitude, summary.siteName, 'map_canvas', null);
		fitToMarkers(markers);
	});
	if (count > 0)
		result1 += '</tbody></table>';

	centerOnMarkers();
	return result1;
}

/* Uses JAX-RS GET to retrieve current member list */
function updateDashboardTable(comp, prod, startDate, endDate) {

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!

	var yyyy = today.getFullYear();
	if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} today = yyyy+'/'+mm+'/'+dd;


	if (startDate == ""){
		startDate = today;
	}

	if (endDate == ""){
		endDate = today;
	}

	$.ajax({
		url: "rest/utility/dashboard/" + comp + "/" + prod + "?startDate=" + startDate + "&endDate=" + endDate,
		cache: false,
		success: function(data) {
			$('#dashboard').empty().append(buildDashboardRows(data));
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
}

function updateGroupTypes(userID){
	var comp = 0;
	var prod = 0;
	$.ajax({
		url: "rest/users/" + userID,
		cache: false,
		async: false,
		success: function(data) {
			comp = data.company.companyID;
			prod = data.product.productID;

			$.ajax({
				url: "rest/users/" + comp + "/" + prod + "/" + userID + "/" + "jobTypes",
				cache: false,
				async: false,
				success: function(datas) {
					var $el = $("#cmb_group_id");
					$el.empty(); // remove old options
					$.each(datas, function() {
						$el.append($("<option></option>").attr("value", this.groupID).text(this.groupName));
					});
				},
				error: function(error) {
					//console.log("error updating table -" + error.status);
				}
			});
		},
		error: function(error) {
			return;
		}
	});
}

function retrieveUsers(userID){
	var comp = 0;
	var prod = 0;
	var returnedData;
	$.ajax({
		url: "rest/users/" + userID,
		cache: false,
		async: false,
		success: function(data) {
			comp = data.company.companyID;
			prod = data.product.productID;

			$.ajax({
				url: "rest/users/" + comp + "/" + prod,
				cache: false,
				async: false,
				success: function(datas) {
					returnedData = datas;
				},
				error: function(error) {
					//console.log("error updating table -" + error.status);
				}
			});
		},
		error: function(error) {
			return;
		}
	});
	return returnedData;
}
function updateUsers(userID){
	var comp = 0;
	var prod = 0;
	$.ajax({
		url: "rest/users/" + userID,
		cache: false,
		async: false,
		success: function(data) {
			comp = data.company.companyID;
			prod = data.product.productID;

			$.ajax({
				url: "rest/users/" + comp + "/" + prod,
				cache: false,
				async: false,
				success: function(datas) {
					var $el = $("#cmb_user_id");
					$el.empty(); // remove old options
					$.each(datas, function() {
						$el.append($("<option></option>").attr("value", this.userID).text(this.lastName + ", " + this.firstName));
					});

				},
				error: function(error) {
					//console.log("error updating table -" + error.status);
				}
			});
		},
		error: function(error) {
			return;
		}
	});
}
function retrieveStatuses(userID){
	var comp = 0;
	var prod = 0;
	var returnedStatuses = [];
	$.ajax({
		url: "rest/users/" + userID,
		cache: false,
		async: false,
		success: function(data) {
			comp = data.company.companyID;
			prod = data.product.productID;

			$.ajax({
				url: "rest/utility/statuses/" + comp + "/" + prod,
				cache: false,
				async: false,
				success: function(datas) {
					returnedStatuses = datas;

				},
				error: function(error) {
					//console.log("error updating table -" + error.status);
				}
			});
		},
		error: function(error) {
			return;
		}
	});
	return returnedStatuses;
}

function retrieveResolutions(userID){
	var prod = 0;
	var returnedResolutions = [];
	$.ajax({
		url: "rest/users/" + userID,
		cache: false,
		async: false,
		success: function(data) {
			prod = data.product.productID;

			$.ajax({
				url: "rest/utility/resolutions/" + prod,
				cache: false,
				async: false,
				success: function(datas) {
					returnedResolutions = datas;

				},
				error: function(error) {
					//console.log("error updating table -" + error.status);
				}
			});
		},
		error: function(error) {
			return;
		}
	});
	return returnedResolutions;
}

function updateStatuses(userID){
	var comp = 0;
	var prod = 0;
	$.ajax({
		url: "rest/users/" + userID,
		cache: false,
		async: false,
		success: function(data) {
			comp = data.company.companyID;
			prod = data.product.productID;

			$.ajax({
				url: "rest/utility/statuses/" + comp + "/" + prod,
				cache: false,
				async: false,
				success: function(datas) {
					var $el = $("#cmb_update_status");
					$el.empty(); // remove old options
					$.each(datas, function() {
						$el.append($("<option></option>")
								.attr("value", this.statusID).text(this.statusName));
					});

				},
				error: function(error) {
					//console.log("error updating table -" + error.status);
				}
			});
		},
		error: function(error) {
			return;
		}
	});
}
function chooseCorrectUserAndStatus(workpackageID){

	$.ajax({
		url: "rest/workpackages/workpackage/" + workpackageID,
		cache: false,
		async: false,
		success: function(data1) {
			var e1 = $("#cmb_update_status");
			if (data1.status)
				e1.val( data1.status.statusID );

			var e2 = $("#cmb_user_id");
			if (data1.assignedUser)
				e2.val( data1.assignedUser.userID );

		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
}

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
///////////            WORKPACKAGE SCORECARD FUNCTIONS                    ///////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////


/* Uses JAX-RS GET to retrieve current member list */
function updateScorecardSummaryTable(wpkey) {
	$.ajax({
		url: "rest/utility/score/" + wpkey,
		cache: false,
		success: function(data) {
			$('#wp_scorecard_src').empty().append(buildScorecardSummaryRows(data));
		},
		error: function(error) {
			//console.log("error updating table -" + error.status);
		}
	});
}

/* Uses JAX-RS GET to retrieve current member list */
function updateScorecardTable(wpkey) {
	$.ajax({
		url: "rest/utility/score/" + wpkey,
		cache: false,
		success: function(data) {
			$('#wp_id_scorecard_src').empty().append(buildScorecardRows(data));
		},
		error: function(error) {
			//console.log("error updating table -" + error.status);
		}
	});
}

function getScorecardTemplate(wpkey) {
	$.ajax({
		url: "tmpl/scorecard.tmpl",
		dataType: "html",
		success: function( data ) {

			$( "head" ).append( data );
			updateScorecardTable(wpkey);
		}
	});
}

function getScorecardSummaryTemplate(wpkey) {
	$.ajax({
		url: "tmpl/scorecardsummary.tmpl",
		dataType: "html",
		success: function( data ) {

			$( "head" ).append( data );
			updateScorecardSummaryTable(wpkey);
		}
	});
}


function buildScorecardRows(scorecards) {
	return _.template( $( "#scorecard-tmpl" ).html(), {"scores": scorecards});
}

function buildScorecardSummaryRows(scorecards) {
	return _.template( $( "#scorecardsummary-tmpl" ).html(), {"scores": scorecards});
}

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
///////////            WORKPACKAGE ATTACHMENT FUNCTIONS                   ///////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

/* Uses JAX-RS GET to retrieve current member list */
function updateAttachmentTable(wpkey, divName, boxName) {
	$.ajax({
		//url: "rest/workpackages/" + wpkey + "/attachments/",
		url: "rest/file/" + wpkey + "/attachments",
		async: true,
		cache: false,
		success: function(data) {
			// wp_gallery_pg_3
			$(divName).empty().append(buildWorkPackageAttachmentsRows(data));
			$(boxName).wplightbox(
					{"loadBtnSrc":"images/lightbox_load.gif","border_e":"images/lightbox_e_6.png","border_n":"images/lightbox_n_6.png","border_w":"images/lightbox_w_6.png","border_s":"images/lightbox_s_6.png","border_ne":"images/lightbox_ne_6.png","border_se":"images/lightbox_se_6.png","border_nw":"images/lightbox_nw_6.png","border_sw":"images/lightbox_sw_6.png","closeBtnSrc":"images/lightbox_close_2.png","closeOverBtnSrc":"images/lightbox_close_over_2.png","nextBtnSrc":"images/lightbox_next_2.png","nextOverBtnSrc":"images/lightbox_next_over_2.png","prevBtnSrc":"images/lightbox_prev_2.png","prevOverBtnSrc":"images/lightbox_prev_over_2.png","blankSrc":"scripts/blank.gif","bBkgrndClickable":true,"strBkgrndCol":"#000000","nBkgrndOpacity":0.0,"strContentCol":"#ffffff","nContentOpacity":0.8,"strCaptionCol":"#555555","nCaptionOpacity":1.0,"nCaptionType":1,"bCaptionCount":false,"strCaptionFontType":"Tahoma,Serif","strCaptionFontCol":"#ffffff","nCaptionFontSz":15,"bShowPlay":false,"bAnimateOpenClose":true,"nPlayPeriod":2000}
			);
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
}


function getAttachmentTemplate(wpkey, divName, boxName) {
	$.ajax({
		url: "tmpl/attachment.tmpl",
		dataType: "html",
		success: function( data ) {
			$( "head" ).append( data );
			updateAttachmentTable(wpkey, divName, boxName);
		}
	}); 
}

function getAttachmentTemplate2(wpkey, divName, boxName) {
	$.ajax({
		url: "tmpl/attachment2.tmpl",
		dataType: "html",
		success: function( data ) {
			$( "head" ).append( data );
			updateAttachmentTable(wpkey, divName, boxName);
		}
	});
}

function deleteAttachmentTemplate(wpkey) {
	$.ajax({
		url: "tmpl/deleteAttachment.tmpl",
		dataType: "html",
		success: function( data ) {
			$( "head" ).append( data );
			deleteAttachmentTable(wpkey);
		}
	});
}

/* Uses JAX-RS GET to retrieve current member list */
function deleteAttachmentTable(wpkey) {
	$.ajax({
		url: "rest/workpackages/" + wpkey + "/attachments/",
		cache: false,
		success: function(data) {
			$('#deleteAttachments').empty().append(buildDeleteWorkPackageAttachmentsRows(data));

		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
}

/* Builds the updated table for the member list */
function buildWorkPackageAttachmentsRows(files) {
	jsonFiles = JSON.parse(files);
	return _.template( $( "#attachment-tmpl" ).html(), {"files": jsonFiles.files});
}
/* Builds the updated table for the member list */
function buildDeleteWorkPackageAttachmentsRows(attachments) {
	return _.template( $( "#deleteAttachment-tmpl" ).html(), {"attachments": attachments});
}

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
///////////                   GOOGLE MAP FUNCTIONS                        ///////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

function addMarker(lat, lng, title, mapDiv, image){

	var options =
	{
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true,
			draggableCursor: 'move'
	};

//	map = new google.maps.Map( document.getElementById("map_canvas"), options);

	var markerR = new google.maps.Marker({
		position: new google.maps.LatLng(lat, lng),
		map: map,
		title: title,
		icon: 'images/markers/circles.png'
	});

	markers.push(markerR);
	var infoContents = '<h3>Directions</h>';
	var infowindowR = new google.maps.InfoWindow({content: infoContents});        
	google.maps.event.addListener(markerR, 'click', function() {
		infowindowR.open(window.map, markerR);
	});
}

function fitToMarkers(markers){

	map.initialZoom = true;
	var bounds = new google.maps.LatLngBounds();
//	for ( var i in markers) {
	for(var i = 0; i < markers.length; i++) {
		var latlong = markers[i].getPosition();
		bounds.extend(latlong);
	}

	// Don't zoom in too far on only one marker
	if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
		var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.50, bounds.getNorthEast().lng() + 0.50);
		var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.50, bounds.getNorthEast().lng() - 0.50);
		bounds.extend(extendPoint1);
		bounds.extend(extendPoint2);
	}
	map.fitBounds(bounds);

}




//Sets the map on all markers in the array.
function setAllMap(map) {
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
	}
}

//Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
	setAllMap(null);
}

//Deletes all markers in the array by removing references to them.
function deleteMarkers() {
	clearMarkers();
	markers = [];
}

function clearMarkers(){
	setAllMap(null);
}

//Shows any markers currently in the array.
function showMarkers() {
	setAllMap(map);
}


function centerOnMarkers(){
//	Make an array of the LatLng's of the markers you want to show

	var LatLngList = [];
	for (var i = 0; i < markers.length; i++ ) {
		LatLngList.push(markers[i].getPosition());
	}

	var bounds = new google.maps.LatLngBounds ();
	//  Go through each...
	for (var i = 0, LtLgLen = LatLngList.length; i < LtLgLen; i++) {
		//  And increase the bounds to take this point
		bounds.extend (LatLngList[i]);
	}
	//  Fit these bounds to the map
	map.fitBounds (bounds);
}

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
///////////                      MISC FUNCTIONS                           ///////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

//Formats all dates to the following values...

/*
 * 
 * 
token:     description:             example:
#YYYY#     4-digit year             1999
#YY#       2-digit year             99
#MMMM#     full month name          February
#MMM#      3-letter month name      Feb
#MM#       2-digit month number     02
#M#        month number             2
#DDDD#     full weekday name        Wednesday
#DDD#      3-letter weekday name    Wed
#DD#       2-digit day number       09
#D#        day number               9
#th#       day ordinal suffix       nd
#hhh#      military/24-based hour   17
#hh#       2-digit hour             05
#h#        hour                     5
#mm#       2-digit minute           07
#m#        minute                   7
#ss#       2-digit second           09
#s#        second                   9
#ampm#     "am" or "pm"             pm
#AMPM#     "AM" or "PM"             PM
 */

Date.prototype.customFormat = function(formatString){
	var YYYY,YY,MMMM,MMM,MM,M,DDDD,DDD,DD,D,hhh,hh,h,mm,m,ss,s,ampm,AMPM,dMod,th;
	var dateObject = this;
	YY = ((YYYY=dateObject.getFullYear())+"").slice(-2);
	MM = (M=dateObject.getMonth()+1)<10?('0'+M):M;
	MMM = (MMMM=["January","February","March","April","May","June","July","August","September","October","November","December"][M-1]).substring(0,3);
	DD = (D=dateObject.getDate())<10?('0'+D):D;
	DDD = (DDDD=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][dateObject.getDay()]).substring(0,3);
	th=(D>=10&&D<=20)?'th':((dMod=D%10)==1)?'st':(dMod==2)?'nd':(dMod==3)?'rd':'th';
	formatString = formatString.replace("#YYYY#",YYYY).replace("#YY#",YY).replace("#MMMM#",MMMM).replace("#MMM#",MMM).replace("#MM#",MM).replace("#M#",M).replace("#DDDD#",DDDD).replace("#DDD#",DDD).replace("#DD#",DD).replace("#D#",D).replace("#th#",th);

	h=(hhh=dateObject.getHours());
	if (h==0) h=24;
	if (h>12) h-=12;
	hh = h<10?('0'+h):h;
	AMPM=(ampm=hhh<12?'am':'pm').toUpperCase();
	mm=(m=dateObject.getMinutes())<10?('0'+m):m;
	ss=(s=dateObject.getSeconds())<10?('0'+s):s;
	return formatString.replace("#hhh#",hhh).replace("#hh#",hh).replace("#h#",h).replace("#mm#",mm).replace("#m#",m).replace("#ss#",ss).replace("#s#",s).replace("#ampm#",ampm).replace("#AMPM#",AMPM);
}

//Get value of tag from query string...
function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function loadIframe(iframeName, url) {
	var $iframe = $('#' + iframeName);
	if ( $iframe.length ) {
		$iframe.attr('src',url);   
		return false;
	}
	return true;
}

//Converts /r/n to <br>...  If XHTML must include closing br tag...
function nl2br (str, is_xhtml) {   
	var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
	return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}

function getPrioritiesForDropDown(apikey, dropDownControl){

	$.ajax({
		url: "rest/utility/priorities/" + apikey,
		cache: false,
		async: false,
		success: function(datas) {
			var $el = $(dropDownControl);
			$el.empty(); // remove old options
			$.each(datas, function() {
				$el.append($("<option></option>")
						.attr("value", this.priorityID).text(this.priorityName));
			});
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
}

function getTechnologiesForDropDown(companyID, productID, dropDownControl){

	$.ajax({
		url: "rest/utility/technologies/" + companyID + "/" + productID,
		cache: false,
		async: false,
		success: function(datas) {
			var $el = $(dropDownControl);
			$el.empty(); // remove old options
			$.each(datas, function() {
				$el.append($("<option></option>")
						.attr("value", this.technologyID).text(this.technologyName + " - " + this.type));
			});
		},
		error: function(error) {
			//	console.log("error updating table -" + error.status);
		}
	});
}

function getTechnologiesForDropDown(apikey, dropDownControl){

	$.ajax({
		url: "rest/utility/technologies/" + apikey,
		cache: false,
		async: false,
		success: function(datas) {
			var $el = $(dropDownControl);
			$el.empty(); // remove old options
			$.each(datas, function() {
				$el.append($("<option></option>")
						.attr("value", this.technologyID).text(this.technologyName + " - " + this.type));
			});
		},
		error: function(error) {
			//	console.log("error updating table -" + error.status);
		}
	});
}

function retrieveGroupTypes(userID){
	var comp = 0;
	var prod = 0;
	var results = [];
	$.ajax({
		url: "rest/users/" + userID,
		cache: false,
		async: false, 
		success: function(data) {
			comp = data.company.companyID;
			prod = data.product.productID;
			$.ajax({
				url: "rest/utility/jobtypes/" + comp + "/" + prod,
				cache: false,
				async: false,
				success: function(datas) {
					results = datas;
				},
				error: function(error) {
					//console.log("error updating table -" + error.status);
				}
			});
		},
		error: function(error) {
			return;
		}
	});
	return results;
}

function getJobTypesForDropDown(apikey, dropDownControl){

	$.ajax({
		url: "rest/utility/jobtypesByCompanyProduct/" + apikey,
		cache: false,
		async: false,
		success: function(datas) {
			var $el = $(dropDownControl);
			$el.empty(); // remove old options
			$.each(datas, function() {
				$el.append($("<option></option>")
						.attr("value", this.groupTypeID).text(this.groupName));
			});
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
}


function getInternetExplorerVersion() {    
	var rv = -1; // Return value assumes failure.    
	if (navigator.appName == 'Microsoft Internet Explorer') {        
		var ua = navigator.userAgent;        
		var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");        
		if (re.exec(ua) != null)            
			rv = parseFloat(RegExp.$1);    
	}    
	return rv;
}

function checkVersion() {    
	var msg = "You're not using Windows Internet Explorer.";    
	var ver = getInternetExplorerVersion();    
	if (ver > -1) {        
		if (ver <= 8.0){            
			msg = "We detected you are using Windows Internet Explorer version 8 or lower.  The application and some features may not display or work correctly.  Please use Internet Explorer version 9 or higher, Chrome or Firefox.";
			alert(msg);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
///////////                      USER FUNCTIONS                           ///////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

/*
Attempts to register a new member using a JAX-RS POST.  The callbacks
the refresh the member table, or process JAX-RS response codes to update
the validation errors.
 */
function validateUser(memberData) {
	//clear existing  msgs
//	$('span.invalid').remove();
//	$('span.success').remove();

	$.ajax({
		url: 'rest/users/login',
		contentType: "application/json",
		dataType: "json",
		type: "POST",
		data: JSON.stringify(memberData),
		success: function(data) {

			//mark success on the registration form
//			$('#formMsgs').append($('<span class="success">Member Logged in</span>'));
			if (data.userGroupType.groupTypeID == 1)
				window.location.replace("work-package-list.html?apikey=" + data.apikey);
//			window.location.replace("http://hawktest.global.tektronix.net/wp_list.html");
			else 
				window.location.replace("work-package-list.html?apikey=" + data.apikey);
		},
		error: function(error) {
			if ((error.status == 409) || (error.status == 400)) {
				//console.log("Validation error registering user!");

				var errorMsg = $.parseJSON(error.responseText);

				$.each(errorMsg, function(index, val) {
					$('#login-message').append($('<span class="invalid">Unknown user</span>'));
					$('<span class="invalid">' + val + '</span>').insertAfter($('#' + index));
				});
			} else {
				//console.log("error - unknown server issue");
				$('#login-message').append($('<span class="invalid">Unknown server error</span>'));
			}
		}
	});
}

/*
Attempts to register a new member using a JAX-RS POST.  The callbacks
the refresh the member table, or process JAX-RS response codes to update
the validation errors.
 */
function registerMember(memberData) {
//	clear existing  msgs
	$('span.invalid').remove();
	$('span.success').remove();

	$.ajax({
		url: 'rest/users',
		contentType: "application/json",
		dataType: "json",
		type: "POST",
		data: JSON.stringify(memberData),
		success: function(data) {
//			console.log("Member registered");

//			clear input fields
			$('#reg')[0].reset();

//			mark success on the registration form
			$('#formMsgs').append($('<span class="success">Member Registered</span>'));

			updateMemberTable();
		},
		error: function(error) {
			if ((error.status == 409) || (error.status == 400)) {
//				console.log("Validation error registering user!");

				var errorMsg = $.parseJSON(error.responseText);

				$.each(errorMsg, function(index, val) {
					$('<span class="invalid">' + val + '</span>').insertAfter($('#' + index));
				});
			} else {
//				console.log("error - unknown server issue");
				$('#formMsgs').append($('<span class="invalid">Unknown server error</span>'));
			}
		}
	});
}

/* Get the member template */
function getMemberTemplate() {
	$.ajax({
		url: "tmpl/member.tmpl",
		dataType: "html",
		success: function( data ) {
			$( "head" ).append( data );
			updateMemberTable();
		}
	});
}

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
///////////                  DASHBOARD FUNCTIONS                          ///////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

function getDashboardTemplate(comp, prod, startDate, endDate) {
	$.ajax({
		url: "tmpl/dashboard.tmpl",
		dataType: "html",
		success: function( data ) {
			$( "head" ).append( data );
			updateDashboardTable(comp, prod, startDate, endDate);
		}
	});
}


/* Builds the updated table for the member list */
function buildDashboardRows(dashboard) {
	return _.template( $( "#dashboard-tmpl" ).html(), {"dashboardRows": dashboard});
}

function getHistoryData(workpackageid){
	var histories;
	$.ajax({
		async:false,
		url: "rest/workpackages/" + workpackageid + "/histories",
		cache: false,
		success: function(data) {
			histories = data;

		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});
	return histories;
}

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
///////////                      CHART FUNCTIONS                          ///////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
function dateItem(itemNumber, content, date){
	this.id = itemNumber;
	this.content = content;
	this.start = date;
}

function createData(workpackageid){

	var histories = getHistoryData(workpackageid);
	var graphdata = new Array();
	var count = 1;
	_.each(histories, function(history) {
		count += count;
		var newDateItem = new dateItem(count, history.currentStatus.statusName, history.currentStatusCreationDate);
		graphdata.push(newDateItem);
	});
	return graphdata;
}

function buildHistoryTimeTable(workpackageid, container){

	var cont = document.getElementById(container);
	$("#" + container).empty();
	var options = {};
	var data = createData(workpackageid);
	timeline = new links.Timeline(cont);
	//var timeline = new vis.Timeline(cont, data, options);
	timeline.draw(data, options);
}

function loadGraph(){

//	var week_data = [
//	{"period": "2011 W27", "licensed": 3407, "sorned": 660},
//	{"period": "2011 W27", "licensed": 3407, "sorned": 660}];


	/* Uses JAX-RS GET to retrieve current member list */
	$.ajax({
		async:false,
		url: "rest/workload/closeSummary/" + comp + "/" + prod,
		cache: false,
		success: function(data) {

			var html = buildWorkloadClosedDashboard(data);

			$('#tbl_wl_ytd').append(html);
		},
		error: function(error) {
			console.log("error updating table -" + error.status);
		}
	});

	Morris.Line({
		element: 'graph',
		data: week_data,
		xkey: 'period',
		ykeys: ['licensed', 'sorned'],
		labels: ['Licensed', 'SORN'],
		events: [
		         '2011-04',
		         '2011-08'
		         ]
	});

	prettyPrint();

//	var week_data = [
//	{"period": "2011 W27", "licensed": 3407, "sorned": 660},
//	{"period": "2011 W26", "licensed": 3351, "sorned": 629},
//	{"period": "2011 W25", "licensed": 3269, "sorned": 618},
//	{"period": "2011 W24", "licensed": 3246, "sorned": 661},
//	{"period": "2011 W23", "licensed": 3257, "sorned": 667},
//	{"period": "2011 W22", "licensed": 3248, "sorned": 627},
//	{"period": "2011 W21", "licensed": 3171, "sorned": 660},
//	{"period": "2011 W20", "licensed": 3171, "sorned": 676},
//	{"period": "2011 W19", "licensed": 3201, "sorned": 656},
//	{"period": "2011 W18", "licensed": 3215, "sorned": 622},
//	{"period": "2011 W17", "licensed": 3148, "sorned": 632},
//	{"period": "2011 W16", "licensed": 3155, "sorned": 681},
//	{"period": "2011 W15", "licensed": 3190, "sorned": 667},
//	{"period": "2011 W14", "licensed": 3226, "sorned": 620},
//	{"period": "2011 W13", "licensed": 3245, "sorned": null},
//	{"period": "2011 W12", "licensed": 3289, "sorned": null},
//	{"period": "2011 W11", "licensed": 3263, "sorned": null},
//	{"period": "2011 W10", "licensed": 3189, "sorned": null},
//	{"period": "2011 W09", "licensed": 3079, "sorned": null},
//	{"period": "2011 W08", "licensed": 3085, "sorned": null},
//	{"period": "2011 W07", "licensed": 3055, "sorned": null},
//	{"period": "2011 W06", "licensed": 3063, "sorned": null},
//	{"period": "2011 W05", "licensed": 2943, "sorned": null},
//	{"period": "2011 W04", "licensed": 2806, "sorned": null},
//	{"period": "2011 W03", "licensed": 2674, "sorned": null},
//	{"period": "2011 W02", "licensed": 1702, "sorned": null},
//	{"period": "2011 W01", "licensed": 1732, "sorned": null}
//	];
}



