package com.fnet.pim.factmodel;

import java.io.Serializable;

public class RadioBaseStation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2193359284305908203L;


	private String siteID;
	private String cellID;
	private Location location; 
	
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getSiteID() {
		return siteID;
	}
	public void setSiteID(String siteID) {
		this.siteID = siteID;
	}
	public String getCellID() {
		return cellID;
	}
	public void setCellID(String cellID) {
		this.cellID = cellID;
	}
}
