package com.fnet.pim.factmodel;

import java.io.Serializable;

public class Location implements Serializable {

	private static final long serialVersionUID = 8323731178140900324L;
	/**
	 * 
	 */
	
	private String address;
	private String city;
	private String state;
	private String country;
	private String region;
	private String postalCode;
	private Float latitude;
	private Float longitude;
	
	public String getAddress() {
		return address;
	}
	public void setAddress1(String address1) {
		this.address = address1;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public Float getLatitude() {
		return latitude;
	}
	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}
	public Float getLongitude() {
		return longitude;
	}
	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}



}
