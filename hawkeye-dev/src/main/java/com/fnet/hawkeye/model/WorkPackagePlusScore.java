package com.fnet.hawkeye.model;

public class WorkPackagePlusScore {

	public WorkPackagePlusScore(){
		
	}
	
	public WorkPackagePlusScore(WorkPackage pack, WorkPackageScore scores){
		this.score = scores;
		this.workpackage = pack;
	}
	private WorkPackage workpackage;
	private WorkPackageScore score;
	public WorkPackage getWorkpackage() {
		return workpackage;
	}
	public void setWorkpackage(WorkPackage workpackage) {
		this.workpackage = workpackage;
	}
	public WorkPackageScore getScore() {
		return score;
	}
	public void setScore(WorkPackageScore score) {
		this.score = score;
	}
}
