package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@Table(name = "hierarchyValue", uniqueConstraints = @UniqueConstraint(columnNames = "hierarchyID"))
public class HierarchyValue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3183970602504124639L;
	
	private Long hierarchyID;
	private Long companyID;
	private Long productID;
	private Long parentHierarchyID;
	private Long hierarchyTypeID;
	private String hierarchyName;
	private String tableName;
	private Long fieldID;
	
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
	public Long getHierarchyID() {
		return hierarchyID;
	}
	public void setHierarchyID(Long hierarchyID) {
		this.hierarchyID = hierarchyID;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	@Column(columnDefinition="INT")
	public Long getCompanyID() {
		return companyID;
	}
	public void setCompanyID(Long companyID) {
		this.companyID = companyID;
	}

	@Column(columnDefinition="INT")
	public Long getProductID() {
		return productID;
	}
	public void setProductID(Long productID) {
		this.productID = productID;
	}

	@Column(columnDefinition="INT")
	public Long getParentHierarchyID() {
		return parentHierarchyID;
	}
	public void setParentHierarchyID(Long parentHierarchyID) {
		this.parentHierarchyID = parentHierarchyID;
	}
	
	@Column(columnDefinition="INT")
	public Long getHierarchyTypeID() {
		return hierarchyTypeID;
	}
	public void setHierarchyTypeID(Long hierarchyTypeID) {
		this.hierarchyTypeID = hierarchyTypeID;
	}
	public String getHierarchyName() {
		return hierarchyName;
	}
	public void setHierarchyName(String hierarchyName) {
		this.hierarchyName = hierarchyName;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	@Column(columnDefinition="INT")
	public Long getFieldID() {
		return fieldID;
	}
	public void setFieldID(Long fieldID) {
		this.fieldID = fieldID;
	}
}
