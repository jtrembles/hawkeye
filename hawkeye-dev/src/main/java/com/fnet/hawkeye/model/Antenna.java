package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "antenna", uniqueConstraints = @UniqueConstraint(columnNames = "antennaID"))

public class Antenna implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4546869862192420179L;

	private Long antennaID;
	private Site site;
	private Technology technology;
	private String antennaType;
	private Double antennaAngle;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="antennaID",columnDefinition="INT")
	public Long getAntennaID() {
		return antennaID;
	}
	public void setAntennaID(Long antennaID) {
		this.antennaID = antennaID;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="siteID",columnDefinition="INT")
	public Site getSite() {
		return site;
	}
	public void setSite(Site site) {
		this.site = site;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="technologyID",columnDefinition="INT")
	public Technology getTechnology() {
		return technology;
	}
	public void setTechnology(Technology technology) {
		this.technology = technology;
	}
	@Column(name="antennaType")
	public String getAntennaType() {
		return antennaType;
	}
	public void setAntennaType(String antennaType) {
		this.antennaType = antennaType;
	}
	@Column(name="antennaAngle")
	public Double getAntennaAngle() {
		return antennaAngle;
	}
	public void setAntennaAngle(Double antennaAngle) {
		this.antennaAngle = antennaAngle;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Column(name="creationDate")
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	@Column(name="lastModifiedDate")
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

}
