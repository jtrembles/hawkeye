package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class WorkloadDetail implements Serializable {

	private static final long serialVersionUID = -4545569862192420179L;

	long userid;
	String caseNumber;
	String lastName;
	String firstName;
	String siteName;
	Timestamp creationdate;
	String status;
	String turf;
	String notes;
	Number daysOpen;
	private BigDecimal longitude;
	private BigDecimal latitude;
	long workpackageid;
	
	
	
	public long getWorkpackageid() {
		return workpackageid;
	}
	public void setWorkpackageid(long workpackageid) {
		this.workpackageid = workpackageid;
	}
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public Timestamp getCreationdate() {
		return creationdate;
	}
	public void setCreationdate(Timestamp creationdate) {
		this.creationdate = creationdate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTurf() {
		return turf;
	}
	public void setTurf(String turf) {
		this.turf = turf;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Number getDaysOpen() {
		return daysOpen;
	}
	public void setDaysOpen(Number daysOpen) {
		this.daysOpen = daysOpen;
	}
	
	public BigDecimal getLongitude() {
		return longitude;
	}
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

}
