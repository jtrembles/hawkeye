package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "threshold", uniqueConstraints = @UniqueConstraint(columnNames = "thresholdID"))
public class Threshold implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7349421629028227668L;

	
	private Long ThresholdID;
	private Company company;
	private Product product;
	private Long hierarchyID;
	private String thresholdPurpose;
	private String thresholdType;
	private String thresholdName;
	private String thresholdValue;
	private Long parameter;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
	public Long getThresholdID() {
		return ThresholdID;
	}
	public void setThresholdID(Long thresholdID) {
		ThresholdID = thresholdID;
	}
	
	@Column(columnDefinition="INT")
	public Long getHierarchyID() {
		return hierarchyID;
	}
	
	public void setHierarchyID(Long hierarchyID) {
		this.hierarchyID = hierarchyID;
	}
	public String getThresholdPurpose() {
		return thresholdPurpose;
	}
	public void setThresholdPurpose(String thresholdPurpose) {
		this.thresholdPurpose = thresholdPurpose;
	}
	public String getThresholdType() {
		return thresholdType;
	}
	public void setThresholdType(String thresholdType) {
		this.thresholdType = thresholdType;
	}
	public String getThresholdName() {
		return thresholdName;
	}
	public void setThresholdName(String thresholdName) {
		this.thresholdName = thresholdName;
	}
	public String getThresholdValue() {
		return thresholdValue;
	}
	public void setThresholdValue(String thresholdValue) {
		this.thresholdValue = thresholdValue;
	}
	
	@Column(columnDefinition="INT")
	public Long getParameter() {
		return parameter;
	}
	public void setParameter(Long parameter) {
		this.parameter = parameter;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="companyID",columnDefinition="INT")
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="productID",columnDefinition="INT")
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
}
