package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "site", uniqueConstraints = @UniqueConstraint(columnNames = "siteID"))
public class Site implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1239819352646152106L;

	private Long siteId;
	private String name;
	private Address address;
	private Company company;
	private Product product;
	private Sector sector;
	private Technology technology;
	private Double longitude;
	private Double latitude;
	private Long cellId;
	private String owner;
	private String carrier;
	private String backhaul;
	private String electricProvidor;
	private String batteryInfo;
	private Usid usid;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;
	
	private HierarchyValue hierarchy;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
	public Long getSiteID() {
		return siteId;
	}
	public void setSiteID(Long siteId) {
		this.siteId = siteId;
	}
	public String getSiteName() {
		return name;
	}
	public void setSiteName(String name) {
		this.name = name;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="companyID",columnDefinition="INT")
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="productID",columnDefinition="INT")
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="sectorID",columnDefinition="INT")
	public Sector getSector() {
		return this.sector;
	}
	public void setSector(Sector sector) {
		this.sector = sector;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="technologyID",columnDefinition="INT")
	public Technology getTechnology() {
		return technology;
	}
	public void setTechnology(Technology technology) {
		this.technology = technology;
	}
	@Column(columnDefinition="decimal", precision=10, scale=7)
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	@Column(columnDefinition="decimal", precision=10, scale=7)
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	@Column(columnDefinition="INT")
	public Long getCellId() {
		return cellId;
	}
	public void setCellId(Long cellId) {
		this.cellId = cellId;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public String getBackhaul() {
		return backhaul;
	}
	public void setBackhaul(String backhaul) {
		this.backhaul = backhaul;
	}
	public String getElectricProvider() {
		return electricProvidor;
	}
	public void setElectricProvider(String electricProvidor) {
		this.electricProvidor = electricProvidor;
	}
	public String getBatteryInfo() {
		return batteryInfo;
	}
	public void setBatteryInfo(String batteryInfo) {
		this.batteryInfo = batteryInfo;
	}
 
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	
	@OneToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name="hierarchyID",columnDefinition="INT")
	public HierarchyValue getHierarchy() {
		return hierarchy;
	}
	public void setHierarchy(HierarchyValue hierarchy) {
		this.hierarchy = hierarchy;
	}

	//the following JoinColumn does not work if this is an @OneToOne mapping.  Don't know why
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name="siteID", insertable=false, updatable=false, referencedColumnName="siteID")
	public Usid getUsid() {
		return usid;
	}
	public void setUsid(Usid usid) {
		this.usid = usid;
	}

	@OneToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name="addressID",columnDefinition="INT")
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}

}
