package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "workPackage", uniqueConstraints = @UniqueConstraint(columnNames = "workPackageID"))
public class WorkPackage implements Serializable {

	private static final long serialVersionUID = 2193359284305908203L;

//	@NotNull
//	@NotEmpty
//	@Size(min = 8, max = 25, message = "8-25 letters")
//	@Pattern(regexp = "[A-Za-z1-9]*", message = "No spaces");
	
	private Long workPackageID;
	private String caseNumber;
	private Long hierarchyReportID;
	private Company company;
	private Product product;
	private Site site;
	private Sector sector;
	private Status status;
	private Technology technology;
	private GroupType groupType;
	private Priority priority;
	private User assignedUser;
	private Resolution resolution;
	private Long siteScore; 
	private String opsTurf;
	private String frequency;
	private String uplinkChFreq;
//	private BigDecimal rSSI;
	private Long rSSI;
	private String engNotes;
	private String iehNotes;
	private String projectManagerNotes;
	private String engineeringZone;
	private String rfEngineer;
	private String rfEngineerNumber;
	private String city;
//	private CtnWorkPackage customerTicketNumber;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;
//	private Set<Note> notes;

	public WorkPackage() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="workPackageID", unique=true, nullable=false, columnDefinition="INT")
	public Long getWorkPackageID() {
		return workPackageID;
	}

	public void setWorkPackageID(Long workPackageID) {
		this.workPackageID = workPackageID;
	}

	@Column(columnDefinition="INT")
	public Long getSiteScore() {
		return siteScore;
	}

	public void setSiteScore(Long siteScore) {
		this.siteScore = siteScore;
	}
	@Column(name="opsTurf")
	public String getOpsTurf() {
		return opsTurf;
	}

	public void setOpsTurf(String opsTurf) {
		this.opsTurf = opsTurf;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="siteID",columnDefinition="INT")
	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}


	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="sectorID",columnDefinition="INT")
	public Sector getSector() {
		return sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="technologyID",columnDefinition="INT")
	public Technology getTechnology() {
		return technology;
	}

	public void setTechnology(Technology technology) {
		this.technology = technology;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getUplinkChFreq() {
		return uplinkChFreq;
	}

	public void setUplinkChFreq(String uplinkChFreq) {
		this.uplinkChFreq = uplinkChFreq;
	}

	@Column(name="RSSI", columnDefinition="INT")
	public Long getrSSI() {
		return rSSI;
	}

	public void setrSSI(Long rSSI) {
		this.rSSI = rSSI;
	}

	@Column(name = "engNotes", columnDefinition="TEXT")
	public String getEngNotes() {
		return engNotes;
	}

	public void setEngNotes(String engNotes) {
		this.engNotes = engNotes;
	}

	@Column(name = "iehNotes", columnDefinition="TEXT")
	public String getIehNotes() {
		return iehNotes;
	}

	public void setIehNotes(String iehNotes) {
		this.iehNotes = iehNotes;
	}
	
	@Column(name = "projectMgrNotes", columnDefinition="TEXT")
	public String getProjectManagerNotes() {
		return projectManagerNotes;
	}

	public void setProjectManagerNotes(String projectManagerNotes) {
		this.projectManagerNotes = projectManagerNotes;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="jobTypeID",columnDefinition="INT")
	public GroupType getGroupType() {
		return groupType;
	}

	public void setGroupType(GroupType groupType) {
		this.groupType = groupType;
	}

	public String getEngineeringZone() {
		return engineeringZone;
	}

	public void setEngineeringZone(String engZone) {
		this.engineeringZone = engZone;
	}

	public String getRfEngineer() {
		return rfEngineer;
	}

	public void setRfEngineer(String rfEngineer) {
		this.rfEngineer = rfEngineer;
	}

	public String getRfEngineerNum() {
		return rfEngineerNumber;
	}

	public void setRfEngineerNum(String rfEngineerNumber) {
		this.rfEngineerNumber = rfEngineerNumber;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="priorityID",columnDefinition="INT")
	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="resolutionID")
	public Resolution getResolution() {
		return resolution;
	}

	public void setResolution(Resolution resolution) {
		this.resolution = resolution;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="companyID",columnDefinition="INT")
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Timestamp getCreationDate() {
		if (creationDate == null)
			creationDate = new Timestamp(new Date().getTime());
		return creationDate;
	}

	public void setCreationDate(Timestamp timestamp) {
		if (timestamp == null)
			timestamp = new Timestamp(new Date().getTime());
		this.creationDate = timestamp;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="statusID",columnDefinition="INT")
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status currentStatus) {
		this.status = currentStatus;
	}

	@Column(columnDefinition="INT")
	public Long getHierarchyReportID() {
		return hierarchyReportID;
	}

	public void setHierarchyReportID(Long hierarchyReportID) {
		this.hierarchyReportID = hierarchyReportID;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="productID",columnDefinition="INT")
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="assignedUserID",columnDefinition="INT")
	public User getAssignedUser() {
		return assignedUser;
	}

	public void setAssignedUser(User assignedUser) {
		this.assignedUser = assignedUser;
	}

	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	// In Customer class:

//    @OneToMany(cascade=CascadeType.REFRESH, fetch = FetchType.EAGER)
//    @JoinColumn(name="workPackageID",columnDefinition="INT")
//    public Set<Note> getNotes() {
//    	return notes; 
//    }
//    
//    public void setNotes(Set<Note> notes){
//    	this.notes = notes;
//    }
//	public CtnWorkPackage getCustomerTicketNumber() {
//		return customerTicketNumber;
//	}
//
//	public void setCustomerTicketNumber(CtnWorkPackage customerTicketNumber) {
//		this.customerTicketNumber = customerTicketNumber;
//	}
//
//	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//	@JoinTable(name = "userJobTypeGroup", catalog = "hawkeye", joinColumns = { 
//			@JoinColumn(name = "userID", nullable = false, updatable = false) }, 
//			inverseJoinColumns = { @JoinColumn(name = "groupTypeID", 
//					nullable = false, updatable = false) })
//	public Set<GroupType> getGroupTypes() {
//		return this.groupTypes;
//	}
// 
//	public void setGroupTypes(Set<GroupType> groupTypes) {
//		this.groupTypes = groupTypes;
//	}
//
//

}
