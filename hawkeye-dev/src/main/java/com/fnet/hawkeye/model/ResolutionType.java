package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "resolutionType", uniqueConstraints = @UniqueConstraint(columnNames = "resolutionTypeID"))
public class ResolutionType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 30138870292846139L;

	private Long resolutionTypeID;
	private String resolutionTypeName;	
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
 	public Long getResolutionTypeID() {
		return resolutionTypeID;
	}
	public void setResolutionTypeID(Long resolutionTypeID) {
		this.resolutionTypeID = resolutionTypeID;
	}
	public String getResolutionTypeName() {
		return resolutionTypeName;
	}
	public void setResolutionTypeName(String resolutionTypeName) {
		this.resolutionTypeName = resolutionTypeName;
	}
	
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
}
