package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@Table(name = "hierarchyType", uniqueConstraints = @UniqueConstraint(columnNames = "hierarchyTypeID"))
public class HierarchyType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3183970602504124639L;
	
	private Long hierarchyTypeID;
	private String hierarchyTypeName;
	private String description;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
	public Long getHierarchyTypeID() {
		return hierarchyTypeID;
	}
	public void setHierarchyTypeID(Long hierarchyTypeID) {
		this.hierarchyTypeID = hierarchyTypeID;
	}
		
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public String getHierarchyTypeName() {
		return hierarchyTypeName;
	}
	public void setHierarchyTypeName(String hierarchyTypeName) {
		this.hierarchyTypeName = hierarchyTypeName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}