package com.fnet.hawkeye.model;

import java.io.Serializable;

public class HierarchyIdHolder implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6755919846029800909L;
	
	private Number hierarchyID;

	public Number getHierarchyID() {
		return hierarchyID;
	}

	public void setHierarchyID(Number hierarchyID) {
		this.hierarchyID = hierarchyID;
	}
}
