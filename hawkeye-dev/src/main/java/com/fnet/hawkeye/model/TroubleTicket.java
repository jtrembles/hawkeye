package com.fnet.hawkeye.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "troubleTicket", uniqueConstraints = @UniqueConstraint(columnNames = "troubleTicketID"))
public class TroubleTicket implements Serializable {

	private static final long serialVersionUID = 2193359284305908203L;

//	@NotNull
//	@NotEmpty
//	@Size(min = 8, max = 25, message = "8-25 letters")
//	@Pattern(regexp = "[A-Za-z1-9]*", message = "No spaces");
	
	private Long troubleTicketID;
	private WorkPackage workPackage;
	private String troubleTicketNumber;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="troubleTicketID", unique=true, nullable=false, columnDefinition="INT")
	public Long getTroubleTicketIDID() {
		return troubleTicketID;
	}

	public void setTroubleTicketIDID(Long troubleTicketID) {
		this.troubleTicketID = troubleTicketID;
	}

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name="workPackageID",columnDefinition="INT")
	public WorkPackage getWorkPackage() {
		return workPackage;
	}

	public void setWorkPackage(WorkPackage workPackage) {
		this.workPackage = workPackage;
	}
	
	public String getTroubleTicketNumber() {
		return troubleTicketNumber;
	}

	public void setTroubleTicketNumber(String customerTicketNumber) {
		this.troubleTicketNumber = customerTicketNumber;
	}
}
