package com.fnet.hawkeye.model;

import java.io.Serializable;

public class WorkPackageScore implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -529280706477271902L;
	private String phase;
	private Number datedifference;
	private Number target;
	private Boolean kpimiss;
	
	public String getPhase() {
		return phase;
	}
	public void setPhase(String phase) {
		this.phase = phase;
	}
	public Number getDatedifference() {
		return datedifference;
	}

	public void setDatedifference(Number datedifference) {
		this.datedifference = datedifference;
	}

	public Number getTarget() {
		return target;
	}
	public void setTarget(Number target) {
		this.target = target;
	}
	public Boolean getKpimiss() {
		return kpimiss;
	}
	public void setKpimiss(Boolean kpimiss) {
		this.kpimiss = kpimiss;
	}
}
