package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@Table(name = "address", uniqueConstraints = @UniqueConstraint(columnNames = "addressID"))

public class Address  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5233752288714617256L;

	private Long addressID;
	private String address;
	private String address2;
	private City city;
	private String province;
	private String postal_code;
	private Double latitude; //	Float longitude;// decimal(10,7)
	private Double longitude; //	Float latitude;// decimal(10,7),
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
	public Long getAddressID() {
		return addressID;
	}

	public void setAddressID(Long addressID) {
		this.addressID = addressID;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getAddress2() {
		return address2;
	}
	
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	
	public String getProvince() {
		return province;
	}
	
	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getPostal_code() {
		return postal_code;
	}
	
	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}
	
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	@Column(columnDefinition="decimal", precision=10, scale=7)
	public Double getLongitude() {
		return longitude;
	}
	
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	@Column(columnDefinition="decimal", precision=10, scale=7)
	public Double getLatitude() {
		return latitude;
	}
	
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="city_id",columnDefinition="INT", referencedColumnName="cityID")
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}


	
}
