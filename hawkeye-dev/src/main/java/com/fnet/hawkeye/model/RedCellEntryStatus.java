package com.fnet.hawkeye.model;

public class RedCellEntryStatus {
	
	private String CaseNumber;
	private String Status;
	private String Reason;
	private String Data;
	
	public RedCellEntryStatus( String Case, String Status, String Reason, String Data) {
		
		setCaseNumber(Case);
		setStatus(Status);
		setReason(Reason);
		setData(Data);
		
	}
	
	public String toString(){
		StringBuilder html = new StringBuilder();
		html.append("{");
		html.append("\"caseNumber\":\"" + (CaseNumber != null ? CaseNumber :  "")  +"\",");
		html.append("\"status\":\"" + (Status != null ? Status :  "" ) + "\",");
		html.append("\"reason\":\"" + (Reason != null ? Reason :  "" ) +"\",");
//		Data.replace('~', ' ');
		html.append("\"data\" :\"" + (Data != null ? Data :  "") + "\"");
		html.append("}");
		return html.toString();
	}
	
	
	
	public RedCellEntryStatus() {
		
	}

	public String getCaseNumber() {
		return CaseNumber;
	}
	public void setCaseNumber(String case1) {
		CaseNumber = case1.replace("\n", "").replace("\r", "");
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status.replace("\n", "").replace("\r", "");
	}
	public String getReason() {
		return Reason;
	}
	public void setReason(String reason) {
		Reason = reason.replace("\n", "").replace("\r", "");
	}
	public String getData() {
		return Data;
	}
	public void setData(String data) {
		Data = data.replace("\n", "").replace("\r", "");
	}
}
