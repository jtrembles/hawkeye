package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "attachmentType", uniqueConstraints = @UniqueConstraint(columnNames = "attachmentTypeID"))
public class AttachmentType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7252144912249041367L;

	
	private Long attachmentTypeID;
	private String attachmentTypeName;	
	private Boolean attachmentValid;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
	public Long getAttachmentTypeID() {
		return attachmentTypeID;
	}
	public void setAttachmentTypeID(Long attachmentTypeId) {
		this.attachmentTypeID = attachmentTypeId;
	}
		public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getAttachmentTypeName() {
		return attachmentTypeName;
	}

	public void setAttachmentTypeName(String attachmentTypeName) {
		this.attachmentTypeName = attachmentTypeName;
	}

	@Column(name = "attachmentValid", columnDefinition = "BIT", length = 1)
	public Boolean getAttachmentValid() {
		return attachmentValid;
	}

	public void setAttachmentValid(Boolean attachmentValid) {
		this.attachmentValid = attachmentValid;
	}
}
