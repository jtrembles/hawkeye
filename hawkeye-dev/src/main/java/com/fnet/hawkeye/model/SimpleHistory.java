package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@Table(name = "history", uniqueConstraints = @UniqueConstraint(columnNames = "historyID"))
public class SimpleHistory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3183970602504124639L;
	
	private Long historyID;
	private Long workPackageID;
	private Long currentStatusID;
	private Timestamp currentStatusCreationDate;
	private Long newStatusID;
	private String notes;
	private Long userID;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;
	private Long historyTypeID;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
	public Long getHistoryID() {
		return historyID;
	}
	public void setHistoryID(Long historyID) {
		this.historyID = historyID;
	}
	
	@Column(name="workPackageID",columnDefinition="INT")
	public Long getWorkPackageID() {
		return workPackageID;
	}
	public void setWorkPackageID(Long workPackageID) {
		this.workPackageID = workPackageID;
	}
	
	@Column(name="userID",columnDefinition="INT")
	public Long getUserID() {
		return userID;
	}
	
	public void setUserID(Long userID) {
		this.userID = userID;
	}
	
	@Column(name = "notes", columnDefinition="TEXT")
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	
	@Column(name="currentStatusCreationDate")
	public Timestamp getCurrentStatusCreationDate() {
		return currentStatusCreationDate;
	}
	public void setCurrentStatusCreationDate(Timestamp currentStatusCreationDate) {
		this.currentStatusCreationDate = currentStatusCreationDate;
	}
	
	@Column(name="historyTypeID",columnDefinition="INT")
	public Long getHistoryTypeID() {
		return historyTypeID;
	}
	public void setHistoryTypeID(Long historyTypeID) {
		this.historyTypeID = historyTypeID;
	}
	
	@Column(name="currentStatusID", columnDefinition="INT")
	public Long getCurrentStatusID() {
		return currentStatusID;
	}
	public void setCurrentStatusID(Long currentStatusID) {
		this.currentStatusID = currentStatusID;
	}
	
	@Column(name="newStatusID", columnDefinition="INT")
	public Long getNewStatusID() {
		return newStatusID;
	}
	public void setNewStatusID(Long newStatusID) {
		this.newStatusID = newStatusID;
	}
}
