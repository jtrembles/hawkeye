package com.fnet.hawkeye.model;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class RedCellEntry {
	//@Inject
	//private Logger log;
	
	private String Priority;
	private String Site;
	private String Sector;
	private String Technology;
	private Double Frequency;
	private String Uplink;
	private Integer RSSILevel;
	private String TimeOfDay;
	private String EngZone;
	private String RFEngineer;
	private String Number;
	private String City;
	private String EngNotes;
	private String Site2;
	private String DateReceived;
	private String OpsTurf;
	private String IEH;
	private String DateAssigned;
	private String IEHNotes;
	private String PMNotes;
	private String FinalStatusDate;
	private String FinalStatus;
	private String FinalStatusSummary;
	private String TroubleTicket;
	private String TroubleType;
	
	public RedCellEntry( String line ) throws Exception {
		StringTokenizer tok = new StringTokenizer(line, ",");
		String lsSkip; 
		
		try {														//column
//			setPriority(stripAndScrub(tok.nextToken())); 			//A
//			setSite(stripAndScrub(tok.nextToken()));				//B
//			setSector(stripAndScrub(tok.nextToken()));				//C
//			setTechnology(stripAndScrub(tok.nextToken()));			//D
//			setFrequency(stripAndScrub(tok.nextToken()));			//E
//			setUplink(stripAndScrub(tok.nextToken()));				//F
//			setRSSILevel(stripAndScrub(tok.nextToken()));			//G
//			setTimeOfDay(stripAndScrub(tok.nextToken()));			//H
//			setEngZone(stripAndScrub(tok.nextToken()));				//I
//			setRFEngineer(stripAndScrub(tok.nextToken()));			//J
//			setNumber(stripAndScrub(tok.nextToken()));				//K
//			setCity(stripAndScrub(tok.nextToken()));				//L
//			setEngNotes(stripAndScrub(tok.nextToken()));			//M
//			setSite2(stripAndScrub(tok.nextToken()));				//N
//			setDateReceived(stripAndScrub(tok.nextToken()));		//O
//			setOpsTurf(stripAndScrub(tok.nextToken()));				//P
//			setIEH(stripAndScrub(tok.nextToken()));					//Q
//			setDateAssigned(stripAndScrub(tok.nextToken()));		//R
//			setIEHNotes(stripAndScrub(tok.nextToken()));			//S
//			setPMNotes(stripAndScrub(tok.nextToken()));				//T
//			setFinalStatusDate(stripAndScrub(tok.nextToken()));		//U
//			setFinalStatus(stripAndScrub(tok.nextToken()));			//V
//			setFinalStatusSummary(stripAndScrub(tok.nextToken()));	//W
			
			//R.09 FORMAT
			setPriority(stripAndScrub(tok.nextToken())); 			//A
			setSite(stripAndScrub(tok.nextToken()));				//B
			setSector(stripAndScrub(tok.nextToken()));				//C
			setTechnology(stripAndScrub(tok.nextToken()));			//D
			setFrequency(stripAndScrub(tok.nextToken()));			//E
			setUplink(stripAndScrub(tok.nextToken()));				//F
			setRSSILevel(stripAndScrub(tok.nextToken()));			//G
			setTimeOfDay(stripAndScrub(tok.nextToken()));			//H
			setEngZone(stripAndScrub(tok.nextToken()));				//I
			setRFEngineer(stripAndScrub(tok.nextToken()));			//J
			setNumber(stripAndScrub(tok.nextToken()));				//K
			setCity(stripAndScrub(tok.nextToken()));				//L
			setEngNotes(stripAndScrub(tok.nextToken()));			//M
			setTroubleTicket(stripAndScrub(tok.nextToken()));		//N
			lsSkip = stripAndScrub(tok.nextToken());				//O  Assigned To
			setDateAssigned(stripAndScrub(tok.nextToken()));		//P
			lsSkip = stripAndScrub(tok.nextToken());				//Q  Resolution
			setFinalStatus(stripAndScrub(tok.nextToken()));			//R
			
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			throw e;
		} catch ( Exception ex ) {
			throw ex;
		}
		
	}
	
	public RedCellEntry( String[] lines, boolean isRFMEReport ) throws Exception {
		//log.finer(String.format("Processing tokens %s", printLines(lines)));
		
		//if ( lines.length != 25 ) {
		//	log.debug("String does not contain the 25 required tokens skipping....");
		//	throw new Exception("invalid record length");
		//}
		
		try {
			
			if ( isRFMEReport )
				loadRFMEReportFormat(lines);
			else
				loadRedCellFormat(lines);
		} catch ( Exception ex ) {
			//log.fine(String.format("Exception processing tokens %s", ex.getMessage()));
			throw ex;
		}
		
	}
	
	private void loadRFMEReportFormat(String[] lines) throws Exception {
				
		if ( lines.length > 3 )
			setTroubleTicket(stripAndScrub(lines[3]));
		if ( lines.length > 4 )
			setFinalStatus(stripAndScrub(lines[4]));
		if ( lines.length > 6 )
			setPriority(stripAndScrub(lines[6]));
		if ( lines.length > 8 )
			setSite(stripAndScrub(lines[8]));		
		if ( lines.length > 11 )
			setRFEngineer(stripAndScrub(lines[11]));
		if ( lines.length > 17 )
			setEngNotes(stripAndScrub(lines[17]));
		
		//this is how we will know it came from CTS
		//we need to process job type differently than shawns
		//list
		setTroubleType("CTS");
	}

	private void loadRedCellFormat(String[] lines) throws Exception {
		
		if ( lines.length > 0 )
			setPriority(stripAndScrub(lines[0]));
		if ( lines.length > 1 )
			setSite(stripAndScrub(lines[1]));
		if ( lines.length > 2 )
			setSector(stripAndScrub(lines[2]));
		if ( lines.length > 3 )
			setTechnology(stripAndScrub(lines[3]));
		if ( lines.length > 4 )
			setFrequency(stripAndScrub(lines[4]));
		if ( lines.length > 5 )
			setUplink(stripAndScrub(lines[5]));
		if ( lines.length > 6 )
			setRSSILevel(stripAndScrub(lines[6]));
		if ( lines.length > 7 )
			setTimeOfDay(stripAndScrub(lines[7]));
		if ( lines.length > 8 )
			setEngZone(stripAndScrub(lines[8]));
		if ( lines.length > 9 )
			setRFEngineer(stripAndScrub(lines[9]));
		if ( lines.length > 10 )
			setNumber(stripAndScrub(lines[10]));
		if ( lines.length > 11 )
			setCity(stripAndScrub(lines[11]));
		if ( lines.length > 12 )
			setEngNotes(stripAndScrub(lines[12]));
		if ( lines.length > 13 )
			setTroubleTicket(stripAndScrub(lines[13]));
		if ( lines.length > 14 ){} //skip
			//setDateReceived(stripAndScrub(lines[14]));
		if ( lines.length > 15 )
			setDateAssigned(stripAndScrub(lines[15]));
			//setOpsTurf(stripAndScrub(lines[15]));
		if ( lines.length > 16 ){} //skip;
			//setIEH(stripAndScrub(lines[16]));
		if ( lines.length > 17 )
			setDateAssigned(stripAndScrub(lines[17]));
		if ( lines.length > 18 )
			setFinalStatus(stripAndScrub(lines[18]));
//		if ( lines.length > 19)
//			setPMNotes(stripAndScrub(lines[19]));
//		if ( lines.length > 20 )
//			setFinalStatusDate(stripAndScrub(lines[20]));
//		if ( lines.length > 21 )
//			setFinalStatus(stripAndScrub(lines[21]));
//		if ( lines.length > 22 )
//			setFinalStatusSummary(stripAndScrub(lines[22]));
		
		setTroubleType("RedCell");
	}
	
	private String printLines(String[] lines) {
		
		StringBuilder sb = new StringBuilder();
		
		for ( String s : lines ) {
			sb.append(s).append('|');
		}
		
		return sb.toString();
	}

	public String getPriority() {
		return Priority;
	}
	public void setPriority(String priority) throws Exception {
		
//		if ( ! priority.equalsIgnoreCase("TOP") )
//			throw new Exception("Priority must be TOP, invalid record");
		Priority = priority;
	}
	public String getSite() {
		return Site;
	}
	public void setSite(String site) {
		Site = site;
	}
	public String getSector() {
		return Sector;
	}
	public void setSector(String sector) {
		if ( sector.contains(",") )
			Sector = sector.substring(0, sector.indexOf(',')).trim();
		else
			Sector = sector;
	}
	public String getTechnology() {
		return Technology;
	}
	public void setTechnology(String technology) {
		Technology = technology;
	}
	public Double getFrequency() {
		return Frequency;
	}
	public void setFrequency(String frequency) throws Exception {
		
		if ( frequency == null )
			throw new Exception("Frequency can not be null");
		
		StringBuilder sb = new StringBuilder();
		
		for (int x = 0; x < frequency.length(); x++ ) {
			
			char c = frequency.charAt(x);
			
			if ( (c >= '0' && c <= '9') )
				sb.append(c);
			else break;
		}
		
		try {
			Frequency = Double.parseDouble(sb.toString());
		} catch (NumberFormatException e) {
			throw new Exception("Error setting Frequency " + frequency + " is not numeric");
		}		
	}
	public String getUplink() {
		return Uplink;
	}
	public void setUplink(String uplink) {
		Uplink = uplink;
	}
	public Integer getRSSILevel() {
		return RSSILevel;
	}
	public void setRSSILevel(String rSSILevel) throws Exception {
		
		if ( rSSILevel == null )
			throw new Exception("RSSI Level can not be null");
		
		try {
			RSSILevel = Integer.parseInt(rSSILevel);
		} catch (NumberFormatException e) {
			throw new Exception("Error setting RSSI Level " + rSSILevel + " is not an integer");
		}
	}
	public String getTimeOfDay() {
		return TimeOfDay;
	}
	public void setTimeOfDay(String timeOfDay) {
		TimeOfDay = timeOfDay;
	}
	public String getEngZone() {
		return EngZone;
	}
	public void setEngZone(String engZone) {
		EngZone = engZone;
	}
	public String getRFEngineer() {
		return RFEngineer;
	}
	public void setRFEngineer(String rFEngineer) {
		RFEngineer = rFEngineer;
	}
	public String getNumber() {
		return Number;
	}
	public void setNumber(String number) {
		Number = number;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getEngNotes() {
		return EngNotes;
	}
	public void setEngNotes(String engNotes) {
		EngNotes = engNotes;
	}
	public String getSite2() {
		return Site2;
	}
	public void setSite2(String site2) {
		Site2 = site2;
	}
	public String getDateReceived() {
		return DateReceived;
	}
	public void setDateReceived(String dateReceived) {
		DateReceived = dateReceived;
	}
	public String getOpsTurf() {
		return OpsTurf;
	}
	public void setOpsTurf(String opsTurf) {
		OpsTurf = opsTurf;
	}
	public String getIEH() {
		return IEH;
	}
	public void setIEH(String iEH) {
		IEH = iEH;
	}
	public String getDateAssigned() {
		return DateAssigned;
	}
	public void setDateAssigned(String dateAssigned) {
		DateAssigned = dateAssigned;
	}
	public String getIEHNotes() {
		return IEHNotes;
	}
	public void setIEHNotes(String iEHNotes) {
		IEHNotes = iEHNotes;
	}
	public String getPMNotes() {
		return PMNotes;
	}
	public void setPMNotes(String pMNotes) {
		PMNotes = pMNotes;
	}
	public String getFinalStatusDate() {
		return FinalStatusDate;
	}
	public void setFinalStatusDate(String finalStatusDate) {
		FinalStatusDate = finalStatusDate;
	}
	public String getFinalStatus() {
		return FinalStatus;
	}
	public void setFinalStatus(String finalStatus) {
		FinalStatus = finalStatus;
	}
	public String getFinalStatusSummary() {
		return FinalStatusSummary;
	}
	public void setFinalStatusSummary(String finalStatusSummary) {
		FinalStatusSummary = finalStatusSummary;
	}

	public String getCaseNumber() {
		if(getTroubleTicket() != null && getTroubleTicket().length() > 0)
			return getTroubleTicket();

		return getTroubleTicket();
	}
	
	private String stripAndScrub( String in ) {
		if ( in == null || in.isEmpty() )
			return in;
		return in.trim().replaceAll("[^\\p{Print}]", "");		
	}

	public String getTroubleTicket() {
		return TroubleTicket;
	}

	public void setTroubleTicket(String troubleTicket) {
		TroubleTicket = troubleTicket;
	}
	
	public String getTroubleType() {
		return TroubleType;
	}

	public void setTroubleType(String troubleType) {
		TroubleType = troubleType;
	}
}
