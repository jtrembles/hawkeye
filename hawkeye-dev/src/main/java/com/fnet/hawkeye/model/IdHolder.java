package com.fnet.hawkeye.model;

import java.io.Serializable;

public class IdHolder implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4513932332197205396L;

	private Number foundID;

	public Number getFoundID() {
		return foundID;
	}

	public void setFoundID(Number foundID) {
		this.foundID = foundID;
	}

	
}
