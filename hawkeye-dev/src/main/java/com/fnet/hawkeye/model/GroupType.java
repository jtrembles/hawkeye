package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@Table(name = "groupType", uniqueConstraints = @UniqueConstraint(columnNames = "groupTypeID"))
public class GroupType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 562941829314449183L;

	private Long groupTypeID;
	private String groupName;
	private Company company;
	private Product product;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;
	private String groupType;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
	public Long getGroupTypeID() {
		return groupTypeID;
	}
	public void setGroupTypeID(Long groupID) {
		this.groupTypeID = groupID;
	}
	
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	
	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="companyID",columnDefinition="INT")
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
 
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="productID",columnDefinition="INT")
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o != null && o.getClass() == GroupType.class)
			return ((GroupType)o).groupTypeID.longValue() == this.groupTypeID.longValue();
		
		return false;
	}
	
	public int hashCode()
	{
		return this.groupTypeID.hashCode();
	}
}
