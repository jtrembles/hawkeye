package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "attachment", uniqueConstraints = @UniqueConstraint(columnNames = "attachmentID"))
public class Attachment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7252144912249041367L;

	
	private Long attachmentID;
	private WorkPackage workPackage;
	private String docName;
	private String docType;
	private String docLabel;	
	private String docLocation;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
	public Long getAttachmentID() {
		return attachmentID;
	}
	public void setAttachmentID(Long attachmentId) {
		this.attachmentID = attachmentId;
	}
	
	@OneToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name="workPackageID",columnDefinition="INT")
	public WorkPackage getWorkPackage() {
		return workPackage;
	}
	public void setWorkPackage(WorkPackage workPackage) {
		this.workPackage = workPackage;
	}
	public String getDocName() {
		return docName;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public String getDocLocation() {
		return docLocation;
	}
	public void setDocLocation(String docLocation) {
		this.docLocation = docLocation;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public String getDocLabel() {
		return docLabel;
	}
	public void setDocLabel(String docLabel) {
		this.docLabel = docLabel;
	}
}
