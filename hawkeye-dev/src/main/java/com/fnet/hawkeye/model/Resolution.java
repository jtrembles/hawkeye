package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "resolution", uniqueConstraints = @UniqueConstraint(columnNames = "resolutionID"))
public class Resolution implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 30138870292846139L;

	private Long resolutionID;
	private ResolutionType resolutionType;
	private String resolutionName;
	private Product product;
	private String resolutionDescription;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
	public Long getResolutionID() {
		return resolutionID;
	}
	public void setResolutionID(Long resolutionId) {
		this.resolutionID = resolutionId;
	}
	@Column(name="resolutionDescription",  columnDefinition="TEXT")
	public String getResolutionDescription() {
		return resolutionDescription;
	}
	public void setResolutionDescription(String description) {
		this.resolutionDescription = description;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="resolutionTypeID",columnDefinition="INT")
	public ResolutionType getResolutionType() {
		return resolutionType;
	}
	public void setResolutionType(ResolutionType resolutionType) {
		this.resolutionType = resolutionType;
	}
	@Column(name="resolutionName")
	public String getResolutionName() {
		return resolutionName;
	}
	public void setResolutionName(String resolutionName) {
		this.resolutionName = resolutionName;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="productID",columnDefinition="INT")
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
}
