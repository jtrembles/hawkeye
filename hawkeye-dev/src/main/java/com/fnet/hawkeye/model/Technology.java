package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "technology", uniqueConstraints = @UniqueConstraint(columnNames = "technologyID"))
public class Technology implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7349421629028227668L;
	private Long TechnologyId;
	private String type;
	private String name;
	private Double bandwidth;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
	public Long getTechnologyID() {
		return TechnologyId;
	}
	public void setTechnologyID(Long technologyId) {
		TechnologyId = technologyId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTechnologyName() {
		return name;
	}
	public void setTechnologyName(String name) {
		this.name = name;
	}
	public Double getBandwidth() {
		return bandwidth;
	}
	public void setBandwidth(Double bandwidth) {
		this.bandwidth = bandwidth;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	
}
