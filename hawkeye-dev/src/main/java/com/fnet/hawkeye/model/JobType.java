package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

//@Entity
//@XmlRootElement
//@Table(name = "jobType", uniqueConstraints = @UniqueConstraint(columnNames = "jobTypeID"))
public class JobType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2261182248221872788L;

	private Long jobTypeId;
	private String name;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;
	private Company company;
	private Product product;
	
	
//	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
//	@Column(columnDefinition="INT")
	public Long getJobTypeID() {
		return jobTypeId;
	}
	
	public void setJobTypeID(Long jobTypeId) {
		this.jobTypeId = jobTypeId;
	}
	
	public String getJobTypeName() {
		return name;
	}
	
	public void setJobTypeName(String name) {
		this.name = name;
	}
 
//	@OneToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name="companyID",columnDefinition="INT")
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	
//	@OneToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name="productID",columnDefinition="INT")
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	} 

	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
}
