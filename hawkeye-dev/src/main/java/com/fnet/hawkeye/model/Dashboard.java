package com.fnet.hawkeye.model;

import java.io.Serializable;

public class Dashboard implements Serializable {

	private static final long serialVersionUID = -4546869862192420179L;
	String jobType;
	String groupType;
	String status;
	Number totalWorkPackages;
	Number threshold;
	Number jeopardy;
	Number target;
	Number reportAverage;
	
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Number getTotalWps() {
		return totalWorkPackages;
	}
	public void setTotalWps(Number totalWorkPackages) {
		this.totalWorkPackages = totalWorkPackages;
	}
	public Number getThreshold() {
		return threshold;
	}
	public void setThreshold(Number threshold) {
		this.threshold = threshold;
	}
	public Number getJeopardy() {
		return jeopardy;
	}
	public void setJeopardy(Number jeopardy) {
		this.jeopardy = jeopardy;
	}
	public Number getTarget() {
		return target;
	}
	public void setTarget(Number target) {
		this.target = target;
	}
	public Number getReportAverage() {
		return reportAverage;
	}
	public void setReportAverage(Number reportAverage) {
		this.reportAverage = reportAverage;
	}


}
