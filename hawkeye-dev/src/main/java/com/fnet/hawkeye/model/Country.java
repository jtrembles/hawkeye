package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@Table(name = "country", uniqueConstraints = @UniqueConstraint(columnNames = "countryID"))

public class Country  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4777439058374129630L;
	private Long countryID;
	private String country;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="countryID", columnDefinition="INT")
	public Long getCountryID() {
		return countryID;
	}
	
	public void setCountryID(Long countryID) {
		this.countryID = countryID;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}
	
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
}
