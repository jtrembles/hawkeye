package com.fnet.hawkeye.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "site", uniqueConstraints = @UniqueConstraint(columnNames = "siteID"))
public class SimpleSite {

	private Long siteID;
	private Long hierarchyID;
	private String name;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
	public Long getSiteID() {
		return siteID;
	}

	public void setSiteID(Long siteID) {
		this.siteID = siteID;
	}

	@Column(name="hierarchyID", columnDefinition="INT")
	public Long getHierarchyID() {
		return hierarchyID;
	}

	public void setHierarchyID(Long hierarchyID) {
		this.hierarchyID = hierarchyID;
	}

	public String getSiteName() {
		return name;
	}

	public void setSiteName(String name) {
		this.name = name;
	}
}
