package com.fnet.hawkeye.model;

import java.io.Serializable;

public class WorkloadCloseSummary implements Serializable {

	private static final long serialVersionUID = -4545569862192420179L;

	long userid;
	String lastName;
	String firstName;
	Number cases;
	Number days;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public Number getDays() {
		return days;
	}
	public void setDays(Number numberofrecords) {
		this.days = numberofrecords;
	}
	
	public Number getCases() {
		return cases;
	}
	public void setCases(Number cases) {
		this.cases = cases;
	}
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}

}
