package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@Table(name = "usid", uniqueConstraints = @UniqueConstraint(columnNames = "usidID"))

public class Usid implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7864123862417287043L;
	private Long usidID;
	private Long usid;
	private Long addressID;
	private Long siteID;
	
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;
	
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	@Id
	@Column(name="usidID", unique=true, nullable=false, columnDefinition="INT")
	public Long getUsidID() {
		return usidID;
	}
	public void setUsidID(Long usidID) {
		this.usidID = usidID;
	}

	@Column(name="usid", columnDefinition="INT")
	public Long getUsid() {
		return usid;
	}
	public void setUsid(Long usid) {
		this.usid = usid;
	}

	@Column(name="siteID", unique=true, columnDefinition="INT")
	public Long getSiteID() {
		return siteID;
	}
	public void setSiteID(Long siteID) {
		this.siteID = siteID;
	}

	@Column(name="addressID", columnDefinition="INT")
	public Long getAddressID() {
		return addressID;
	}
	public void setAddressID(Long addressID) {
		this.addressID = addressID;
	}

}
