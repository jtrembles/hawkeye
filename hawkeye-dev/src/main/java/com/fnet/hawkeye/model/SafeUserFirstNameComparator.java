package com.fnet.hawkeye.model;

import java.util.Comparator;

public class SafeUserFirstNameComparator implements Comparator<SafeUser> {

	@Override
	public int compare(SafeUser o1, SafeUser o2) {
		if(o1 == null && o2 == null)
			return 0;
		
		if(o1 == null || o2 == null)
			return -1;
		
		return o1.getFirstName().compareToIgnoreCase(o2.getFirstName());
	}

}
