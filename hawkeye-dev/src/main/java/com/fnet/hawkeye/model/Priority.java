package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "priority", uniqueConstraints = @UniqueConstraint(columnNames = "priorityID"))
public class Priority implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5127452260458095278L;
	private Long priorityId;
	private String name;
	private Company company;
	private Product product;
	private Long severity;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
	public Long getPriorityID() {
		return priorityId;
	}
	public void setPriorityID(Long priorityId) {
		this.priorityId = priorityId;
	}
	public String getPriorityName() {
		return name;
	}
	public void setPriorityName(String name) {
		this.name = name;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="companyID",columnDefinition="INT")
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	
	@Column(columnDefinition="INT")
	public Long getSeverity() {
		return severity;
	}
	public void setSeverity(Long severity) {
		this.severity = severity;
	}
 
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="productID",columnDefinition="INT")
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}

}
