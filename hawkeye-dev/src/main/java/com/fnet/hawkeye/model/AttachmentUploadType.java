package com.fnet.hawkeye.model;

import javax.xml.bind.annotation.XmlRootElement;

import com.fnet.hawkeye.util.IToJson;

@XmlRootElement
public class AttachmentUploadType implements IToJson{
	
	private String attachmentName;
	private int attachmentSize;
	private String docUrl;
	private String thumbName;
	private String thumbUrl;
	private String docType;
	private String caption;
	private String error;
	private Long workPackageId;
	private String apiKey;
	
	private String deleteUrl;
	private String deleteType;
	
	
	public Long getWorkPackageId() {
		return workPackageId;
	}
	public void setWorkPackageId(Long workPackageId) {
		this.workPackageId = workPackageId;
	}
	
	public String getAttachmentName() {
		return attachmentName;
	}
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}
	public String getDocUrl() {
		return docUrl;
	}
	public void setDocUrl(String docUrl) {
		this.docUrl = docUrl;
	}
	public String getThumbName() {
		return thumbName;
	}
	public void setThumbName(String thumbName) {
		this.thumbName = thumbName;
	}
	public String getThumbUrl() {
		return thumbUrl;
	}
	public void setThumbUrl(String thumbUrl) {
		this.thumbUrl = thumbUrl;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public int getAttachmentSize() {
		return attachmentSize;
	}
	public void setAttachmentSize(int attachmentSize) {
		this.attachmentSize = attachmentSize;
	}
	
	public String getDeleteUrl() {
		return deleteUrl;
	}
	public void setDeleteUrl(String deleteUrl) {
		this.deleteUrl = deleteUrl;
	}
	public String getDeleteType() {
		return deleteType;
	}
	public void setDeleteType(String deleteType) {
		this.deleteType = deleteType;
	}
	public String getApiKey() {
		return (apiKey != null ? apiKey : "");
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	
	
	@Override
	public String toJson() {
		StringBuilder lBuilder = new StringBuilder("{");

		if(attachmentName != null) lBuilder.append("\"attachmentName\":\"").append(attachmentName).append("\"");
		lBuilder.append(",\"attachmentSize\":\"").append(attachmentSize).append("\"");
		if(docUrl != null) lBuilder.append(",\"docUrl\":\"").append(docUrl).append("\"");
		if(thumbName != null) lBuilder.append(",\"thumbName\":\"").append(thumbName).append("\"");
		if(thumbUrl != null) lBuilder.append(",\"thumbUrl\":\"").append(thumbUrl).append("\"");
		if(docType != null) lBuilder.append(",\"docType\":\"").append(docType).append("\"");
		if(caption != null) lBuilder.append(",\"caption\":\"").append(caption).append("\"");
		if(error != null) lBuilder.append(",\"error\":\"").append(error).append("\"");
		if(workPackageId != null) lBuilder.append(",\"workPackageId\":\"").append(workPackageId).append("\"");
		
		if(docUrl != null) lBuilder.append(String.format(",\"deleteUrl\":\"/hawkeye/rest/file/%d/%s/attachments/%s/delete\"", workPackageId, getApiKey(), attachmentName));
		lBuilder.append(",\"deleteType\":\"GET\"");
		
		lBuilder.append("}");
		
		return lBuilder.toString();
	}
}
