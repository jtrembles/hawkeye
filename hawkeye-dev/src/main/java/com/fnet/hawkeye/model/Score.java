package com.fnet.hawkeye.model;

import java.io.Serializable;

public class Score implements Serializable {

	private static final long serialVersionUID = -4546869862192420179L;

	String nstatus;
	Number numberofrecords;
	Number minimum;
	Number average;
	Number maximum;
	Number median;
	Number wptime;
	Number threshold; 
	Number jeopardy; 
	Number target;
	String color;

	public String getNstatus() {
		return nstatus;
	}
	public void setNstatus(String nstatus) {
		this.nstatus = nstatus;
	}
	

	public Number getNumberofrecords() {
		return numberofrecords;
	}
	public void setNumberofrecords(Number numberofrecords) {
		this.numberofrecords = numberofrecords;
	}
	public Number getMinimum() {
		return minimum;
	}
	public void setMinimum(Number minimum) {
		this.minimum = minimum;
	}
	public Number getAverage() {
		return average;
	}
	public void setAverage(Number average) {
		this.average = average;
	}
	public Number getMaximum() {
		return maximum;
	}
	public void setMaximum(Number maximum) {
		this.maximum = maximum;
	}
	public Number getMedian() {
		return median;
	}
	public void setMedian(Number median) {
		this.median = median;
	}
	public Number getWptime() {
		return wptime;
	}
	public void setWptime(Number wptime) {
		this.wptime = wptime;
	}
	public Number getThreshold() {
		return threshold;
	}
	public void setThreshold(Number threshold) {
		this.threshold = threshold;
	}
	public Number getJeopardy() {
		return jeopardy;
	}
	public void setJeopardy(Number jeopardy) {
		this.jeopardy = jeopardy;
	}
	public Number getTarget() {
		return target;
	}
	public void setTarget(Number target) {
		this.target = target;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
}
