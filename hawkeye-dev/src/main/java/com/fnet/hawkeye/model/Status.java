package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "status", uniqueConstraints = @UniqueConstraint(columnNames = "statusID"))
public class Status implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1990873667903332471L;

	private Long statusID;
	private Boolean showUI;
	private String statusName;
	private GroupType group;
	private Company company;
	private Product product;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
	public Long getStatusID() {
		return statusID;
	}
	public void setStatusID(Long statusId) {
		statusID = statusId;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String name) {
		this.statusName = name;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="groupTypeID",columnDefinition="INT")
	public GroupType getGroup() {
		return group;
	}
	public void setGroup(GroupType group) {
		this.group = group;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="companyID",columnDefinition="INT")
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
 
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="productID",columnDefinition="INT")
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	@Column(name = "showUI", columnDefinition = "BIT", length = 1)
	public Boolean getShowUI() {
		return showUI;
	}
	public void setShowUI(Boolean showUI) {
		this.showUI = showUI;
	}
}
