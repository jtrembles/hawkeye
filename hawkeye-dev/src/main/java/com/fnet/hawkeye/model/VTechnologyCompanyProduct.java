package com.fnet.hawkeye.model;

import java.io.Serializable;

public class VTechnologyCompanyProduct implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8698922426532206671L;

	private Number companyID;
	private Number productID;
	private Number technologyID;
	private String type;
	private String name;
	private Number bandwidth;


//	@Column(columnDefinition="INT")
	public Number getCompanyID() {
		return companyID;
	}
	public void setCompanyID(Number companyId) {
		companyID = companyId;
	}

//	@Column(columnDefinition="INT")
	public Number getProductID() {
		return productID;
	}
	public void setProductID(Number productId) {
		productID = productId;
	}
//	@Id
//	@Column(columnDefinition="INT")
	public Number getTechnologyID() {
		return technologyID;
	}
	public void setTechnologyID(Number technologyId) {
		technologyID = technologyId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTechnologyName() {
		return name;
	}
	public void setTechnologyName(String name) {
		this.name = name;
	}
	public Number getBandwidth() {
		return bandwidth;
	}
	public void setBandwidth(Number bandwidth) {
		this.bandwidth = bandwidth;
	}

//	@Transient
//	public Technology getTechnology() {
//		return utility.findTechnologyById(technologyID.longValue());
//	}
}
