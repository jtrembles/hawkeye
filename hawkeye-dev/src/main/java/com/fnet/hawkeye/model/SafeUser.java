package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "user", uniqueConstraints = @UniqueConstraint(columnNames = "userID"))
public class SafeUser implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3201655490732923439L;
	private Long userId;
	private String userEmail;
	private Company company;
//	private Long productID;
	private Product product; 
	private String firstname;
	private String lastname;
	private Set<GroupType> groupTypes = new HashSet<GroupType>();
	private Set<HierarchyValue> geoGroupHierarckyValues = new HashSet<HierarchyValue>();
	private GroupType userGroup;
	private Boolean active;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;
	private String apikey;
	private Boolean loggedIn;
	
	public SafeUser(){
		
	}
	
//	public User(User usr){
//		  userId = usr.userId;
//		 userEmail = usr.userEmail;
//		 company = usr.company;
////		  productID = usr;
//		 product = usr.product; 
//		 firstname = usr.firstname;
//		 lastname = usr.lastname;
//		 password = usr.password;
//		 groupTypes = usr.groupTypes;
////		 userGroup = usr.userGroup;
//		 active = usr.active;
//		 creationDate = usr.creationDate;
//		 lastModifiedDate = usr.lastModifiedDate;
//		 apikey = usr.apikey;
//		 loggedIn = usr.loggedIn;
//	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
	public Long getUserID() {
		return userId;
	}
	public void setUserID(Long userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstname;
	}
	public void setFirstName(String firstname) {
		this.firstname = firstname;
	}
	public String getLastName() {
		return lastname;
	}
	public void setLastName(String lastname) {
		this.lastname = lastname;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="groupTypeID",columnDefinition="INT")
	public GroupType getUserGroupType() {
		return userGroup;
	}
	public void setUserGroupType(GroupType group) {
		this.userGroup = group;
	}
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "userJobTypeGroup", catalog = "hawkeye", joinColumns = { 
			@JoinColumn(name = "userID", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "groupTypeID", 
					nullable = false, updatable = false) })
	public Set<GroupType> getGroupTypes() {
		return this.groupTypes;
	}
	
	public void setGroupTypes(Set<GroupType> groupTypes) {
		this.groupTypes = groupTypes;
	}
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "userGeoGroup", catalog = "hawkeye", joinColumns = { 
			@JoinColumn(name = "userID", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "hierarchyID", 
					nullable = false, updatable = false) })
	public Set<HierarchyValue> getGeoGroupHierarchyValues()	{
		return geoGroupHierarckyValues;
	}
 
	public void setGeoGroupHierarchyValues(Set<HierarchyValue> values) {
		this.geoGroupHierarckyValues = values;
	}
	
	@Column(name = "active", columnDefinition = "BIT", length = 1)
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="companyID",columnDefinition="INT")
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
 
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="productID",columnDefinition="INT")
	public Product getProduct() {
		return product;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}

	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String emailAddress) {
		this.userEmail = emailAddress;
	}
	
	@Transient
	public Boolean getLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(Boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	
	public String getApikey() {
		return apikey;
	}
	
	public void setApikey(String currentAPIKey) {
		this.apikey = currentAPIKey;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userEmail == null) ? 0 : userEmail.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof User))
			return false;
		User other = (User) obj;
		if (userEmail == null) {
			if (other.getUserEmail() != null)
				return false;
		} else if (!userEmail.equals(other.getUserEmail()))
			return false;
		if (userId == null) {
			if (other.getUserID() != null)
				return false;
		} else if (!userId.equals(other.getUserID()))
			return false;
		return true;
	}

	
	
}
