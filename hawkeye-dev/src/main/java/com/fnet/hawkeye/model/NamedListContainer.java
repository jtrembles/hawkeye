package com.fnet.hawkeye.model;

import java.util.List;

import com.fnet.hawkeye.util.IToJson;

public class NamedListContainer<T extends List<? extends IToJson>> implements IToJson {
	private T container;
	private String containerName;
	
	public NamedListContainer(String name)
	{
		setContainerName(name);
	}
	

	public T getContainer() {
		return container;
	}

	public void setContainer(T container) {
		this.container = container;
	}

	public String getContainerName() {
		return containerName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}
	
	@Override
	public String toJson() {
//		StringBuilder lBuilder = new StringBuilder("{");
		boolean lbHasElements = false;
		StringBuilder lBuilder = new StringBuilder("{\"");
		lBuilder.append(containerName).append("\":");
		lBuilder.append("[");
		if(container != null)
		{
			if(container.size() > 0)
				lbHasElements = true;
			for(IToJson item : container)
			{
				lBuilder.append(item.toJson());
				lBuilder.append(',');
			}
		}

		if(lbHasElements) //remove trailing separator
			lBuilder.setLength(lBuilder.length() - 1);
		lBuilder.append("]}");
		return lBuilder.toString();
	}
	
	@Override
	public String toString()
	{
		return toJson();
	}
}
