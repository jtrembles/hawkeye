package com.fnet.hawkeye.model;

import java.io.Serializable;

public class WorkloadSummary implements Serializable {

	private static final long serialVersionUID = -4545569862192420179L;

	long userid;
	String lastName;
	String firstName;
	String hierarchyName;
	Number numberRecords;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getHierarchyName() {
		return hierarchyName;
	}
	public void setHierarchyName(String hierarchyName) {
		this.hierarchyName = hierarchyName;
	}
	public Number getNumberRecords() {
		return numberRecords;
	}
	public void setNumberRecords(Number numberofrecords) {
		this.numberRecords = numberofrecords;
	}
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
}
