package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@Table(name = "note", uniqueConstraints = @UniqueConstraint(columnNames = "noteID"))
public class Note implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3183970602504124639L;
	
	private Long noteID;
	private WorkPackage workPackage;
	private Company company;
	private Product product;
	private User user;
	private String note;
	private Timestamp creationDate;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition="INT")
	public Long getNoteID() {
		return noteID;
	}
	public void setNoteID(Long noteID) {
		this.noteID = noteID;
	}

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name="productID",columnDefinition="INT")
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name="workPackageID",columnDefinition="INT")
	public WorkPackage getWorkPackage() {
		return workPackage;
	}
	public void setWorkPackage(WorkPackage workPackage) {
		this.workPackage = workPackage;
	}
	
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name="userID",columnDefinition="INT")
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	@Column(name = "note", columnDefinition="TEXT")
	public String getNote() {
		return note;
	}
	public void setNote(String notes) {
		this.note = notes;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name="companyID",columnDefinition="INT")
	public Company getCompany() {
		return company;
	}
	
	public void setCompany(Company company) {
		this.company = company;
	}

}