package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@Table(name = "hierarchySort", uniqueConstraints = @UniqueConstraint(columnNames = "hierarchySortID"))
public class HierarchySort implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3183970602504124639L;
	
	private Long hierarchySortID;
	private Long companyID;
	private Long productID;
	private Long parentHierarchyID;
	private Long childHierarchyID;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="hierarchySortID", unique=true, nullable=false, columnDefinition="INT")
	public Long getHierarchySortID() {
		return hierarchySortID;
	}
	public void setHierarchySortID(Long hierarchySortID) {
		this.hierarchySortID = hierarchySortID;
	}
	
	@Column(columnDefinition="INT")
	public Long getCompanyID() {
		return companyID;
	}
	public void setCompanyID(Long companyID) {
		this.companyID = companyID;
	}

	@Column(columnDefinition="INT")
	public Long getProductID() {
		return productID;
	}
	public void setProductID(Long productID) {
		this.productID = productID;
	}

	@Column(columnDefinition="INT")
	public Long getParentHierarchyID() {
		return parentHierarchyID;
	}
	public void setParentHierarchyID(Long parentHierarchyID) {
		this.parentHierarchyID = parentHierarchyID;
	}
	
	@Column(columnDefinition="INT")
	public Long getChildHierarchyID() {
		return childHierarchyID;
	}
	public void setChildHierarchyID(Long childHierarchyID) {
		this.childHierarchyID = childHierarchyID;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
}
