package com.fnet.hawkeye.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@Table(name = "addressType", uniqueConstraints = @UniqueConstraint(columnNames = "addressTypeID"))

public class AddressType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 619223945605049836L;
	@Id
	@Column(name="addressTypeID", unique=true, nullable=false, columnDefinition="INT")
	private Long addressTypeID;
	private String addressType;
	private Timestamp creationDate;
	private Timestamp lastModifiedDate;
	
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
}
