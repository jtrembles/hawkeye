package com.fnet.hawkeye.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import au.com.bytecode.opencsv.CSVReader;

import com.fnet.hawkeye.data.UserRepository;
import com.fnet.hawkeye.data.UtilityRepository;
import com.fnet.hawkeye.data.WorkpackageRepository;
import com.fnet.hawkeye.model.Company;
import com.fnet.hawkeye.model.GroupType;
import com.fnet.hawkeye.model.HierarchyValue;
import com.fnet.hawkeye.model.History;
import com.fnet.hawkeye.model.Priority;
import com.fnet.hawkeye.model.Product;
import com.fnet.hawkeye.model.RedCellEntry;
import com.fnet.hawkeye.model.RedCellEntryStatus;
import com.fnet.hawkeye.model.Site;
import com.fnet.hawkeye.model.Status;
import com.fnet.hawkeye.model.Technology;
import com.fnet.hawkeye.model.User;
import com.fnet.hawkeye.model.VTechnologyCompanyProduct;
import com.fnet.hawkeye.model.WorkPackage;
 
@Path("/wplist")
public class UploadWPListFileService {
 
	private final String UPLOADED_FILE_PATH = 	String.format("%s/deployments/Attachments.war/", System.getProperty("jboss.server.base.dir"));

	
//"/opt/rh/wpfiles/";  // Unix or Linux
//	private final String UPLOADED_FILE_PATH = "c:/tmp/";  // Windows
	



	private List<Technology> technologyList = null;
	private Priority criticalPriority = null;
	private GroupType pimGroupType = null;
	private GroupType extInterferenceGroupType = null;
	private GroupType ctsGroupType = null;
	private Status startStatus = null;
	private Status newStatus = null;
	private Status pendingStatus = null;
	private List<Priority> priorityList = null;
	
	//this is temporary!----------
	//TODO remove this code
	//private User shawn = null;
	//----------------------------
	
	@Inject
	private WorkpackageRepository repository;
	
	@Inject
	private UserRepository userRepository;
	
	@Inject
	private UtilityRepository utilityRepository;


	private void initReferenceData(Company company, Product product)//, User user)
	{
		technologyList = utilityRepository.findTechologiesByCompanyProduct(company, product);
		criticalPriority = utilityRepository.findPriorityByCompanyProductPriorityName(company, product, "CRITICAL");
		pimGroupType = utilityRepository.findJobTypeGroupByCompanyProductGroupName(company, product, "PIM");
		setExtInterferenceGroupType(utilityRepository.findJobTypeGroupByCompanyProductGroupName(company, product, "Ext Int"));
		ctsGroupType = utilityRepository.findJobTypeGroupByCompanyProductGroupName(company, product, "CTS");
		startStatus = utilityRepository.findStatusByCompanyProductStatusName(company, product, "Start");
		pendingStatus = utilityRepository.findStatusByCompanyProductStatusName(company, product, "Pending");
		newStatus = utilityRepository.findStatusByCompanyProductStatusName(company, product, "New");
		
		priorityList = utilityRepository.findPrioritiesByCompanyProductGroupName(company.getCompanyID(), product.getProductID());
		
		//temporary!!!!--------------
		//TODO remove this code
		//shawn = userRepository.findByEmail("rr2675@att.com");
		//---------------------------
	}
	

	
	
	@POST
	@Path("/upload")
	@Consumes("multipart/form-data")
	@Produces("text/plain")  //MediaType.APPLICATION_JSON)
	public Response uploadFile(MultipartFormDataInput input, @QueryParam("apikey") String apikey) {

		long userID = 0;
		Company comp = null;
		Product prod = null;
		User usr = null;
		StringBuilder sb = new StringBuilder();

		try
		{
			Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
	
			List<InputPart> wpKeyPart = uploadForm.get("txt_userID");
	
			List<RedCellEntryStatus> myStatuses = new ArrayList<RedCellEntryStatus>();
	
			try {
				List<InputPart> l = wpKeyPart;
				String topic1 = l.get(0).getBodyAsString();
//				userID = new Long(topic1);
	
				// Get Product and Company based on User
				usr = userRepository.findByApiKey(apikey);
	
				comp = usr.getCompany();
				// compID = comp.getCompanyID().toString();
				prod = usr.getProduct();
				// prodID = prod.getProductID().toString();
				initReferenceData(comp, prod);
	
			} catch (IOException e) {
				e.printStackTrace();
				return Response.status(Response.Status.PRECONDITION_FAILED).build();
			}
	
			List<InputPart> inputParts = uploadForm.get("file_upload");
			
			boolean isRFMEReport = false;
	
			for (InputPart inputPart : inputParts) {
				try {
					MultivaluedMap<String, String> header = inputPart.getHeaders();
	
					// convert the uploaded file to inputstream
					InputStream inputStream = inputPart.getBody(InputStream.class, null);
					InputStreamReader sReader = new InputStreamReader(inputStream);
					CSVReader reader = new CSVReader(sReader);
					List<String[]> myEntries = reader.readAll();
					Iterator<String[]> i = myEntries.iterator();
					String[] myFields = null;
					RedCellEntry cell = null;
					
					while (i.hasNext()) {
						try {
							cell = null;
							myFields = (String[]) i.next();
							
							if(myFields.length > 0 && myFields[0] != null && myFields[0].equalsIgnoreCase("Priority")) {
								continue; //header row so skip it
							} else if (myFields.length > 0 && myFields[0] != null && myFields[0].toLowerCase().contains("region")) {
								isRFMEReport = true;
								continue;
							}
							
							cell = new RedCellEntry(myFields, isRFMEReport);
							
							//validate redcell
							if(!validateCell(cell, comp, prod))
								continue; //we don't log retrun falses, only exceptions
							
							processRedCellEntry(cell, comp, prod, usr);
							
							// build success status
							myStatuses.add(new RedCellEntryStatus(cell.getCaseNumber(), "SUCCESS", "", printRedCellData(myFields)));
							
						} catch (Exception e) {
							// build failure status
							RedCellEntryStatus stat = new RedCellEntryStatus();
	
							if (cell != null)
								stat.setCaseNumber(cell.getCaseNumber() != null ? cell.getCaseNumber() : "UNDETERMINED");
	
							stat.setData(printRedCellData(myFields));
							stat.setReason(e.getMessage());
							stat.setStatus("FAIL");
							myStatuses.add(stat);
							continue;
						}
					}
	
					reader.close();
					inputStream.close();
	
				} catch (Exception e) {
					e.printStackTrace();
					throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
				}
			}
			
			sb.append("{\"statuses\":[");
			
			for (RedCellEntryStatus reds : myStatuses){
				sb.append(reds).append(",");
			}
			
			int lastComma = sb.lastIndexOf(",");
			sb.delete(lastComma,lastComma + 1);
			sb.append("]}");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(sb.toString()).build();
	}

	
	
 
	//validation logic for red cell records
	//skip header line (not an error to be logged just skipped)
	//check to make sure site exists
	private boolean validateCell(RedCellEntry cell, Company company, Product product) throws Exception
	{
		//check for header record
		//if(cell.getPriority() != null && cell.getPriority().equalsIgnoreCase("Priority"))
			//return false;
		
		//check the status, we throw out completed records
		if ( cell.getFinalStatus() != null ) {
			String stat = cell.getFinalStatus();
			
			if ( stat.toLowerCase().contains("closed") )
				return false;
		}
		
		if ( cell.getSite() == null || cell.getSite().isEmpty() ) {
			return false;
		}
		
		Site site = utilityRepository.findSiteByCompanyProductSiteName(company, product, cell.getSite());
		if(site == null)
			throw new Exception(String.format("No matching site found for site name:%s", cell.getSite()));
		
		return true;
	}
	
	private void processRedCellEntry(RedCellEntry cell, Company company, Product product, User user) throws Exception {

		//does a workpackage exist already for this caseNumber
		StringBuilder sb = new StringBuilder();
		WorkPackage wp = null;
		try {
			wp = repository.findWorkPackage(cell.getSite(), company.getCompanyID(), product.getProductID());						
		} catch (Exception e) {
			sb.append("Database Error looking for site: ").append(cell.getCaseNumber()).append(" - ").append(e.getMessage());
			throw new Exception( sb.toString());
		}
		
		if ( wp != null ) {
			sb.append("Work Package number ").append(wp.getWorkPackageID()).append(" already exists for site ").append(cell.getSite());
			throw new Exception( sb.toString());
		}
		
		//create new workpackage
		wp = CreateWorkPackage(cell, company, product, user);

		try
		{
			repository.insertWorkPackage(wp);
		}
		catch(Exception e)
		{
			throw new Exception(String.format("Failed to insert work package for case number - %s\r\n%s", cell.getCaseNumber(), e.getMessage()));
		}
		
		try
		{
			repository.addHistory(wp, user, startStatus.getStatusID(), newStatus.getStatusID(), "Status", "System Loaded");
			repository.addWorkpackageNote(wp, user, wp.getEngNotes());
			//repository.insertWorkPackageHistory(createHistory(wp, user));
		}
		catch(Exception e2)
		{
			e2.printStackTrace();
		}

		//TODO remove this code
		//temprorary!!!!!
		try
		{
			//move to pending
			wp.setStatus(newStatus);
			repository.updateWorkpackage(wp.getWorkPackageID(), pendingStatus.getStatusID(), user.getUserID(), user.getUserID(), null, null);
//			if(shawn != null)
//				repository.updateWorkpackage(wp.getWorkPackageID(), newStatus.getStatusID(), shawn.getUserID(), user.getUserID(), null);
		}
		catch(Exception e3)
		{
			e3.printStackTrace();
		}
		//-------------
	}
	
	public WorkPackage CreateWorkPackage( RedCellEntry cell, Company company, Product product, User user ) throws Exception
	{
		WorkPackage wp = new WorkPackage();
		
		Site site = utilityRepository.findSiteByCompanyProductSiteName(company, product, cell.getSite());
		if(site == null)
			throw new Exception(String.format("site:%s not found in database", cell.getSite()));
		
		HierarchyValue hierarchy = utilityRepository.findHierarchyByCompanyProductSite(company, product, site);
		if(hierarchy == null)
			throw new Exception(String.format("Hierarchy Value not found in database for site:%s", cell.getSite()));
		
		HierarchyValue parentHierarchyValue = utilityRepository.findHierarchyByHierarchyId(hierarchy.getParentHierarchyID());
		if(parentHierarchyValue == null)
			throw new Exception(String.format("No parent hierarch found for site:%s", cell.getSite()));
		
		wp.setProduct(product);
		wp.setCompany(company);


		//we must now look for the user in our DB that was in the CSV file
		//only for CTS not shawn so here we go again
		//if the CTS entry has no match or is null just use the current 
		//logged in user
		wp.setAssignedUser(user);
		
		if ( cell.getTroubleType().equals("CTS") && cell.getRFEngineer() != null) {
			
			String[] daEngineer = cell.getRFEngineer().split("\\s+");
			if ( daEngineer.length == 2 ) {
				User ctsUser = userRepository.findByFirstLastName(daEngineer[0], daEngineer[1], company.getCompanyID(), product.getProductID());
				if (ctsUser != null )
					wp.setAssignedUser(ctsUser);
			}
		}
	
		if(cell.getTroubleTicket() != null && cell.getTroubleTicket().length() > 0)
			wp.setCaseNumber(cell.getTroubleTicket());//cell.getCaseNumber());
		else
			wp.setCaseNumber(cell.getCaseNumber()); //old way
		
//		SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yyyy");
//		String date = sdf.format(new Date()); 
		
		if ( cell.getCity() != null )
			wp.setCity(cell.getCity());
		
		wp.setCreationDate(getToday());
		
		if ( cell.getEngZone() != null )
			wp.setEngineeringZone(cell.getEngZone());
		
		if (cell.getEngNotes() != null && cell.getEngNotes().length() > 0)
			wp.setEngNotes(cell.getEngNotes());
		
		if ( cell.getFrequency() != null )
			wp.setFrequency(cell.getFrequency().toString());
		
		wp.setHierarchyReportID(parentHierarchyValue.getHierarchyID());
		wp.setIehNotes(cell.getIEHNotes());
		
		//for new CTS load
		if ( cell.getTroubleType().equals("CTS")) {
			wp.setGroupType(ctsGroupType);
		}
		//shawn's original rules, this is getting hairy I'm sure we'll be here again
		//this is actually a GroupType now
		//if they have both external interference and PIM make it external interference
		else if(user.getGroupTypes().contains(getExtInterferenceGroupType()))
			wp.setGroupType(getExtInterferenceGroupType());
		else 
			wp.setGroupType(pimGroupType);
		
		//use the hierarchy from above if ops turf is null
		wp.setOpsTurf(parentHierarchyValue.getHierarchyName());
		
		wp.setPriority(criticalPriority);//default
		if(priorityList != null && cell.getPriority() != null)
		{
			for(Priority lPriority : priorityList)
			{
				if(lPriority.getPriorityName().equalsIgnoreCase(cell.getPriority()))
				{
					wp.setPriority(lPriority);
					break;
				}
			}
		}

		wp.setProjectManagerNotes(cell.getPMNotes());
		//wp.setResolutionId();
		wp.setRfEngineer(cell.getRFEngineer());
		wp.setRfEngineerNum(cell.getNumber());
		if(cell.getRSSILevel() != null)
			wp.setrSSI(cell.getRSSILevel().longValue());
		else
			wp.setrSSI(null);
		
		if ( cell.getSector() != null ) {
			wp.setSector(utilityRepository.findSectorBySectorName(cell.getSector()));
		}
		
		wp.setSite(site);
		
		//wp.setSiteScore(siteScore);
		wp.setStatus(startStatus);
		
		wp.setTechnology(site.getTechnology());
		
		//for(Technology tech : technologyList)
		//{
		//	if(tech.getTechnologyName().equalsIgnoreCase(cell.getTechnology()))
		//	{
				
		//		wp.setTechnology(utilityRepository.findTechnologyById(tech.getTechnologyID().longValue()));
		//		break;
		//	}
		//}
		
		if ( cell.getUplink() != null ) {
			wp.setUplinkChFreq(cell.getUplink());
		}
		
		return wp;		
	}
	
	private History createHistory(WorkPackage workPackage, User user)
	{
		History hist = new History();
		
		hist.setCreationDate(workPackage.getCreationDate());
		hist.setCurrentStatusCreationDate(workPackage.getCreationDate());
		hist.setCurrentStatusID(startStatus.getStatusID());
		hist.setNewStatusID(newStatus.getStatusID());
		hist.setNotes("System Loaded");
		hist.setUser(user);
		hist.setWorkPackage(workPackage);
		
		return hist;
	}
	
	private String printRedCellData(String[] lines) {
		
		StringBuilder sb = new StringBuilder();
		
		for ( String s : lines ) {
			sb.append(s).append('|');
		}
		
		return sb.toString();
	}

	/**
	 * header sample
	 * {
	 * 	Content-Type=[image/png], 
	 * 	Content-Disposition=[form-data; name="file"; filename="filename.extension"]
	 * }
	 **/
	//get uploaded filename, is there a easy way in RESTEasy?
	private String getFileName(MultivaluedMap<String, String> header) {
 
		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
 
		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {
 
				String[] name = filename.split("=");
 
				String finalFileName = name[1].trim().replaceAll("\"", "");
				return finalFileName;
			}
		}
		return "unknown";
	}
 
	private String getWorkPackageID(MultivaluedMap<String, String> header) {
		 
		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
 
		for (String key : contentDisposition) {
			if ((key.trim().startsWith("value"))) {
 
				String[] name = key.split("=");
 
				String finalKey = name[1].trim().replaceAll("\"", "");
				return finalKey;
			}
		}
		return "0";
	}
	//save to somewhere
	private void writeFile(byte[] content, String filename) throws IOException {
 
		File file = new File(filename);
 
		if (!file.exists()) {
			file.createNewFile();
		}
 
		FileOutputStream fop = new FileOutputStream(file);
 
		fop.write(content);
		fop.flush();
		fop.close();
 
	}

	private Timestamp getToday() {
		// TODO Auto-generated method stub
		return new java.sql.Timestamp(java.lang.System.currentTimeMillis());
		
	}


	public GroupType getExtInterferenceGroupType() {
		return extInterferenceGroupType;
	}


	public void setExtInterferenceGroupType(GroupType extInterferenceGroupType) {
		this.extInterferenceGroupType = extInterferenceGroupType;
	}

	
}
