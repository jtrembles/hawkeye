package com.fnet.hawkeye.rest;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jboss.resteasy.util.Base64;

import com.fnet.hawkeye.data.UserRepository;
import com.fnet.hawkeye.data.UtilityRepository;
import com.fnet.hawkeye.data.WorkpackageRepository;
import com.fnet.hawkeye.model.Attachment;
import com.fnet.hawkeye.model.AttachmentUploadType;
import com.fnet.hawkeye.model.NamedListContainer;
import com.fnet.hawkeye.model.User;
import com.fnet.hawkeye.model.WorkPackage;
import com.fnet.hawkeye.util.RandomString;

@Path("/file")
public class UploadFileService {

//	private final String UPLOADED_FILE_PATH = "/opt/rh/brms531/jboss-eap-6.0/standalone/deployments/Attachments.war/";
	private static final String UPLOADED_FILE_PATH = String.format("%s/deployments/Attachments.war/", System.getProperty("jboss.server.base.dir"));
	private static final String ATTACHMENT_LOCATION = "Attachments";

	private static final List<String> _imageExtensions;
	static {//initializer
		_imageExtensions = new ArrayList<String>();
		_imageExtensions.add("jpg");
		_imageExtensions.add("jpeg");
		_imageExtensions.add("png");
		_imageExtensions.add("bmp");
	}
	
	@Inject
	private WorkpackageRepository repository;
	
	@Inject
	private UtilityRepository utilityRepository;
	
	@Inject
	private UserRepository userRepository;
    
	@Inject
    private RandomString randomPasswordString;

	@GET
	@Path("/{workpackageID:[0-9][0-9]*}/{apikey}/attachments")
	@Produces(MediaType.TEXT_PLAIN)
	public Response lookupAttachmentsByWorkpackage(@PathParam("workpackageID") long workpackageid, @PathParam("apikey") String apiKey) {
		List<AttachmentUploadType> lAttachmentList = new ArrayList<AttachmentUploadType>();
		List<Attachment> attachments = null;
		NamedListContainer<List<AttachmentUploadType>> lNamedList = new NamedListContainer<List<AttachmentUploadType>>("files");
		
		try
		{
			User lUser = userRepository.findByApiKey(apiKey);
			if(lUser == null)
			{
				return Response.status(Response.Status.FORBIDDEN).build();
			}
			
			attachments = repository.findAttachmentsByWorkPackage(workpackageid);
	
			if (attachments == null) {
				throw new WebApplicationException(Response.Status.NOT_FOUND);
			}
			
			for(Attachment lAttachment : attachments)
			{
				AttachmentUploadType lType = new AttachmentUploadType();
				lType.setWorkPackageId(workpackageid);
				lType.setAttachmentName(lAttachment.getDocName());
				lType.setCaption(lAttachment.getDocLabel());
				lType.setApiKey(apiKey);
				lType.setDocUrl(String.format("/%s/%d/%s", lAttachment.getDocLocation(), workpackageid, lAttachment.getDocName()));
				java.nio.file.Path lPath = Paths.get(getThumbName(lType.getAttachmentName(), workpackageid));
				lType.setThumbName(lPath.getFileName().toString());
				lType.setThumbUrl(String.format("/%s/%s", lAttachment.getDocLocation(), lPath.toString().replace('\\', '/')));
				try {
					lType.setAttachmentSize((int)java.nio.file.Files.size(Paths.get(String.format("%s%d/%s", UPLOADED_FILE_PATH, workpackageid, lType.getAttachmentName()))));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				lAttachmentList.add(lType);
			}
	
			lNamedList.setContainer(lAttachmentList);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
		
		
		return Response.ok(lNamedList.toString()).build();
	}
	
	@GET
	@Path("/{workpackageID:[0-9][0-9]*}/{apikey}/attachments/{attachmentName}/delete")
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteAttachmentsByWorkpackage(@PathParam("workpackageID") long workpackageid, @PathParam("apikey") String apiKey, @PathParam("attachmentName") String name) {
		//added user id to the end as an after thought for history logging
		//it was easier than changing all  the code to store user id or trying to break it up in the jscript
		
		String lsJson = "";
		try
		{
			User lUser = userRepository.findByApiKey(apiKey);
			if(lUser == null)
			{
				return Response.status(Response.Status.FORBIDDEN).build();
			}
			
			List<Attachment> attachments = repository.findAttachmentsByWorkPackage(workpackageid);
	
			if (attachments == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
			
			String lsFileExtension;
			java.nio.file.Path lWorkPackagePath = Paths.get(String.format(String.format("%s%d", UPLOADED_FILE_PATH, workpackageid)));
			if(!java.nio.file.Files.isDirectory(lWorkPackagePath))
			{
				return Response.status(Response.Status.NOT_FOUND).build();
			}
	
			for(Attachment a : attachments)
			{
				if(a.getDocName().equalsIgnoreCase(name)){
					//delete file and thumbnail
					try {
						java.nio.file.Path lAttachmentPath = Paths.get(String.format("%s%d/%s", UPLOADED_FILE_PATH, workpackageid, a.getDocName()));
						
						lsFileExtension = "";
						int liExtensionIndex = 0;
						if(a.getDocName().lastIndexOf('.') != -1)
						{
							lsFileExtension = a.getDocName().substring(a.getDocName().lastIndexOf('.'));
							if(lsFileExtension.length() > 1)
								lsFileExtension = lsFileExtension.substring(1); //remove the "."
							else
								lsFileExtension = "";
						}
						
						if(lsFileExtension.length() > 0)
						{
							for(String lsImageExtension : _imageExtensions)
							{
								if(lsFileExtension.equalsIgnoreCase(lsImageExtension))
								{
									//delete the thumbnail
									java.nio.file.Path lThumbnailPath = Paths.get(String.format("%s%d/t_%s", UPLOADED_FILE_PATH, workpackageid, a.getDocName()));
									java.nio.file.Files.deleteIfExists(lThumbnailPath);
									break;
								}
							}
						}
						
						//delete the attachment itself
						java.nio.file.Files.deleteIfExists(lAttachmentPath);
						lsJson = String.format("{\"files\":[{\"%s\":true}]}", name);
	
						//delete the directory if it is empty
						java.nio.file.Files.deleteIfExists(lWorkPackagePath);
						
						//return for jquery upload:
	//					{"files": [
	//			           {
	//			             "picture1.jpg": true
	//			           },
	//			           {
	//			             "picture2.jpg": true
	//			           }
	//			         ]}
						
						
					}catch (DirectoryNotEmptyException de){
						//log nothing this ios ok
					}
					catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					try
					{
						repository.deleteWorkpackageAttachments(workpackageid, a.getDocName());
						createHistoryRecord(a.getWorkPackage(), /*lUser.getUserID()*/ 9, a, String.format("deleted %s", a.getDocName()));
						if(lsJson.length() == 0)
							lsJson = String.format("{\"files\":[{\"%s\":true}]}", name);
					}
					catch(Exception le)
					{
						le.printStackTrace();
						lsJson = String.format("{\"files\":[{\"%s\":false}]}", name);
					}
					
					break; //done
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(lsJson).build();
	}
	
	
	
	
	@POST
	@Path("/{apikey}/upload")
	@Consumes("multipart/form-data")
	@Produces(MediaType.TEXT_PLAIN) //(MediaType.APPLICATION_JSON)
	public Response uploadFile(MultipartFormDataInput input, @PathParam("apikey") String apiKey) {

		String documentLabel = "";
		//String fileName = "";
		long workpackageID = 0;
		List<AttachmentUploadType> lAttachmentList = new ArrayList<AttachmentUploadType>();

		try
		{
			User lUser = userRepository.findByApiKey(apiKey);
			if(lUser == null)
			{
				return Response.status(Response.Status.FORBIDDEN).build();
			}
			
			Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
	
			
			String lsDebug = String.format("jboss.server.base.dir=<%s>", System.getProperty("jboss.server.base.dir"));
	
			List<InputPart> wpKeyPart = uploadForm.get("h_wpid");
	
			try {
				List<InputPart> l = wpKeyPart;
				String topic1 = l.get(0).getBodyAsString();
				workpackageID = new Integer(topic1);
			} catch (IOException e) {
				e.printStackTrace();
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
	
			List<InputPart> captionParts = uploadForm.get("file_caption");
	
			try {
				for(InputPart inputPart : captionParts)
				{
					AttachmentUploadType lAttachment = new AttachmentUploadType();
					lAttachment.setWorkPackageId(workpackageID);
					//List<InputPart> l = labelPart;
					//documentLabel = l.get(0).getBodyAsString();
					lAttachment.setCaption(inputPart.getBodyAsString());
					if(lAttachment.getCaption().length() > 25)//column max length
						lAttachment.setCaption(lAttachment.getCaption().substring(0, 25));
					if(lAttachment.getCaption() != null  && lAttachment.getCaption().contains("\\"))
						lAttachment.setCaption(lAttachment.getCaption().replace("\\", "\\\\"));
					if(lAttachment.getCaption().length() > 25)//column max length
					{
						lAttachment.setCaption(lAttachment.getCaption().substring(0, 25));
						while(lAttachment.getCaption() != null && lAttachment.getCaption().endsWith("\\"))
						{
							if("\\".equals(lAttachment.getCaption()))
							{
								lAttachment.setCaption(null);
								continue;
							}
							lAttachment.setCaption(lAttachment.getCaption().substring(0, lAttachment.getCaption().length() - 2));
						}
					}					
					lAttachmentList.add(lAttachment);
				}
			} catch (IOException e) {
				e.printStackTrace();
				return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
			}
	
	
			List<InputPart> inputParts = uploadForm.get("files[]");
	
			for(int i = 0; i < inputParts.size(); i++) {
				boolean lbIsImage = false;
				try {
					InputPart inputPart = inputParts.get(i);
					MultivaluedMap<String, String> header = inputPart.getHeaders();
					String lsDocName = getFileName(header);
					java.nio.file.Path lWorkPackagePath = Paths.get(String.format("%s%d/", UPLOADED_FILE_PATH, workpackageID));
	
					//check to see if the directory already exists.  If not... create it
					if(!java.nio.file.Files.exists(lWorkPackagePath))
						java.nio.file.Files.createDirectory(lWorkPackagePath);
	
					AttachmentUploadType lCurrentAttachment = lAttachmentList.get(i);
					lCurrentAttachment.setAttachmentName(lsDocName);
	
					//convert the uploaded file to inputstream
					InputStream inputStream = inputPart.getBody(InputStream.class,null);
					byte [] bytes = IOUtils.toByteArray(inputStream);
					lCurrentAttachment.setAttachmentSize(bytes.length);
	
					String filename = "";
					
					try
					{
						lsDocName = cleanFileName(lsDocName);
						filename = String.format("%s/%s", lWorkPackagePath.toString(), lsDocName);
						lCurrentAttachment.setAttachmentName(lsDocName);
						lCurrentAttachment.setDocUrl(String.format("/%s/%d/%s", ATTACHMENT_LOCATION, workpackageID, lsDocName));
						lCurrentAttachment.setApiKey(apiKey); //for delete url
						writeFile(bytes, filename);
						
						//create thumbnail
						String lsFileExtension = "";
						int liExtensionIndex = 0;
						if(lCurrentAttachment.getAttachmentName().lastIndexOf('.') != -1)
						{
							lsFileExtension = lCurrentAttachment.getAttachmentName().substring(lCurrentAttachment.getAttachmentName().lastIndexOf('.'));
							if(lsFileExtension.length() > 1)
								lsFileExtension = lsFileExtension.substring(1); //remove the "."
							else
								lsFileExtension = "";
						}
						
						if (lsFileExtension.length() > 0){
							boolean lbCreatedThumbNail = false;
							for(String lsImageExtension : _imageExtensions)
							{
								if(lsFileExtension.equalsIgnoreCase(lsImageExtension))
								{
									BufferedImage img = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
									img.createGraphics().drawImage(ImageIO.read(new File(filename)).getScaledInstance(100, 100, Image.SCALE_SMOOTH),0,0,null);
				
				//					String thumbFilename = String.format("%st_%d_%s", UPLOADED_FILE_PATH, workpackageID, fileName);
									lCurrentAttachment.setThumbName(String.format("t_%s", lsDocName));
									lCurrentAttachment.setThumbUrl(String.format("/%s/%d/%s", ATTACHMENT_LOCATION, workpackageID, lCurrentAttachment.getThumbName()));
				
									ImageIO.write(img, "jpg", new File(String.format("%s%d/%s", UPLOADED_FILE_PATH, workpackageID, lCurrentAttachment.getThumbName())));
									
									lbCreatedThumbNail = true;
									lbIsImage = true;
									break;
								}
							}
	
							if(!lbCreatedThumbNail)
							{
								java.nio.file.Path lThumbPath = Paths.get(String.format("%scommon/%sthumbnail.jpg", UPLOADED_FILE_PATH, lsFileExtension));
								if(!java.nio.file.Files.exists(lThumbPath))
									lThumbPath = Paths.get(String.format("%scommon/genericthumbnail.jpg", UPLOADED_FILE_PATH));
								
								lCurrentAttachment.setThumbName(lThumbPath.getFileName().toString());
								lCurrentAttachment.setThumbUrl(String.format("/%s/common/%s", ATTACHMENT_LOCATION, lThumbPath.getFileName().toString()));
							}
						}
						else
						{
							lCurrentAttachment.setThumbName(Paths.get(getThumbName(lCurrentAttachment.getAttachmentName(), workpackageID)).getFileName().toString());
							lCurrentAttachment.setThumbUrl(String.format("/%s/%s", ATTACHMENT_LOCATION, lCurrentAttachment.getThumbName()));
						}
					}
					catch(Exception le)
					{
						lCurrentAttachment.setError(le.getMessage());
						continue;
					}
					
					// Insert record into database.
					if(!repository.attachmentExists(lAttachmentList.get(i).getWorkPackageId(), lCurrentAttachment.getAttachmentName()))
					{
						Attachment lDbAttachment = repository.insertAttachmentsByWorkPackage(lAttachmentList.get(i).getWorkPackageId(), ATTACHMENT_LOCATION, lCurrentAttachment.getAttachmentName(), lCurrentAttachment.getCaption(), lbIsImage);
						if(lDbAttachment != null)
							createHistoryRecord(lDbAttachment.getWorkPackage(), lUser.getUserID(), lDbAttachment, String.format("added %s", lCurrentAttachment.getAttachmentName()));
					}
					else
					{
						//update the caption
						Attachment lDbAttachment = repository.updateAttachment(lAttachmentList.get(i).getWorkPackageId(),lCurrentAttachment.getAttachmentName(), lCurrentAttachment.getCaption());
						if(lDbAttachment != null)
							createHistoryRecord(lDbAttachment.getWorkPackage(), lUser.getUserID(), lDbAttachment, String.format("updated %s", lCurrentAttachment.getAttachmentName()));
					}
					System.out.println("Done");
	
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		NamedListContainer<List<AttachmentUploadType>> lNamedList = new NamedListContainer<List<AttachmentUploadType>>("files");
		lNamedList.setContainer(lAttachmentList);

		return Response.ok(lNamedList.toString()).build();
	}

	@POST
	@Path("/{apikey}/android/upload")
	@Consumes("text/plain")
	@Produces(MediaType.TEXT_PLAIN) //(MediaType.APPLICATION_JSON)
	public Response uploadFileAndroid(String input, @PathParam("apikey") String apiKey)
	{
		String documentLabel = "";
		String fileName = "";
		long workpackageID = 0;
		List<AttachmentUploadType> lAttachmentList = new ArrayList<AttachmentUploadType>();

		try
		{
			User lUser = userRepository.findByApiKey(apiKey);
			if(lUser == null)
			{
				return Response.status(Response.Status.FORBIDDEN).build();
			}
			
//			if(!hasPermission(lUser, PermissionChecker.WPEDIT))
//				return Response.status(Response.Status.FORBIDDEN).build();
			
			if(!input.startsWith("&"))
			{
				input = "&" + input;
			}
			
			documentLabel = getValueFromPostString("pictureCaption", input);
			documentLabel = URLDecoder.decode(documentLabel, "ISO-8859-1");
			fileName = getValueFromPostString("filename", input);
			if(fileName == null || fileName.trim().isEmpty())
			{//this is temporary code -- should be removed
				fileName = randomPasswordString.nextString() + ".jpg";
			}
			
			String lsWpID = getValueFromPostString("wpid", input);
			if(lsWpID == null || lsWpID.isEmpty())
				return Response.status(Response.Status.BAD_REQUEST).entity("Invalid workpackage").build();
			
			try {
				workpackageID = Long.parseLong(lsWpID);
			} catch (Exception e1) {
				e1.printStackTrace();
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			
			
			String imageAsString = getValueFromPostString("imageSrc", input);
			if(imageAsString == null || imageAsString.isEmpty())
				return Response.status(Response.Status.BAD_REQUEST).entity("Invalid attachment").build();
			if(imageAsString.startsWith("data:"))
			{
				imageAsString = imageAsString.substring(imageAsString.indexOf(',') + 1);
			}
			byte[] imageBytes = Base64.decode(imageAsString);
			
			
			String lsDebug = String.format("jboss.server.base.dir=<%s>", System.getProperty("jboss.server.base.dir"));
	

			AttachmentUploadType lAttachment = new AttachmentUploadType();
			lAttachment.setWorkPackageId(workpackageID);
			lAttachment.setCaption(documentLabel);
			if(lAttachment.getCaption().length() > 25)//column max length
				lAttachment.setCaption(lAttachment.getCaption().substring(0, 25));
			if(lAttachment.getCaption() != null  && lAttachment.getCaption().contains("\\"))
				lAttachment.setCaption(lAttachment.getCaption().replace("\\", "\\\\"));
			if(lAttachment.getCaption().length() > 25)//column max length
			{
				lAttachment.setCaption(lAttachment.getCaption().substring(0, 25));
				while(lAttachment.getCaption() != null && lAttachment.getCaption().endsWith("\\"))
				{
					if("\\".equals(lAttachment.getCaption()))
					{
						lAttachment.setCaption(null);
						continue;
					}
					lAttachment.setCaption(lAttachment.getCaption().substring(0, lAttachment.getCaption().length() - 2));
				}
			}			
			lAttachmentList.add(lAttachment);
	
			java.nio.file.Path lWorkPackagePath = Paths.get(String.format("%s%d/", UPLOADED_FILE_PATH, workpackageID));
			//check to see if the directory already exists.  If not... create it
			if(!java.nio.file.Files.exists(lWorkPackagePath))
				java.nio.file.Files.createDirectory(lWorkPackagePath);

			lAttachment.setAttachmentName(fileName);
			lAttachment.setAttachmentSize(imageBytes.length);
			String lsDocName = fileName;
			boolean lbIsImage = false;

			try
			{
				String filename;
				lsDocName = cleanFileName(lsDocName);
				filename = String.format("%s/%s", lWorkPackagePath.toString(), lsDocName);
				lAttachment.setAttachmentName(lsDocName);
				lAttachment.setDocUrl(String.format("/%s/%d/%s", ATTACHMENT_LOCATION, workpackageID, lsDocName));
				lAttachment.setApiKey(apiKey); //for delete url
				writeFile(imageBytes, filename);
				
				//create thumbnail
				String lsFileExtension = "";
				int liExtensionIndex = 0;
				if(lAttachment.getAttachmentName().lastIndexOf('.') != -1)
				{
					lsFileExtension = lAttachment.getAttachmentName().substring(lAttachment.getAttachmentName().lastIndexOf('.'));
					if(lsFileExtension.length() > 1)
						lsFileExtension = lsFileExtension.substring(1); //remove the "."
					else
						lsFileExtension = "";
				}
				
				if (lsFileExtension.length() > 0){
					boolean lbCreatedThumbNail = false;
					for(String lsImageExtension : _imageExtensions)
					{
						if(lsFileExtension.equalsIgnoreCase(lsImageExtension))
						{
							BufferedImage img = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
							img.createGraphics().drawImage(ImageIO.read(new File(filename)).getScaledInstance(100, 100, Image.SCALE_SMOOTH),0,0,null);
		
		//					String thumbFilename = String.format("%st_%d_%s", UPLOADED_FILE_PATH, workpackageID, fileName);
							lAttachment.setThumbName(String.format("t_%s", lsDocName));
							lAttachment.setThumbUrl(String.format("/%s/%d/%s", ATTACHMENT_LOCATION, workpackageID, lAttachment.getThumbName()));
		
							ImageIO.write(img, "jpg", new File(String.format("%s%d/%s", UPLOADED_FILE_PATH, workpackageID, lAttachment.getThumbName())));
							
							lbCreatedThumbNail = true;
							lbIsImage = true;
							break;
						}
					}

					if(!lbCreatedThumbNail)
					{
						java.nio.file.Path lThumbPath = Paths.get(String.format("%scommon/%sthumbnail.jpg", UPLOADED_FILE_PATH, lsFileExtension));
						if(!java.nio.file.Files.exists(lThumbPath))
							lThumbPath = Paths.get(String.format("%scommon/genericthumbnail.jpg", UPLOADED_FILE_PATH));
						
						lAttachment.setThumbName(lThumbPath.getFileName().toString());
						lAttachment.setThumbUrl(String.format("/%s/common/%s", ATTACHMENT_LOCATION, lThumbPath.getFileName().toString()));
					}
				}
				else
				{
					lAttachment.setThumbName(Paths.get(getThumbName(lAttachment.getAttachmentName(), workpackageID)).getFileName().toString());
					lAttachment.setThumbUrl(String.format("/%s/%s", ATTACHMENT_LOCATION, lAttachment.getThumbName()));
				}
			}
			catch(Exception le)
			{
				lAttachment.setError(le.getMessage());
			}			

			// Insert record into database.
			if(!repository.attachmentExists(lAttachment.getWorkPackageId(), lAttachment.getAttachmentName()))
			{
				Attachment lDbAttachment = repository.insertAttachmentsByWorkPackage(lAttachment.getWorkPackageId(), ATTACHMENT_LOCATION, lAttachment.getAttachmentName(), lAttachment.getCaption(), lbIsImage);
				if(lDbAttachment != null)
					createHistoryRecord(lDbAttachment.getWorkPackage(), lUser.getUserID(), lDbAttachment, String.format("added %s", lAttachment.getAttachmentName()));
			}
			else
			{
				//update the caption
				Attachment lDbAttachment = repository.updateAttachment(lAttachment.getWorkPackageId(),lAttachment.getAttachmentName(), lAttachment.getCaption());
				if(lDbAttachment != null)
					createHistoryRecord(lDbAttachment.getWorkPackage(), lUser.getUserID(), lDbAttachment, String.format("updated %s", lAttachment.getAttachmentName()));
			}
//			System.out.println("Done");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		NamedListContainer<List<AttachmentUploadType>> lNamedList = new NamedListContainer<List<AttachmentUploadType>>("files");
		lNamedList.setContainer(lAttachmentList);

		return Response.ok(lNamedList.toString()).build();	
	}
	
	private String getValueFromPostString(String psParameterName, String psInput)
	{
		String lsResult = null;
		if(!psParameterName.startsWith("&"))
			psParameterName = String.format("&%s=", psParameterName);
		
		int liStartIndex = psInput.indexOf(psParameterName);
		int liEndIndex = 0;
		if(liStartIndex != -1)
		{
			liStartIndex += psParameterName.length();
			if(liStartIndex < psInput.length())
			{
				liEndIndex = psInput.indexOf('&', liStartIndex);
				if(liEndIndex != -1)
					lsResult = psInput.substring(liStartIndex, liEndIndex);
				else 
					lsResult = psInput.substring(liStartIndex);
			}
			else
				lsResult = ""; //parameter exists but no data
		}
		
		return lsResult;
	}
	
	
	
	//trims off backslashes from filenames because IE8 uses full path for filename
	private String cleanFileName(String psFileName) throws Exception
	{
		String lsReturnName = psFileName;
		lsReturnName = lsReturnName.replace('/', '-');
		int liIndex = lsReturnName.lastIndexOf('\\');
		
		try
		{
			do
			{
				if(liIndex != -1)
				{
					if(liIndex == (lsReturnName.length() -1))
						lsReturnName = lsReturnName.substring(0, liIndex);
					else
						lsReturnName = lsReturnName.substring(liIndex + 1);
				}
			}while((liIndex = lsReturnName.lastIndexOf('\\')) > -1);
		}
		catch(Exception e)
		{
			lsReturnName = "";
		}
		
		if(lsReturnName.length() == 0)
			throw new Exception("Invalid file name");

		return lsReturnName;
	}

	/**
	 * header sample
	 * {
	 * 	Content-Type=[image/png], 
	 * 	Content-Disposition=[form-data; name="file"; filename="filename.extension"]
	 * }
	 **/
	//get uploaded filename, is there a easy way in RESTEasy?
	private String getFileName(MultivaluedMap<String, String> header)  {

		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {

				String[] name = filename.split("=");

				String finalFileName = name[1].trim().replaceAll("\"", "");
				return finalFileName;
			}
		}
		return "unknown";
	}

	private String getID(MultivaluedMap<String, String> header) {

		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

		for (String key : contentDisposition) {
			if ((key.trim().startsWith("value"))) {

				String[] name = key.split("=");

				String finalKey = name[1].trim().replaceAll("\"", "");
				return finalKey;
			}
		}
		return "0";
	}
	//save to somewhere
	private void writeFile(byte[] content, String filename) throws IOException {

		File file = new File(filename);

		if (!file.exists()) {
			file.createNewFile();
		}

		FileOutputStream fop = new FileOutputStream(file);

		fop.write(content);
		fop.flush();
		fop.close();

	}
	
	private String getThumbName(String psDocName, long psWorkPackageId)
	{
		String lsFileExtension = "";
		String lsThumbName = "";
		int liExtensionIndex = 0;
		if(psDocName.lastIndexOf('.') != -1)
		{
			lsFileExtension = psDocName.substring(psDocName.lastIndexOf('.'));
			if(lsFileExtension.length() > 1)
				lsFileExtension = lsFileExtension.substring(1); //remove the "."
			else
				lsFileExtension = "";
		}
		
		if (lsFileExtension.length() > 0){
			boolean lbCreatedThumbNail = false;
			for(String lsImageExtension : _imageExtensions)
			{
				if(lsFileExtension.equalsIgnoreCase(lsImageExtension))
				{
					lsThumbName = String.format("%d/t_%s", psWorkPackageId, psDocName);
					lbCreatedThumbNail = true;
					break;
				}
			}
			
			if(!lbCreatedThumbNail)
			{
				java.nio.file.Path lThumbPath = Paths.get(String.format("%scommon/%sthumbnail.jpg", UPLOADED_FILE_PATH, lsFileExtension));
				if(!java.nio.file.Files.exists(lThumbPath))
					lThumbPath = Paths.get(String.format("%scommon/genericthumbnail.jpg", UPLOADED_FILE_PATH));
				
				lsThumbName = String.format("common/%s", lThumbPath.getFileName().toString());
			}
		}
		else
			lsThumbName = "common/genericthumbnail.jpg";
		
		return lsThumbName;
	}
	
	private void createHistoryRecord(WorkPackage workPackage, long pUserId, Attachment pAttachment, String psNote)
	{
		repository.addHistory(workPackage.getWorkPackageID(), pUserId, null, pAttachment.getAttachmentID(), "Attachment", psNote);
	}
}