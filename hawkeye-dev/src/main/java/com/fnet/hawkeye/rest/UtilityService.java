/**
 * JBoss, Home of Professional Open Source
 * Copyright 2012, Red Hat, Inc., and individual contributors
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fnet.hawkeye.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.jws.soap.SOAPBinding.Use;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fnet.hawkeye.data.UserRepository;
import com.fnet.hawkeye.data.UtilityRepository;
import com.fnet.hawkeye.model.Company;
import com.fnet.hawkeye.model.Dashboard;
import com.fnet.hawkeye.model.GroupType;
import com.fnet.hawkeye.model.Priority;
import com.fnet.hawkeye.model.Product;
import com.fnet.hawkeye.model.Resolution;
import com.fnet.hawkeye.model.Score;
import com.fnet.hawkeye.model.SimpleSite;
import com.fnet.hawkeye.model.Site;
import com.fnet.hawkeye.model.Status;
import com.fnet.hawkeye.model.Technology;
import com.fnet.hawkeye.model.User;
import com.fnet.hawkeye.model.VTechnologyCompanyProduct;

/**
 * JAX-RS Example
 * <p/>
 * This class produces a RESTful service to read/write the contents of the users table.
 */
@Path("/utility")
@RequestScoped
@Stateful
public class UtilityService {
	@Inject
	private Logger log;

	@Inject
	private Validator validator;

	@Inject
	private UtilityRepository repository;
	
	@Inject
	private UserRepository userRepository;

	@GET
	@Path("/statuses/{companyID:[0-9][0-9]*}/{productID:[0-9][0-9]*}/{statusID:[0-9][0-9]*}") 
	@Produces(MediaType.APPLICATION_JSON)
	public Response findByCompanyProductStatus(@PathParam("companyID") long companyID, @PathParam("productID")long productID, @PathParam("statusID") long statusID) {
		Status status = null;
		try
		{
			status = repository.findByCompanyProductId(companyID, productID, statusID);
			if (status == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
		return Response.ok(status).build();
	}

	@GET
	@Path("/statuses/{companyID:[0-9][0-9]*}/{productID:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupStatusByCompanyProduct(@PathParam("companyID") long companyid,@PathParam("productID") long productid) {
		List<Status> statuses = null;
		try
		{
			statuses = repository.findByCompanyProduct(companyid, productid);
			if (statuses == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
		
		return Response.ok(statuses).build();
	}

	@GET
	@Path("/technologies/{companyID:[0-9][0-9]*}/{productID:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupTechnologyByCompanyProduct(@PathParam("companyID") long companyid,@PathParam("productID") long productid) {
		List<Technology> technologies = null;
		try
		{
			technologies = repository.findTechologiesByCompanyProduct(companyid, productid);
			if (technologies == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(technologies).build();
	}

	@GET
	@Path("/technologies/{apikey}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupTechnologyByApiKey(@PathParam("apikey") String apikey) {
		List<Technology> technologies = null;
		try
		{
			User usr = userRepository.findByApiKey(apikey);
			technologies = repository.findTechologiesByCompanyProduct(usr.getCompany().getCompanyID(), usr.getProduct().getProductID());
			if (technologies == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(technologies).build();
	}

	
	@GET
	@Path("/score/{workpackageID:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON) 
	public Response lookupScoreByWorkPackage(@PathParam("workpackageID") Integer workpackageid) {
		List<Score> scores = null;
		try
		{
			scores = repository.findScoreByWorkPackageID(workpackageid);
			if (scores == null) {
				Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(scores).build();
	}

	@GET
	@Path("/sites/{companyID:[0-9][0-9]*}/{productID:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupSiteByCompanyProduct(@PathParam("companyID") long companyid,@PathParam("productID") long productid) {
		List<Site> sites = null;
		try
		{
			sites = repository.findSiteByCompanyProduct(companyid, productid);
			if (sites == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(sites).build();
	}

	@GET
	@Path("/sites/autocomplete/{companyID:[0-9][0-9]*}/{productID:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchSiteByCompanyProduct(@PathParam("companyID") long companyid,@PathParam("productID") long productid, @QueryParam("term") String searchTerm) {
		List<Site> sites = null;
		try {
			sites = repository.findSiteByCompanyProductSearchTerm(companyid, productid, searchTerm);
			
			if (sites == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		} 
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(sites).build();
	}

	@GET
	@Path("/sites/autocomplete/{userID:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchSiteByUser(@PathParam("userID") long userid, @QueryParam("term") String searchTerm) {
		List<SimpleSite> sites = null;
		try {
			User lUser = userRepository.findById(userid);
			sites = repository.findSiteByUserGeoGroupsSearchTerm(lUser.getGeoGroupHierarchyValues(), searchTerm);
			
			if (sites == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
			
		} 
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(sites).build();
	}

	public Date getTimestamp(String dateStr) throws WebApplicationException {
	    if (dateStr == null || dateStr.equals("")) {
	      return null;
	    }
	    final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	    try {
	      return dateFormat.parse(dateStr);
	    } catch (ParseException e) {
	      throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
	        .entity("Couldn't parse date string: " + e.getMessage())
	        .build());
	    }
	  }

	@GET
	@Path("/dashboard/{companyID:[0-9][0-9]*}/{productID:[0-9][0-9]*}") 
	@Produces(MediaType.APPLICATION_JSON)
	public Response findDashboardByCompanyProduct(@PathParam("companyID") long companyID, @PathParam("productID")long productID, @QueryParam("startDate")String startDateStr, @QueryParam("endDate")String endDateStr) {
		List<Dashboard> dashboardRows = null;
		
		try
		{
			Date startDate = getTimestamp(startDateStr);
			Date endDate = getTimestamp(endDateStr);
			dashboardRows = repository.findDashboardByCompanyProductDateRange(companyID, productID, startDate, endDate);
			if (dashboardRows == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
		
		return Response.ok(dashboardRows).build();
	}

	@GET
	@Path("/jobtypes/{companyID:[0-9][0-9]*}/{productID:[0-9][0-9]*}") 
	@Produces(MediaType.APPLICATION_JSON)
	public Response findJobTypeByCompanyIDProductID(@PathParam("companyID") long companyID, @PathParam("productID")long productID) {
		List<GroupType> groups = null;
		try
		{
			groups = repository.findJobTypeByCompanyProductGroupName(companyID, productID);
			if (groups == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(groups).build();
	}

	@GET
	@Path("/jobtypes/{apikey}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findJobTypeByApiKey(@PathParam("apikey") String apikey) {
		List<GroupType> groups = null;
		try
		{
			User usr = userRepository.findByApiKey(apikey);
			
			groups = repository.findJobTypeByCompanyProductGroupName(usr.getCompany().getCompanyID(), usr.getProduct().getProductID());
			if (groups == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(groups).build();
	}

	@GET
	@Path("/jobtypesByCompanyProduct/{apikey}") 
	@Produces(MediaType.APPLICATION_JSON)
	public Response findJobTypeByCompanyProduct(@PathParam("apikey") String apikey) {
		List<GroupType> groups = null;
		try
		{
			User usr = userRepository.findByApiKey(apikey);
			
			groups = repository.findJobTypeByCompanyProductGroupName(usr.getCompany().getCompanyID(), usr.getProduct().getProductID());
			if (groups == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(groups).build();
	}
	
	@GET
	@Path("/resolutions/{productID:[0-9][0-9]*}") 
	@Produces(MediaType.APPLICATION_JSON)
	public Response findResolutionsByProductID(@PathParam("productID")long productID) {
		List<Resolution> resolutions = null;
		try
		{
			resolutions = repository.findResolutionsByProduct(productID);
			if (resolutions == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(resolutions).build();
	}

	@GET
	@Path("/priorities/{companyID:[0-9][0-9]*}/{productID:[0-9][0-9]*}") 
	@Produces(MediaType.APPLICATION_JSON)
	public Response findPrioritiesByCompanyIDProductID(@PathParam("companyID") long companyID, @PathParam("productID")long productID) {
		List<Priority> priorities = null;
		try
		{
			priorities = repository.findPrioritiesByCompanyProductGroupName(companyID, productID);
			if (priorities == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(priorities).build();
	}

	@GET
	@Path("/priorities/{apikey}") 
	@Produces(MediaType.APPLICATION_JSON)
	public Response findPrioritiesByApiKey(@PathParam("apikey") String apikey) {
		List<Priority> priorities = null;
		try
		{
			User usr = userRepository.findByApiKey(apikey);
			priorities = repository.findPrioritiesByCompanyProductGroupName(usr.getCompany().getCompanyID(), usr.getProduct().getProductID());
			if (priorities == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(priorities).build();
	}

	/**
	 * Creates a JAX-RS "Bad Request" response including a map of all violation fields, and their message.
	 * This can then be used by clients to show violations.
	 *
	 * @param violations A set of violations that needs to be reported
	 * @return JAX-RS response containing all violations
	 */
	private Response.ResponseBuilder createViolationResponse(Set<ConstraintViolation<?>> violations) {
		log.fine("Validation completed. violations found: " + violations.size());

		Map<String, String> responseObj = new HashMap<String, String>();

		for (ConstraintViolation<?> violation : violations) {
			responseObj.put(violation.getPropertyPath().toString(), violation.getMessage());
		}

		return Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
	}

}
