/**
 * JBoss, Home of Professional Open Source
 * Copyright 2012, Red Hat, Inc., and individual contributors
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fnet.hawkeye.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fnet.hawkeye.data.UserRepository;
import com.fnet.hawkeye.data.WorkloadRepository;
import com.fnet.hawkeye.model.User;
import com.fnet.hawkeye.model.WorkloadCloseSummary;
import com.fnet.hawkeye.model.WorkloadDetail;
import com.fnet.hawkeye.model.WorkloadSummary;

/**
 * JAX-RS Example
 * <p/>
 * This class produces a RESTful service to read/write the contents of the users table.
 */
@Path("/workload")
@RequestScoped
@Stateful
public class WorkloadService {
	@Inject
	private Logger log;

	@Inject
	private Validator validator;

	@Inject
	private WorkloadRepository repository;

	@Inject
	private UserRepository userRepository;
	
	@GET
	@Path("/detail/{apiKey}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupWorkloadDetailByCompanyProduct(
			@PathParam("apiKey") String apiKey,
			@QueryParam("userID")	long userID,
			@QueryParam("groupTypeID") long groupTypeID,
			@QueryParam("marketName") String marketName,
			@QueryParam("jobTypeID") long jobTypeID)
			{
				
		List<WorkloadDetail> packages = null;
		try
		{
			User user = userRepository.findByApiKey(apiKey);
			packages = repository.findWorkloadDetailByCompanyProduct(user, userID, groupTypeID, marketName, jobTypeID);
			if (packages == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(packages).build();
	}

	@GET
	@Path("/summaryTech/{apiKey}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupTechWorkloadSummaryByCompanyProduct(
			@PathParam("apiKey") String apiKey,
			@QueryParam("userID")	long userID,
			@QueryParam("groupTypeID") long groupTypeID,
			@QueryParam("jobTypeID") long jobTypeID) {
				
		List<WorkloadSummary> packages = null;
		try
		{
			User user = userRepository.findByApiKey(apiKey);
			packages = repository.findTechWorkloadSummaryByCompanyProduct(user, userID, groupTypeID, jobTypeID);
			if (packages == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(packages).build();
	}

	@GET
	@Path("/summaryMkt/{apiKey}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupMktWorkloadSummaryByCompanyProduct(
			@PathParam("apiKey") String apiKey,
			@QueryParam("userID")	long userID,
			@QueryParam("groupTypeID") long groupTypeID,
			@QueryParam("jobTypeID") long jobTypeID) {
				
		List<WorkloadSummary> packages = null;
		try
		{
			User user = userRepository.findByApiKey(apiKey);
			packages = repository.findMktWorkloadSummaryByCompanyProduct(user, userID, groupTypeID, jobTypeID);
			if (packages == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(packages).build();
	}

	@GET
	@Path("/closeSummary/{apiKey}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupWorkloadCloseByCompanyProduct(
			@PathParam("apiKey") String apiKey,
			@QueryParam("jobTypeID") long jobTypeID) {
				
		List<WorkloadCloseSummary> packages = null;
		try
		{
			User user = userRepository.findByApiKey(apiKey);
			packages = repository.findWorkloadCloseSummaryByCompanyProduct(user, jobTypeID);//.getCompany().getCompanyID(), user.getProduct().getProductID(), jobTypeID);
			if (packages == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(packages).build();
	}

	/**
	 * Creates a JAX-RS "Bad Request" response including a map of all violation fields, and their message.
	 * This can then be used by clients to show violations.
	 *
	 * @param violations A set of violations that needs to be reported
	 * @return JAX-RS response containing all violations
	 */
	private Response.ResponseBuilder createViolationResponse(Set<ConstraintViolation<?>> violations) {
		log.fine("Validation completed. violations found: " + violations.size());

		Map<String, String> responseObj = new HashMap<String, String>();

		for (ConstraintViolation<?> violation : violations) {
			responseObj.put(violation.getPropertyPath().toString(), violation.getMessage());
		}

		return Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
	}
}
