/**
 * JBoss, Home of Professional Open Source
 * Copyright 2012, Red Hat, Inc., and individual contributors
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fnet.hawkeye.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fnet.hawkeye.data.UserRepository;
import com.fnet.hawkeye.data.UtilityRepository;
import com.fnet.hawkeye.data.WorkpackageRepository;
import com.fnet.hawkeye.model.Attachment;
import com.fnet.hawkeye.model.Company;
import com.fnet.hawkeye.model.GroupType;
import com.fnet.hawkeye.model.History;
import com.fnet.hawkeye.model.Note;
import com.fnet.hawkeye.model.Priority;
import com.fnet.hawkeye.model.Product;
import com.fnet.hawkeye.model.Score;
import com.fnet.hawkeye.model.SimpleHistory;
import com.fnet.hawkeye.model.Site;
import com.fnet.hawkeye.model.Status;
import com.fnet.hawkeye.model.Technology;
import com.fnet.hawkeye.model.User;
import com.fnet.hawkeye.model.VTechnologyCompanyProduct;
import com.fnet.hawkeye.model.WorkPackage;
import com.fnet.hawkeye.model.WorkPackagePlusScore;
import com.fnet.hawkeye.model.WorkPackageScore;

/**
 * JAX-RS Example
 * <p/>
 * This class produces a RESTful service to read/write the contents of the users table.
 */
@Path("/workpackages")
@RequestScoped
@Stateful
public class WorkpackageService {
	@Inject
	private Logger log;

	@Inject
	private Validator validator;

	@Inject
	private WorkpackageRepository repository;

	@Inject
	private UtilityRepository utilityRepository;

	@Inject
	private UserRepository userRepository;

	@GET
	@Path("/workpackage/{workpackageID:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupWorkpackageByID(@PathParam("workpackageID") long workpackageid) {
		WorkPackage workpackage = null;
		try
		{
			workpackage = repository.findWorkPackage(workpackageid);
			if (workpackage == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
				//throw new WebApplicationException(Response.Status.NOT_FOUND);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
		return Response.ok(workpackage).build();
	}

	@GET
	@Path("/workpackage/lastCompletionDate/{workpackageID:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLastWorkPackageCompletionDate(@PathParam("workpackageID") long workpackageid) {
		History date = null;
		try
		{
			date = repository.getLastWorkPackageCompletionDate(workpackageid);
			if (date == null || date.getCurrentStatusCreationDate() == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
		return Response.ok(date).build();//"{date:" + result + "}";
	}

	//getListLastWorkPackageCompletionDate

	@POST
	@Path("/workpackage/lastCompletionDates")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getListLastWorkPackageCompletionDate(String workpackageids) {
		List<SimpleHistory> hists = new ArrayList<SimpleHistory>();
		try
		{
			workpackageids = workpackageids.replace("[", "");
			workpackageids = workpackageids.replace("]", "");		

			hists = repository.getListLastWorkPackageCompletionDate(workpackageids);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
		return Response.ok(hists).build();//"{date:" + result + "}";
	}
	
	
	@GET
	@Path("/{workpackageID:[0-9][0-9]*}/attachments")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupAttachmentsByWorkpackage(@PathParam("workpackageID") long workpackageid) {
		List<Attachment> attachments = null;
		try
		{
			attachments = repository.findAttachmentsByWorkPackage(workpackageid);
			if (attachments == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
		return Response.ok(attachments).build();
	}

	@GET
	@Path("/{workpackageID:[0-9][0-9]*}/notes")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupNotesByWorkpackage(@PathParam("workpackageID") long workpackageid) {
		List<Note> notes = null;
		try
		{
			notes = repository.findNotesByWorkPackage(workpackageid);
			if (notes == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
		return Response.ok(notes).build();
	}
	@GET
//	@Path("/{companyID:[0-9][0-9]*}/{productID:[0-9][0-9]*}/{statusID:[0-9][0-9]*}")
	@Path("/{apikey}/{statusID:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupWorkPackageByUserStatus(@PathParam("apikey") String apiKey,@PathParam("statusID") long id) {
		if(apiKey == null || apiKey.trim().length() == 0)
			return Response.status(Response.Status.FORBIDDEN).build();
		
		List<WorkPackage> workpackages = null;
		try
		{
			User lUser = userRepository.findByApiKey(apiKey);
			if(lUser == null)
				return Response.status(Response.Status.FORBIDDEN).build();
			
	//		List<WorkPackage> workpackages = repository.findByCompanyProductStatus(companyid, productid, id);
			workpackages = repository.findByUserStatus(lUser, id);
	//		if (workpackages == null || workpackages.size() == 0) {
	//			throw new WebApplicationException(Response.Status.NOT_FOUND);
	//		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
		
		return Response.ok(workpackages).build();
	}

	@GET
	@Path("/{companyID:[0-9][0-9]*}/{productID:[0-9][0-9]*}/{assignedUserID:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupWorkPackageByCompanyProductUser(@PathParam("companyID") long companyid,@PathParam("productID") long productid,@PathParam("assignedUserID") long assigneduserid) {
		List<WorkPackage> workpackages = null;
		try
		{
			workpackages = repository.findByCompanyProductUser(companyid, productid, assigneduserid);
			if (workpackages == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
		
		return Response.ok(workpackages).build();
	}

//	@GET
//	@Path("/{companyID:[0-9][0-9]*}/{productID:[0-9][0-9]*}")
//	@Produces(MediaType.APPLICATION_JSON)
//	public List<WorkPackage> lookupPackageByCompanyProduct(@PathParam("companyID") long companyid,@PathParam("productID") long productid) {
//		List<WorkPackage> packages = repository.findByCompanyProduct(companyid, productid);
//		if (packages == null) {
//			throw new WebApplicationException(Response.Status.NOT_FOUND);
//		}
//		return packages;
//	}
	@GET
	@Path("/{apikey}/user")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupPackageByUser(@PathParam("apikey") String apiKey) {
		if(apiKey == null || apiKey.trim().length() == 0)
			return Response.status(Response.Status.FORBIDDEN).build();
		
		List<WorkPackage> packages = null;
		try
		{
			User lUser = userRepository.findByApiKey(apiKey);
			if(lUser == null)
				return Response.status(Response.Status.FORBIDDEN).build();
		
			packages = repository.findForUser(lUser);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
		
		return Response.ok(packages).build();
	}

	@GET
	@Path("/{apikey}/jobType")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupPackageByUserJobType(@PathParam("apikey") String apiKey, @QueryParam("statusType") String statusType) {
		if(apiKey == null || apiKey.trim().length() == 0)
			return Response.status(Response.Status.FORBIDDEN).build();
		
		List<WorkPackage> packages = null;
		
		try
		{
			User lUser = userRepository.findByApiKey(apiKey);
			if(lUser == null)
				return Response.status(Response.Status.FORBIDDEN).build();
			
			if("All".equalsIgnoreCase(statusType))
				packages = repository.findByGroupTypesBasedOnUser(lUser);
			else
			{
				GroupType lStatusType = utilityRepository.findStatusTypeByCompanyProductGroupName(lUser.getCompany().getCompanyID(), lUser.getProduct().getProductID(), statusType);
				packages = repository.findByWorkPackgeBasedOnUserStatusType(lUser, lStatusType);
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(packages).build();
	}
//	@GET
//	@Path("/{companyID:[0-9][0-9]*}/{productID:[0-9][0-9]*}/{userID:[0-9][0-9]*}/jobType")
//	@Produces(MediaType.APPLICATION_JSON)
//	public List<WorkPackage> lookupPackageByCompanyProductUser(@PathParam("companyID") long companyid,@PathParam("productID") long productid, @PathParam("userID") long userid) {
//		List<WorkPackage> packages = repository.findByCompanyProductGroupTypesBasedOnUserID(companyid, productid, userid);
//		if (packages == null) {
//			throw new WebApplicationException(Response.Status.NOT_FOUND);
//		}
//		return packages;
//	}

	@POST
	@Path("/update/{apikey}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateWorkpackage(
			@FormParam("workpackageID") long workpackageid,
			@FormParam("statusID") long statusid,
			@FormParam("techID") long technicianid,
			@FormParam("jobTypeID") long jobtypeid,
			@FormParam("resolutionID") long resolutionid,
			@PathParam("apikey") String apiKey
//			@FormParam("priorityID") long priorityid
			) 
	{

		Response.ResponseBuilder builder = null;

		try {
			User lUser = userRepository.findByApiKey(apiKey);
			if(lUser == null)
				return Response.status(Response.Status.FORBIDDEN).build();
			
			repository.updateWorkpackage(workpackageid, statusid, technicianid, lUser.getUserID(), jobtypeid, resolutionid);//, priorityid);
			builder = Response.ok();
		} catch (ConstraintViolationException ce) {
			//Handle bean validation issues
			builder = createViolationResponse(ce.getConstraintViolations());
		} catch (ValidationException e) {
			//Handle the unique constrain violation
			Map<String, String> responseObj = new HashMap<String, String>();
			responseObj.put("email", "Email taken");
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<String, String>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}
		return builder.build();

	}

	@POST
	@Path("/insert")
	//	@Consumes("multipart/form-data")
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertWorkpackage(
			@FormParam("caseNumber") String caseNumber,
			@FormParam("ctn") String customerTicketNumber, // CTN == CustomerTrackingNumber
			@FormParam("txt_siteID_h") String siteId,
			@FormParam("rfEngineer") String rfEngineer,
			@FormParam("sectorName") String sectorName,
			@FormParam("rfEngineerNum") String rfEngineerNum,
			@FormParam("notes") String notes,
			@FormParam("cmb_technology") long technology,
			@FormParam("frequency") String frequency,
			@FormParam("engineeringZone") String engineeringZone,
			@FormParam("uplinkChFreq") String uplinkChFreq,
			@FormParam("troubleTicketNumber") String troubleTicketNumber,
			@FormParam("rSSI") String rSSI,
			@FormParam("cmb_jobType") long jobtypeid,
			@FormParam("cmb_priority") long priorityid,
			@FormParam("h_txt_userID") long userid,
			@FormParam("h_txt_apiKey") String apikey
			) {

		Response.ResponseBuilder builder = null;

		User user = null; 
		Company company = null;
		Product product = null;

		try {

			if(apikey != null && apikey.length() > 0)
				user = userRepository.findByApiKey(apikey);
			else
				user = userRepository.findById(userid);
			
			if(user == null)
				return Response.status(Response.Status.FORBIDDEN).build();

			company = user.getCompany();
			product = user.getProduct();

			Technology tech = utilityRepository.findTechnologyById(technology);
			Priority priority = utilityRepository.findPriorityById(priorityid);
			GroupType groupType = utilityRepository.findJobTypeById(jobtypeid);
			Status startStatus = utilityRepository.findStatusByCompanyProductStatusName(company, product, "Start");
			Status newStatus = utilityRepository.findStatusByCompanyProductStatusName(company, product, "New");
			Status pendingStatus = utilityRepository.findStatusByCompanyProductStatusName(company, product, "Pending");

			Site site = utilityRepository.findSiteById(new Long(siteId));
			if(site == null)
				throw new Exception(String.format("Site is not found in database"));

			//check for existing work package
			List<WorkPackage> lActivePAckageList = repository.findActivePackageByCompanyProductSite(company.getCompanyID(), product.getProductID(), site.getSiteID());
			if(lActivePAckageList.size() > 0)
				throw new Exception(String.format("An open work package exists for site:%s", site.getSiteName()));

			WorkPackage wp = repository.insertWorkpackage(
					//caseNumber,
					customerTicketNumber,
					siteId,
					rfEngineer,
					sectorName,
					rfEngineerNum,
					tech,
					frequency,
					engineeringZone,
					uplinkChFreq,
					troubleTicketNumber,
					rSSI,
					user, notes, company, product, newStatus, groupType, priority
					);

				repository.insertWorkPackageHistory(createFirstHistory(wp, user));
			//			WorkPackage wp = repository.findWorkPackage(site.getSiteName(), company.getCompanyID(), product.getProductID());
				
				//move to pending 
				//first set the status to new since that's what the history says from createFirstHistory
				wp.setStatus(newStatus);
				//history trail will get created by update
				repository.updateWorkpackage(wp.getWorkPackageID(), pendingStatus.getStatusID(), user.getUserID(), user.getUserID(), wp.getGroupType().getGroupTypeID(), null);
			
			builder = Response.ok(wp);
		} catch (ConstraintViolationException ce) {
			//Handle bean validation issues
			builder = createViolationResponse(ce.getConstraintViolations());
		} catch (ValidationException e) {
			//Handle the unique constrain violation
			Map<String, String> responseObj = new HashMap<String, String>();
			responseObj.put("email", "Email taken");
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<String, String>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}
		return builder.build();

	}


	private History createFirstHistory(WorkPackage workPackage, User user)
	{
		History hist = new History();
		
		Status startStatus = utilityRepository.findStatusByCompanyProductStatusName(user.getCompany(), user.getProduct(), "Start");
		Status newStatus = utilityRepository.findStatusByCompanyProductStatusName(user.getCompany(), user.getProduct(), "New");
		GroupType historyType = utilityRepository.findHistoryTypeByCompanyProductGroupName(user.getCompany(), user.getProduct(), "Status");

		hist.setCreationDate(workPackage.getCreationDate());
		hist.setCurrentStatusCreationDate(workPackage.getCreationDate());
		hist.setCurrentStatusID(startStatus.getStatusID());
		hist.setNewStatusID(newStatus.getStatusID());
		hist.setNotes("System Loaded");
		hist.setUser(user);
		hist.setWorkPackage(workPackage);
		hist.setHistoryType(historyType);
		
		return hist;
	}

	@GET
	@Path("/updateNotes")
	@Consumes(MediaType.APPLICATION_JSON)
	//	@Produces(MediaType.APPLICATION_JSON)
	public Response updateWorkpackageNotes(@QueryParam("workpackageID") long workpackageid,
			@QueryParam("userID") Long userid,
			@QueryParam("notes") String notes
			) {

		Response.ResponseBuilder builder = null;

		try {
			repository.updateWorkpackageNotes(workpackageid, userid, notes);
			builder = Response.status(Response.Status.OK);
		} catch (ConstraintViolationException ce) {
			//Handle bean validation issues
			builder = createViolationResponse(ce.getConstraintViolations());
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<String, String>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}
		return builder.entity("Notes updated").build();
	}

	@GET
	@Path("/addNote")
	@Consumes(MediaType.APPLICATION_JSON)
	//	@Produces(MediaType.APPLICATION_JSON)
	public Response updateWorkpackageNote(@QueryParam("workpackageID") long workpackageid,
			@QueryParam("apikey") String apikey,
			@QueryParam("note") String note
			) {

		Response.ResponseBuilder builder = null;

		try {
			repository.addWorkpackageNote(workpackageid, apikey, note);
			builder = Response.status(Response.Status.OK);
		} catch (ConstraintViolationException ce) {
			//Handle bean validation issues
			builder = createViolationResponse(ce.getConstraintViolations());
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<String, String>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}
		return builder.entity("Notes updated").build();
	}

	@GET
	@Path("/{workpackageID:[0-9][0-9]*}/deleteAttachment")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteWorkpackageAttachment(@PathParam("workpackageID") long workpackageid,
			@QueryParam("attachmentName") String attachmentName) {

		Response.ResponseBuilder builder = null;

		try {
			repository.deleteWorkpackageAttachments(workpackageid, attachmentName);
			builder = Response.status(Response.Status.OK);
		} catch (ConstraintViolationException ce) {
			//Handle bean validation issues
			builder = createViolationResponse(ce.getConstraintViolations());
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<String, String>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}
		return builder.build();
	}

	//	@GET
	//	@Path("/{id:[0-9][0-9]*}")
	//	@Produces(MediaType.APPLICATION_JSON)
	//	public WorkPackage findById(@PathParam("id") long id) {
	//		WorkPackage newpackage = repository.findById(id);
	//		if (newpackage == null) {
	//			throw new WebApplicationException(Response.Status.NOT_FOUND);
	//		}
	//		return newpackage;
	//	}


	/**
	 * Creates a JAX-RS "Bad Request" response including a map of all violation fields, and their message.
	 * This can then be used by clients to show violations.
	 *
	 * @param violations A set of violations that needs to be reported
	 * @return JAX-RS response containing all violations
	 */
	private Response.ResponseBuilder createViolationResponse(Set<ConstraintViolation<?>> violations) {
		log.fine("Validation completed. violations found: " + violations.size());

		Map<String, String> responseObj = new HashMap<String, String>();

		for (ConstraintViolation<?> violation : violations) {
			responseObj.put(violation.getPropertyPath().toString(), violation.getMessage());
		}

		return Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
	}

	@GET
	@Path("/{workpackageID:[0-9][0-9]*}/histories")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findWorkPackageHistory(@PathParam("workpackageID") long workpackageid) {
		List<History> histories = null;
		
		try
		{
			histories = repository.findWorkPackageHistory(workpackageid);
			if (histories == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(histories).build();
	}

	@GET
	@Path("/score/{workpackageID:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON) 
	public Response lookupScoreByWorkPackage(@PathParam("workpackageID") Integer workpackageid) {
		List<WorkPackageScore> scores = null;
		try
		{
			scores = repository.findScoreByWorkPackageID(workpackageid);
			if (scores == null) {
				Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(scores).build();
	}

//	@GET
//	@Path("/redList/{apikey}")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response findBeyondThresholdWorkPackages(@PathParam("apikey") String apikey) {
//		List<WorkPackagePlusScore> reds = null;	
//		try
//		{
//			reds = repository.findWorkPackageHotList(apikey);
//			if (reds == null) {
//				return Response.status(Response.Status.NOT_FOUND).build();
//			}
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
//		}
//
//		return Response.ok(reds).build();
//	}
}
