/**
 * JBoss, Home of Professional Open Source
 * Copyright 2012, Red Hat, Inc., and individual contributors
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fnet.hawkeye.rest;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fnet.hawkeye.data.UserRepository;
import com.fnet.hawkeye.model.GroupType;
import com.fnet.hawkeye.model.SafeUser;
import com.fnet.hawkeye.model.User;

/**
 * JAX-RS Example
 * <p/>
 * This class produces a RESTful service to read/write the contents of the users table.
 */
@Path("/users")
//@RequestScoped
@Stateless
public class UserService {
	@Inject
	private Logger log;

	@Inject
	private Validator validator;

	@Inject
	private UserRepository repository;


//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	public List<User> listAllUsers() {
//		return repository.findAllOrderedByName();
//	}
	
	@GET
	@Path("/validateApiKey/{apiKey}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response validateApiKey(@PathParam("apiKey") String apiKey) {

		Response.ResponseBuilder builder = Response.status(Response.Status.FORBIDDEN);
		try
		{
			if("unavailable".equalsIgnoreCase(apiKey))
				return Response.status(Response.Status.FORBIDDEN).build();
			
			User usr = repository.findByApiKey(apiKey);
			
			if ( usr != null )
				builder = Response.ok();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return builder.build();
	}

	@GET
	@Path("/user/{apiKey}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupMemberByApiKey(@PathParam("apiKey") String apiKey) {

		Response.ResponseBuilder builder = Response.status(Response.Status.FORBIDDEN);
		try
		{
			SafeUser usr = repository.findSafeUserByApiKey(apiKey);
			
			if ( usr != null )
				builder = Response.ok().entity(usr);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return builder.build();
	}

	@GET
	@Path("/{companyID:[0-9][0-9]*}/{productID:[0-9][0-9]*}/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupMemberByCompanyProductUser(@PathParam("companyID") long companyid,@PathParam("productID") long productid,@PathParam("id") long id) {
		User member = null;
		try
		{
			member = repository.findByCompanyProductUser(companyid, productid,id);
			if (member == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(member).build();
	}

	@GET
	@Path("/{companyID:[0-9][0-9]*}/{productID:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupMemberByCompanyProduct(@PathParam("companyID") long companyid,@PathParam("productID") long productid) {
		List<SafeUser> members = null;
		try
		{
			members = repository.findByCompanyProduct(companyid, productid);
			if (members == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(members).build();
	}

	@GET
	@Path("/admin/all/{apikey}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupMembersByApiKey(@PathParam("apikey") String apikey) {
		List<SafeUser> members = null;
		try
		{
			User lUser = repository.findByApiKey(apikey);
			
			if(lUser == null)
				return Response.status(Response.Status.FORBIDDEN).build();
			
			members = repository.findByCompanyProduct(lUser.getCompany().getCompanyID(), lUser.getProduct().getProductID());
			if (members == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(members).build();
	}
	
	@GET
	@Path("/all/{apikey}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupMembersInAreaByApiKey(@PathParam("apikey") String apikey) {
		List<SafeUser> members = null;
		try
		{
			User lUser = repository.findByApiKey(apikey);
			
			if(lUser == null)
				return Response.status(Response.Status.FORBIDDEN).build();
			
			members = repository.findByCompanyProductUserGeo(lUser);
			if (members == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		return Response.ok(members).build();
	}

	@GET
	@Path("/logout/{companyID:[0-9][0-9]*}/{productID:[0-9][0-9]*}/{userID:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response logout(@PathParam("companyID") long companyid,@PathParam("productID") long productid, @PathParam("userID") long userid) {

		Response.ResponseBuilder builder = null;

		try
		{
			repository.logout(userid);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		builder = Response.ok();
		return builder.build();
	}

	@GET
	@Path("/logout/{apiKey}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response logout(@PathParam("apiKey") String apikey) {

		Response.ResponseBuilder builder = null;

		try
		{
			User usr = repository.findByApiKey(apikey);
			if(usr == null)
				return Response.status(Response.Status.NOT_FOUND).build();
			
			repository.logout(usr.getUserID());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

		builder = Response.ok();
		return builder.build();
	}

	/**
	 * Creates a new member from the values provided.  Performs validation, and will return a JAX-RS response with either
	 * 200 ok, or with a map of fields, and related errors.
	 */
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(User member) {
		Response.ResponseBuilder builder = null;
		try {
			member = repository.findByEmailAndPassword(member.getUserEmail(), member.getPassword());
			
			if(member == null)
				return Response.status(Response.Status.NOT_FOUND).build();
			
			member.setApikey(repository.generateAPIKey());
		
			repository.updateUser(member);
			
			builder = Response.ok(member);

		} catch (ConstraintViolationException ce) {
			//Handle bean validation issues
			builder = createViolationResponse(ce.getConstraintViolations());
		} catch (ValidationException e) {
			//Handle the unique constrain violation
			Map<String, String> responseObj = new HashMap<String, String>();
			responseObj.put("email", "Email taken");
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			Map<String, String> responseObj = new HashMap<String, String>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<String, String>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}
		
		return builder.build();
		
	}


	/**
	 * <p>Validates the given Member variable and throws validation exceptions based on the type of error.
	 * If the error is standard bean validation errors then it will throw a ConstraintValidationException
	 * with the set of the constraints violated.</p>
	 * <p>If the error is caused because an existing member with the same email is registered it throws a regular
	 * validation exception so that it can be interpreted separately.</p>
	 *
	 * @param member Member to be validated
	 * @throws ConstraintViolationException If Bean Validation errors exist
	 * @throws ValidationException          If member with the same email already exists
	 */
	private void validateMember(User member) throws ConstraintViolationException, ValidationException {
		//Create a bean validator and check for issues.
		Set<ConstraintViolation<User>> violations = validator.validate(member);

		if (!violations.isEmpty()) {
			throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(violations));
		}

		//Check the uniqueness of the email address
		if (emailAlreadyExists(member.getUserEmail())) {
			throw new ValidationException("Unique Email Violation");
		}
	}

	/**
	 * Creates a JAX-RS "Bad Request" response including a map of all violation fields, and their message.
	 * This can then be used by clients to show violations.
	 *
	 * @param violations A set of violations that needs to be reported
	 * @return JAX-RS response containing all violations
	 */
	private Response.ResponseBuilder createViolationResponse(Set<ConstraintViolation<?>> violations) {
		log.fine("Validation completed. violations found: " + violations.size());

		Map<String, String> responseObj = new HashMap<String, String>();

		for (ConstraintViolation<?> violation : violations) {
			responseObj.put(violation.getPropertyPath().toString(), violation.getMessage());
		}

		return Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
	}

	/**
	 * Checks if a member with the same email address is already registered.  This is the only way to
	 * easily capture the "@UniqueConstraint(columnNames = "email")" constraint from the Member class.
	 *
	 * @param email The email to check
	 * @return True if the email already exists, and false otherwise
	 */
	public boolean emailAlreadyExists(String email) {
		User member = null;
		try {
			member = repository.findByEmail(email);
		} catch (NoResultException e) {
			// ignore
		}
		return member != null;
	}

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupMemberByCompanyProductUser(@PathParam("id") long id) {
		User member = null;
		
		try
		{
			member = repository.findById(id);
			if (member == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
		return Response.ok(member).build();
	}

	@GET
	@Path("/{companyID:[0-9][0-9]*}/{productID:[0-9][0-9]*}/{id:[0-9][0-9]*}/jobTypes")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lookupJobTypeByCompanyProductUser(@PathParam("companyID") long companyid,@PathParam("productID") long productid,@PathParam("id") long id) {
		List<GroupType> groups = null;
		try
		{
			groups = repository.findJobTypeByCompanyProductUser(companyid, productid,id);
			if (groups == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
		return Response.ok(groups).build();
	}
}
