/**
 * JBoss, Home of Professional Open Source
 * Copyright 2012, Red Hat, Inc., and individual contributors
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fnet.hawkeye.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.fnet.hawkeye.model.HierarchyValue;
import com.fnet.hawkeye.model.User;
import com.fnet.hawkeye.model.WorkloadCloseSummary;
import com.fnet.hawkeye.model.WorkloadDetail;
import com.fnet.hawkeye.model.WorkloadSummary;

@Stateless
public class WorkloadRepository {
	
	@Inject
	private UserRepository userRepository;

	@PersistenceContext	(unitName = "primary")
	private EntityManager em;

	
	
	public List<WorkloadDetail> findWorkloadDetailByCompanyProduct(User lUser, long userID, long groupTypeID, String marketName, long jobtypeid) {


		long companyid = lUser.getCompany().getCompanyID();
		long productid = lUser.getProduct().getProductID(); 
		
		List<WorkloadDetail> result = new ArrayList<WorkloadDetail>();
		List<WorkloadDetail> innerResult = new ArrayList<WorkloadDetail>();

		try {
			if (marketName == null || marketName.trim().length() == 0)
				marketName = "ALL";
			String resultString = "";
			Session session = em.unwrap(org.hibernate.Session.class);
			Set<HierarchyValue> vals = lUser.getGeoGroupHierarchyValues();

			Iterator<HierarchyValue> it = vals.iterator();

			HierarchyValue hv = null;
			while (it.hasNext()){
				hv = it.next();
			Query query = session.createSQLQuery("CALL getWorkLoadDetail(:companyID, :productID, :userID, :groupTypeID, :Market, :jobTypeID, :userGeoGroup)")
					.setResultTransformer(Transformers.aliasToBean(WorkloadDetail.class))
					.setParameter("companyID", companyid)
					.setParameter("productID", productid)
					.setParameter("userID", userID)
					.setParameter("groupTypeID", groupTypeID)
					.setParameter("Market", marketName)
					.setParameter("jobTypeID", jobtypeid)
					.setParameter("userGeoGroup", hv.getHierarchyID());
			innerResult = query.list();

			for (WorkloadDetail ws : innerResult){
				result.add(ws);
			}
			innerResult.clear();
		}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public List<WorkloadSummary> findTechWorkloadSummaryByCompanyProduct(User lUser, long userID, long groupTypeID, long jobtypeid) {

		long companyid = lUser.getCompany().getCompanyID();
		long productid = lUser.getProduct().getProductID(); 
		
		List<WorkloadSummary> result = new ArrayList<WorkloadSummary>();
		List<WorkloadSummary> innerResult = new ArrayList<WorkloadSummary>();

		try {
			Session session = em.unwrap(org.hibernate.Session.class);

//			User lUser = userRepository.findById(userID);

			Set<HierarchyValue> vals = lUser.getGeoGroupHierarchyValues();

			Iterator<HierarchyValue> it = vals.iterator();

			HierarchyValue hv = null;
			while (it.hasNext()){
				hv = it.next();
				Query query = session.createSQLQuery("CALL getWorkLoadSummary(:companyID, :productID, :userID, :groupTypeID, :jobTypeID, :userGeoGroup)")
						.setResultTransformer(Transformers.aliasToBean(WorkloadSummary.class))
						.setParameter("companyID", companyid)
						.setParameter("productID", productid)
						.setParameter("userID", userID)
						.setParameter("groupTypeID", groupTypeID)
						.setParameter("jobTypeID", jobtypeid)
						.setParameter("userGeoGroup", hv.getHierarchyID());
				innerResult = query.list();

				for (WorkloadSummary ws : innerResult){
					result.add(ws);
				}
				innerResult.clear();
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<WorkloadSummary> findMktWorkloadSummaryByCompanyProduct(User lUser, long userID, long groupTypeID, long jobtypeid) {


		long companyid = lUser.getCompany().getCompanyID();
		long productid = lUser.getProduct().getProductID(); 
		
		List<WorkloadSummary> result = new ArrayList<WorkloadSummary>();
		List<WorkloadSummary> innerResult = new ArrayList<WorkloadSummary>();

		try {
			String resultString = "";
			Session session = em.unwrap(org.hibernate.Session.class);

			Set<HierarchyValue> vals = lUser.getGeoGroupHierarchyValues();

			Iterator<HierarchyValue> it = vals.iterator();

			HierarchyValue hv = null;
			while (it.hasNext()){
				hv = it.next();			
				long hvValue = hv.getHierarchyID().longValue();

				Query query = session.createSQLQuery("CALL getWorkLoadMarketSummary(:companyID, :productID, :userID, :groupTypeID, :jobTypeID, :userGeoGroup)")
						.setResultTransformer(Transformers.aliasToBean(WorkloadSummary.class))
						.setParameter("companyID", companyid)
						.setParameter("productID", productid)
						.setParameter("userID", userID)
						.setParameter("groupTypeID", groupTypeID)
						.setParameter("jobTypeID", jobtypeid)
						.setParameter("userGeoGroup", hvValue);

				innerResult = query.list();

				for (WorkloadSummary ws : innerResult){
					result.add(ws);
				}
				innerResult.clear();
			}

		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<WorkloadCloseSummary> findWorkloadCloseSummaryByCompanyProduct(User lUser, long jobtypeid){//long companyid, long productid, long jobtypeid, long userID) {
	
		long companyid = lUser.getCompany().getCompanyID();
		long productid = lUser.getProduct().getProductID();
		List<WorkloadCloseSummary> result = new ArrayList<WorkloadCloseSummary>();
		List<WorkloadCloseSummary> innerResult = new ArrayList<WorkloadCloseSummary>();

		try {
			String resultString = "";
			Session session = em.unwrap(org.hibernate.Session.class);		

			Set<HierarchyValue> vals = lUser.getGeoGroupHierarchyValues();

			Iterator<HierarchyValue> it = vals.iterator();

			HierarchyValue hv = null;
			while (it.hasNext()){
				hv = it.next();			

				Query query = session.createSQLQuery("CALL getClosedSummary(:companyID, :productID, :jobTypeID, :userGeoGroup)")
						.setResultTransformer(Transformers.aliasToBean(WorkloadCloseSummary.class))
						.setParameter("companyID", companyid)
						.setParameter("productID", productid)
						.setParameter("jobTypeID", jobtypeid)
						.setParameter("userGeoGroup", hv.getHierarchyID());
				innerResult = query.list();

				for (WorkloadCloseSummary ws : innerResult){
					result.add(ws);
				}
				innerResult.clear();
			}

		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return result;
	}
}