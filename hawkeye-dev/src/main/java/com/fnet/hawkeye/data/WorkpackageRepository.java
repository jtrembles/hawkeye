/**
 * JBoss, Home of Professional Open Source
 * Copyright 2012, Red Hat, Inc., and individual contributors
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fnet.hawkeye.data;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.drools.definition.rule.Rule;

import org.drools.event.rule.AfterActivationFiredEvent;
import org.drools.event.rule.DefaultAgendaEventListener;
import org.drools.runtime.StatefulKnowledgeSession;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.fnet.hawkeye.core.BRMSUtil;
import com.fnet.hawkeye.model.Attachment;
import com.fnet.hawkeye.model.Company;
import com.fnet.hawkeye.model.GroupType;
import com.fnet.hawkeye.model.HierarchyValue;
import com.fnet.hawkeye.model.History;
import com.fnet.hawkeye.model.Note;
import com.fnet.hawkeye.model.Priority;
import com.fnet.hawkeye.model.Product;
import com.fnet.hawkeye.model.Resolution;
import com.fnet.hawkeye.model.Sector;
import com.fnet.hawkeye.model.SimpleHistory;
import com.fnet.hawkeye.model.SimpleSite;
import com.fnet.hawkeye.model.Site;
import com.fnet.hawkeye.model.Status;
import com.fnet.hawkeye.model.Technology;
import com.fnet.hawkeye.model.User;
import com.fnet.hawkeye.model.WorkPackage;
import com.fnet.hawkeye.model.WorkPackageScore;
//import com.fnet.hawkeye.model.WorkPackagePlusScore;
//import com.fnet.hawkeye.model.WorkPackageScore;

@Stateless
public class WorkpackageRepository {

	
	@Inject
	private UserRepository userRepository;
	
	@Inject
	private UtilityRepository utilityRepository;
	
	@PersistenceContext	(unitName = "primary")
	private EntityManager em;

	public History getLastWorkPackageCompletionDate(long workpackageid){
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<History> criteria = cb.createQuery(History.class);
		Root<History> history = criteria.from(History.class);
		
//		return (Integer)entityManager.createQuery("select max(u.id) from User u").getSingleResult();
//		return entityManager.createQuery("select max(u.id) from User u", Integer.class).getSingleResult();
		
		String sql = "SELECT MAX(creationDate) FROM History as h where historyTypeID = 19 AND h.workPackage.workPackageID = :workpackageid AND newStatusID IN (select statusID from Status as s where s.group.groupTypeID = 6)";
		
		History h = new History();
		Timestamp t = (Timestamp)em.createQuery(sql).setParameter("workpackageid", workpackageid).getSingleResult();
		h.setCreationDate(t);
		return h;
		
//		return (Timestamp)em.createQuery(sql).setParameter("workPackageID", workpackageid).getSingleResult();
		
		
		// Swap criteria statements if you would like to try out type-safe criteria queries, a new
		// feature in JPA 2.0
		// criteria.select(member).where(cb.equal(member.get(Member_.name), email));

//		criteria.select(workpackage).where(cb.equal(workpackage.get("company").get("companyID"), companyid), cb.equal(workpackage.get("product").get("productID"), productid));
//		return em.createQuery(criteria).getResultList();
//		return "";
	}

	public List<SimpleHistory> getListLastWorkPackageCompletionDate(String workpackageids){
		
		List<SimpleHistory> hists = new ArrayList<SimpleHistory>();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<SimpleHistory> criteria = cb.createQuery(SimpleHistory.class);
			Root<SimpleHistory> history = criteria.from(SimpleHistory.class);
			
//		return (Integer)entityManager.createQuery("select max(u.id) from User u").getSingleResult();
//		return entityManager.createQuery("select max(u.id) from User u", Integer.class).getSingleResult();
			String sql = "SELECT h.workPackageID, MAX(creationDate) FROM SimpleHistory as h where h.historyTypeID = 19 AND workPackageID in (" + workpackageids + ") AND newStatusID IN (select statusID from Status where groupTypeID = 6) GROUP BY h.workPackageID";
//			String sql = "SELECT workpackageID, MAX(creationDate) FROM History as h where h.workPackage.workPackageID in (" + workpackageids + ") AND newStatusID IN (select statusID from Status as s where s.group.groupTypeID = 6)";
			
			List<Object[]> resultList = em.createQuery(sql).getResultList();
			for (Object[] result : resultList){
				SimpleHistory h = new SimpleHistory();
				h.setCreationDate((Timestamp)result[1]);
				h.setWorkPackageID((Long)result[0]);
				hists.add(h);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hists;		
	}

	public List<WorkPackage> findByCompanyProduct(long companyid, long productid) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WorkPackage> criteria = cb.createQuery(WorkPackage.class);
		Root<WorkPackage> workpackage = criteria.from(WorkPackage.class);
		// Swap criteria statements if you would like to try out type-safe criteria queries, a new
		// feature in JPA 2.0
		// criteria.select(member).where(cb.equal(member.get(Member_.name), email));

		criteria.select(workpackage).where(cb.equal(workpackage.get("company").get("companyID"), companyid), cb.equal(workpackage.get("product").get("productID"), productid));
		return em.createQuery(criteria).getResultList();
	}

	public List<WorkPackage> findByGroupTypesBasedOnUser(User pUser) {
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<WorkPackage> criteria = cb.createQuery(WorkPackage.class);
			Root<WorkPackage> workpackage = criteria.from(WorkPackage.class);

			//get the sites based on user geo group
			List<WorkPackage> lUserVisiblePackages = new ArrayList<WorkPackage>();
			List<SimpleSite> lUserSites = null;
			
			if(pUser.getGeoGroupHierarchyValues() != null && pUser.getGeoGroupHierarchyValues().size() > 0)
			{
				for(HierarchyValue geo : pUser.getGeoGroupHierarchyValues())
				{
					//get all the sites for this hierarchyID
					lUserSites = utilityRepository.findSitesByHierarchyId(geo.getHierarchyID());
					
					List<Long> lSiteIdList = new ArrayList<Long>();
					for(SimpleSite n : lUserSites)
					{
						lSiteIdList.add(n.getSiteID());
					}
					
					if(lUserSites != null)
					{
						
							criteria.select(workpackage).where(workpackage.get("site").get("siteID").in(lSiteIdList), workpackage.get("groupType").in(pUser.getGroupTypes()));
							lUserVisiblePackages.addAll(em.createQuery(criteria).getResultList());
					}
				}
			}
			
			return lUserVisiblePackages;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return null;
	}
	
	public List<WorkPackage> findByWorkPackgeBasedOnUserStatusType(User pUser, GroupType pStatusType) {
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<WorkPackage> criteria = cb.createQuery(WorkPackage.class);
			Root<WorkPackage> workpackage = criteria.from(WorkPackage.class);

			//get the sites based on user geo group
			List<WorkPackage> lUserVisiblePackages = new ArrayList<WorkPackage>();
			List<SimpleSite> lUserSites = null;
			
			if(pUser.getGeoGroupHierarchyValues() != null && pUser.getGeoGroupHierarchyValues().size() > 0)
			{
				for(HierarchyValue geo : pUser.getGeoGroupHierarchyValues())
				{
					//get all the sites for this hierarchyID
					lUserSites = utilityRepository.findSitesByHierarchyId(geo.getHierarchyID());
					
					List<Long> lSiteIdList = new ArrayList<Long>();
					for(SimpleSite n : lUserSites)
					{
						lSiteIdList.add(n.getSiteID());
					}
					
					if(lUserSites != null)
					{
						criteria.select(workpackage).where(workpackage.get("site").get("siteID").in(lSiteIdList), cb.equal(workpackage.get("status").get("group").get("groupTypeID"), 
								pStatusType.getGroupTypeID()), workpackage.get("groupType").in(pUser.getGroupTypes()));
						lUserVisiblePackages.addAll(em.createQuery(criteria).getResultList());
					}
				}
			}
			
			return lUserVisiblePackages;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return null;
	}
	
	
//	public List<WorkPackage> findByCompanyProductGroupTypesBasedOnUserID(long companyid, long productid, long userid) {
//		
//		User usr = userRepository.findById(userid);
//
//		CriteriaBuilder cb = em.getCriteriaBuilder();
//		CriteriaQuery<WorkPackage> criteria = cb.createQuery(WorkPackage.class);
//		Root<WorkPackage> workpackage = criteria.from(WorkPackage.class);
//		criteria.select(workpackage).where(workpackage.get("groupType").in(usr.getGroupTypes()));
//
//		return em.createQuery(criteria).getResultList();
//	}

	public List<WorkPackage> findByUserStatus(User user, long statusid) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WorkPackage> criteria = cb.createQuery(WorkPackage.class);
		Root<WorkPackage> workpackage = criteria.from(WorkPackage.class);
	
		//get the sites based on user geo group
		List<WorkPackage> lUserVisiblePackages = new ArrayList<WorkPackage>();
		List<SimpleSite> lUserSites = null;
		
		if(user.getGeoGroupHierarchyValues() != null && user.getGeoGroupHierarchyValues().size() > 0)
		{
			for(HierarchyValue geo : user.getGeoGroupHierarchyValues())
			{
				//get all the sites for this hierarchyID
				lUserSites = utilityRepository.findSitesByHierarchyId(geo.getHierarchyID());
				
				List<Long> lSiteIdList = new ArrayList<Long>();
				for(SimpleSite n : lUserSites)
				{
					lSiteIdList.add(n.getSiteID());
				}
				
				if(lUserSites != null)
				{
					criteria.select(workpackage).where(cb.equal(workpackage.get("status").get("statusID"), statusid), workpackage.get("site").get("siteID").in(lSiteIdList));
					lUserVisiblePackages.addAll(em.createQuery(criteria).getResultList());
				}
			}
		}
		
		return lUserVisiblePackages;	
	}

//	public List<WorkPackage> findByCompanyProductStatus(long companyid,
//			long productid, long statusid) {
//		CriteriaBuilder cb = em.getCriteriaBuilder();
//		CriteriaQuery<WorkPackage> criteria = cb.createQuery(WorkPackage.class);
//		Root<WorkPackage> workpackage = criteria.from(WorkPackage.class);
//
//		// Swap criteria statements if you would like to try out type-safe criteria queries, a new
//		// feature in JPA 2.0
//		// criteria.select(member).where(cb.equal(member.get(Member_.name), email));
//		criteria.select(workpackage).where(cb.equal(workpackage.get("company").get("companyID"), companyid), cb.equal(workpackage.get("product").get("productID"), productid), cb.equal(workpackage.get("status").get("statusID"), statusid));
//		return em.createQuery(criteria).getResultList();
//	}

	public List<WorkPackage> findByCompanyProductUser(long companyid, long productid,
			long userid) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WorkPackage> criteria = cb.createQuery(WorkPackage.class);
		Root<WorkPackage> workpackage = criteria.from(WorkPackage.class);
		// Swap criteria statements if you would like to try out type-safe criteria queries, a new
		// feature in JPA 2.0
		// criteria.select(member).where(cb.equal(member.get(Member_.name), email));
		criteria.select(workpackage).where(cb.equal(workpackage.get("company").get("companyID"), companyid), cb.equal(workpackage.get("product").get("productID"), productid), cb.equal(workpackage.get("assignedUser").get("userID"), userid));
		return em.createQuery(criteria).getResultList();
	}

	public List<WorkPackage> findByCompanyProductId(long companyid, long productid,
			long id) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WorkPackage> criteria = cb.createQuery(WorkPackage.class);
		Root<WorkPackage> workpackage = criteria.from(WorkPackage.class);
		// Swap criteria statements if you would like to try out type-safe criteria queries, a new
		// feature in JPA 2.0
		// criteria.select(member).where(cb.equal(member.get(Member_.name), email));
		criteria.select(workpackage).where(cb.equal(workpackage.get("company").get("companyID"), companyid), cb.equal(workpackage.get("product").get("productID"), productid), cb.equal(workpackage.get("workPackageID"), id));
		return em.createQuery(criteria).getResultList();
	}
	
	public List<WorkPackage> findForUser(User pUser)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WorkPackage> criteria = cb.createQuery(WorkPackage.class);
		Root<WorkPackage> workpackage = criteria.from(WorkPackage.class);
		
		//get the sites based on user geo group
		List<WorkPackage> lUserVisiblePackages = new ArrayList<WorkPackage>();
		List<SimpleSite> lUserSites = null;
		
		if(pUser.getGeoGroupHierarchyValues() != null && pUser.getGeoGroupHierarchyValues().size() > 0)
		{
			for(HierarchyValue geo : pUser.getGeoGroupHierarchyValues())
			{
				//get all the sites for this hierarchyID
				lUserSites = utilityRepository.findSitesByHierarchyId(geo.getHierarchyID());
				
				List<Long> lSiteIdList = new ArrayList<Long>();
				for(SimpleSite n : lUserSites)
				{
					lSiteIdList.add(n.getSiteID());
				}
				
				if(lUserSites != null)
				{
					
						criteria.select(workpackage).where(workpackage.get("site").get("siteID").in(lSiteIdList));
						lUserVisiblePackages.addAll(em.createQuery(criteria).getResultList());
				}
			}
		}
		
		return lUserVisiblePackages;
	}
	
	public List<WorkPackage> findActivePackageByCompanyProductSite(long companyid, long productid, long siteid) {
		String lsSelectString = //"select w " + 
								"from WorkPackage w " + 
								"where CompanyId = :compId and ProductId = :prodId and SiteId = :sitid " +
									"and statusID in (select s.statusID  " +
									"from Status s " +
									"where s.group.groupType = 'Status' and s.group.groupName = 'Open')";
		
		TypedQuery<WorkPackage> tq = em.createQuery(lsSelectString, WorkPackage.class);
		tq.setParameter("prodId", productid);
		tq.setParameter("compId", companyid);
		tq.setParameter("sitid", siteid);
		
		return tq.getResultList();
	}

	public List<Attachment> findAttachmentsByWorkPackage(long workpackageid) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Attachment> criteria = cb.createQuery(Attachment.class);
		Root<Attachment> attachment = criteria.from(Attachment.class);
		// Swap criteria statements if you would like to try out type-safe criteria queries, a new
		// feature in JPA 2.0
		// criteria.select(member).where(cb.equal(member.get(Member_.name), email));
		criteria.select(attachment).where(cb.equal(attachment.get("workPackage").get("workPackageID"), workpackageid));
		return em.createQuery(criteria).getResultList();
	}

	public void insertAttachmentsByWorkPackage(long workpackageid, String dir, String name, String docLabel) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WorkPackage> criteria = cb.createQuery(WorkPackage.class);
		Root<WorkPackage> workpackage = criteria.from(WorkPackage.class);
		// Swap criteria statements if you would like to try out type-safe criteria queries, a new
		// feature in JPA 2.0
		// criteria.select(member).where(cb.equal(member.get(Member_.name), email));
		criteria.select(workpackage).where(cb.equal(workpackage.get("workPackageID"), workpackageid));

		Attachment a = new Attachment();
		a.setCreationDate(new Timestamp(new Date().getTime()));
		//a.setDocLocation("http://hawkeye.flukenetworks.com/data");

		if (name.indexOf(".pdf") > 0){
			a.setDocType("pdf");
		} else {
			a.setDocType("image");
		}
		a.setDocLocation(dir);
		a.setDocName(name);
		a.setDocLabel(docLabel);					
		a.setLastModifiedDate(new Timestamp(new Date().getTime()));
		a.setWorkPackage(em.createQuery(criteria).getSingleResult());
		try {
			em.persist(a);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}
	
	public void insertWorkPackage(WorkPackage workPackage)
	{
		//load reference objects through entity manager for persistence
		updateEntityManager(workPackage);
		
		em.persist(workPackage);
	}
	
	private void updateEntityManager(WorkPackage workPackage)
	{
		if(workPackage.getCompany() != null && !em.contains(workPackage.getCompany()))
			workPackage.setCompany(em.find(Company.class, workPackage.getCompany().getCompanyID()));
		
		if(workPackage.getProduct() != null && !em.contains(workPackage.getProduct()))
			workPackage.setProduct(em.find(Product.class, workPackage.getProduct().getProductID()));
		
		if(workPackage.getSite() != null && !em.contains(workPackage.getSite()))
			workPackage.setSite(em.find(Site.class, workPackage.getSite().getSiteID()));
		
		if(workPackage.getSector() != null && !em.contains(workPackage.getSector()))
			workPackage.setSector(em.find(Sector.class, workPackage.getSector().getSectorID()));
		
		if(workPackage.getStatus() != null && !em.contains(workPackage.getStatus()))
			workPackage.setStatus(em.find(Status.class, workPackage.getStatus().getStatusID()));
		
		if(workPackage.getTechnology() != null && !em.contains(workPackage.getTechnology()))
			workPackage.setTechnology(em.find(Technology.class, workPackage.getTechnology().getTechnologyID()));
		
		if(workPackage.getGroupType() != null && !em.contains(workPackage.getGroupType())) //this is a GroupType!
			workPackage.setGroupType(em.find(GroupType.class, workPackage.getGroupType().getGroupTypeID()));
		
		if(workPackage.getPriority() != null && !em.contains(workPackage.getPriority()))
			workPackage.setPriority(em.find(Priority.class, workPackage.getPriority().getPriorityID()));
		
		if(workPackage.getAssignedUser() != null && !em.contains(workPackage.getAssignedUser()))
			workPackage.setAssignedUser(em.find(User.class, workPackage.getAssignedUser().getUserID()));
		
		if(workPackage.getResolution() != null && !em.contains(workPackage.getResolution()))
			workPackage.setResolution(em.find(Resolution.class, workPackage.getResolution().getResolutionID()));
	}
	
	
	public void insertWorkPackageHistory(History history)
	{
		try {
			updateEntityManager(history);
			em.persist(history);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void updateEntityManager(History history)
	{
		if(history.getWorkPackage() != null && !em.contains(history.getWorkPackage()))
			history.setWorkPackage(em.find(WorkPackage.class, history.getWorkPackage().getWorkPackageID()));
		
//		if(history.getCurrentStatus() != null && !em.contains(history.getCurrentStatus()))
//			history.setCurrentStatus(em.find(Status.class, history.getCurrentStatus().getStatusID()));
//	
//		if(history.getNewStatus() != null && !em.contains(history.getNewStatus()))
//			history.setNewStatus(em.find(Status.class, history.getNewStatus().getStatusID()));
		
		if(history.getUser() != null && !em.contains(history.getUser()))
			history.setUser(em.find(User.class, history.getUser().getUserID()));
	}
	
	//returns a history object with ONLY the CreationDate populated
	public History getPreviousWorkPackageHistoryDate(long workpackageid, long historytypeid){
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<History> criteria = cb.createQuery(History.class);
		Root<History> history = criteria.from(History.class);
		
//		return (Integer)entityManager.createQuery("select max(u.id) from User u").getSingleResult();
//		return entityManager.createQuery("select max(u.id) from User u", Integer.class).getSingleResult();
		
		//just get record for completed statuses  (5 == Complete)
		String sql = "SELECT MAX(creationDate) FROM History where workPackageID = :workpackageid and historyTypeID = :historyTypeID";
		
		History h = new History();
		try
		{
			List<Timestamp> t = (List<Timestamp>)em.createQuery(sql).setParameter("workpackageid", workpackageid).setParameter("historyTypeID", historytypeid).getResultList();
			if(t != null && t.size() > 0)
				h.setCreationDate(t.get(0));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return h;
		
//		return (Timestamp)em.createQuery(sql).setParameter("workPackageID", workpackageid).getSingleResult();
		
		
		// Swap criteria statements if you would like to try out type-safe criteria queries, a new
		// feature in JPA 2.0
		// criteria.select(member).where(cb.equal(member.get(Member_.name), email));

//		criteria.select(workpackage).where(cb.equal(workpackage.get("company").get("companyID"), companyid), cb.equal(workpackage.get("product").get("productID"), productid));
//		return em.createQuery(criteria).getResultList();
//		return "";
	}
	public void updateWorkpackage(Long workpackageid, Long statusid,
			Long technicianid, Long userid, Long jobtypeid, Long resolutionid) {  //, Long priorityid) {  FOR LATER!!!!

		BRMSUtil brms = null;
		StatefulKnowledgeSession ksession = null;

		try{
			brms = new BRMSUtil();
			ksession = brms.getStatefulSession();

			CriteriaBuilder cb = em.getCriteriaBuilder();

			CriteriaQuery<WorkPackage> workPackageCriteria = cb.createQuery(WorkPackage.class);
			Root<WorkPackage> workPackage = workPackageCriteria.from(WorkPackage.class);
			workPackageCriteria.select(workPackage).where(cb.equal(workPackage.get("workPackageID"), workpackageid));
			WorkPackage wp = em.createQuery(workPackageCriteria).getSingleResult();
			List<GroupType> historyTypes = utilityRepository.findHistoryTypeByCompanyProduct(wp.getCompany(), wp.getProduct());

			GroupType gt = null;
			try {
				CriteriaQuery<GroupType> groupCriteria = cb.createQuery(GroupType.class);
				Root<GroupType> group = groupCriteria.from(GroupType.class);
				groupCriteria.select(group).where(cb.equal(group.get("groupTypeID"), jobtypeid));
				gt = em.createQuery(groupCriteria).getSingleResult();
			} catch (Exception ex) {
				gt = null;
			}
			Resolution res = null;
			if (resolutionid != null)
				res = em.find(Resolution.class, resolutionid);
//			Priority priority = em.find(Priority.class, priorityid);
			
			Status sts = null;
			if (statusid != null)
				sts = em.find(Status.class, statusid);
			
			User tch = null;
			if (technicianid != null)
				tch = em.find(User.class, technicianid);
			
			User usr = null;
			if (userid != null)
				usr = em.find(User.class, userid);
			

			//			if (notes != null && notes != "")
			//				wp.setEngNotes(notes);

			ksession.addEventListener( new DefaultAgendaEventListener() {
				public void afterActivationFired(AfterActivationFiredEvent event) {
					super.afterActivationFired( event );
					System.out.println( event );
					final Rule rule = event.getActivation().getRule();
                    System.out.println(String.format("%s.%s - %s", rule.getPackageName(), rule.getName(), event.getClass().getSimpleName()));
				}
			}); 

			if (tch != null)
				tch.setLoggedIn(false);
			
			if (tch != usr){
				usr.setLoggedIn(true);
				ksession.insert(usr);
			}
			if (gt != null)
				ksession.insert(gt);
			if (wp != null)
				ksession.insert(wp);
			if (sts != null)
				ksession.insert(sts);
			if (res != null)
				ksession.insert(res);
//			if (priority != null)
//				ksession.insert(priority);
			if (tch != null)
				ksession.insert(tch);
			
			if(historyTypes != null)
			{
				for(GroupType hist : historyTypes)
					ksession.insert(hist);
			}
			
			int rules = ksession.fireAllRules();

			System.out.println("# of rules fired: " + rules);

			//			wp.setEngNotes(notes);
			if (rules > 0){
				Collection<Object> objs = ksession.getObjects();
//				ksession.dispose();
//				brms.stopResourceChangeScannerServices();

				History h = null;

				for (Object obj : objs){
					if (obj.getClass() == History.class){
						h = (History)obj;
						History h1 = getPreviousWorkPackageHistoryDate(workpackageid, h.getHistoryType().getGroupTypeID());
						h.setCurrentStatusCreationDate(h1.getCreationDate() != null ? h1.getCreationDate() : getToday());
						h.setCreationDate(new Timestamp(new Date().getTime()));
						em.persist(h);
						//insertWorkPackageHistory(h);
					} else {
						System.out.println(obj.getClass().getSimpleName());
					}
				}

				//TODO -- this is because of a bug in the 5.5 rules
				//if you set the value of a field to null in the "Then" section of a rule and 
				//that value is being dereferenced in the "When" section of the rule a null reference exception will be thrown.
				//--- start rules hack----------------
				//this code will remove a resolution from the work package is one was not passed in
				if(res == null)
					wp.setResolution(null);
				// --- end rules hack ---------------
				
				
				em.merge(wp);
			}
			System.out.println("Process completed");
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if(ksession != null)
				ksession.dispose();
			if(brms != null)
				brms.stopResourceChangeScannerServices();
			
			ksession = null;
			brms = null;
		}
	}

	public void updateWorkpackageNotes(long workpackageid, Long technicianid, String notes) {

		//		final KnowledgeRuntimeLogger logger;

		try{
			WorkPackage wp = em.find(WorkPackage.class, workpackageid);
			User usr = em.find (User.class, technicianid);

			SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yyyy");
			String date = sdf.format(new Date()); 

			if (notes != null && notes != "")
			{
				if(!notes.endsWith("\r\n"))
					notes = notes + "\r\n";
				String oldNotes = wp.getEngNotes();
				if (oldNotes != null && oldNotes.length() > 0)
					wp.setEngNotes(date +" -- " + usr.getUserEmail() +"\r\n" + notes + "\r\n" + oldNotes);
				else
					wp.setEngNotes(date +" -- " + usr.getUserEmail() +"\r\n" + notes + "\r\n");

				//add a history record --------------
				History hist = addHistory(wp, usr, null, null, "Note", "Notes added");
				//-----------------------------------
			}
			em.merge(wp);
			System.out.println("Process completed");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public History addHistory(WorkPackage workPackage, User user, Long currentValue, Long newValue, String historyType, String note)
	{

		//need to get the 
		GroupType histType = utilityRepository.findHistoryTypeByCompanyProductGroupName(workPackage.getCompany(), workPackage.getProduct(), historyType);
		
		if(histType == null) //log this???
			return null;
	
		History hist = getPreviousWorkPackageHistoryDate(workPackage.getWorkPackageID(), histType.getGroupTypeID());

		hist.setWorkPackage(workPackage);
		hist.setUser(user);
		hist.setCurrentStatusCreationDate(hist.getCreationDate() != null ? hist.getCreationDate() : getToday());
		hist.setCreationDate(new Timestamp(new Date().getTime()));
		
		hist.setCurrentStatusID(currentValue);
		hist.setNewStatusID(newValue);
		hist.setHistoryType(histType);
		hist.setNotes(note);
		
		insertWorkPackageHistory(hist);
		return hist;
	}
	
	public History addHistory(long workPackageId, long userId, Long currentValue, Long newValue, String histType, String note)
	{
		WorkPackage wp = findWorkPackage(workPackageId);
		User usr = userRepository.findById(userId);
		
		return addHistory(wp, usr, currentValue, newValue, histType, note);
	}

	public WorkPackage findWorkPackage(long workpackageid) {
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<WorkPackage> criteria = cb.createQuery(WorkPackage.class);
			Root<WorkPackage> workpackage = criteria.from(WorkPackage.class);
			// Swap criteria statements if you would like to try out type-safe criteria queries, a new
			// feature in JPA 2.0
			// criteria.select(member).where(cb.equal(member.get(Member_.name), email));

			criteria.select(workpackage).where(cb.equal(workpackage.get("workPackageID"), workpackageid));
			return em.createQuery(criteria).getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public WorkPackage findWorkPackage(String siteName, long company, long product) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WorkPackage> criteria = cb.createQuery(WorkPackage.class);
		Root<WorkPackage> workpackage = criteria.from(WorkPackage.class);
		// Swap criteria statements if you would like to try out type-safe criteria queries, a new
		// feature in JPA 2.0
		// criteria.select(member).where(cb.equal(member.get(Member_.name), email));

		criteria.select(workpackage).where(cb.equal(workpackage.get("site").get("siteName"), siteName), cb.equal(workpackage.get("company").get("companyID"), company), cb.equal(workpackage.get("product").get("productID"), product), cb.notEqual(workpackage.get("status").get("statusName"), "Complete"));
		List<WorkPackage> workPackages = em.createQuery(criteria).getResultList();
		if (workPackages.size() >  0) {
			return workPackages.get(0);
		}
		
		return null;
	}

	public void deleteWorkpackageAttachments(long workpackageid,
			String attachmentName) {
		try{
			System.out.print("WPID" + workpackageid + ":" + attachmentName );

			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<WorkPackage> workPackageCriteria = cb.createQuery(WorkPackage.class);
			Root<WorkPackage> workPackage = workPackageCriteria.from(WorkPackage.class);
			workPackageCriteria.select(workPackage).where(cb.equal(workPackage.get("workPackageID"), workpackageid));
			WorkPackage wp = em.createQuery(workPackageCriteria).getSingleResult();

			CriteriaQuery<Attachment> attachmentCriteria = cb.createQuery(Attachment.class);
			Root<Attachment> attachment = attachmentCriteria.from(Attachment.class);
			attachmentCriteria.select(attachment).where(cb.equal(attachment.get("workPackage").get("workPackageID"), workpackageid), cb.equal(attachment.get("docName"), attachmentName));
			List<Attachment> attachs = em.createQuery(attachmentCriteria).getResultList();

			for (Attachment attach : attachs){
				Attachment deleteItem = em.find(Attachment.class, attach.getAttachmentID());
				SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
				String date = sdf.format(new Date()); 
				em.remove(deleteItem);
				System.out.println("Attachment Deleted: " + date);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}


	}
	
	public List<History> findWorkPackageHistory(long workpackageid) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<History> criteria = cb.createQuery(History.class);
		Root<History> history = criteria.from(History.class);
		// Swap criteria statements if you would like to try out type-safe criteria queries, a new
		// feature in JPA 2.0
		// criteria.select(member).where(cb.equal(member.get(Member_.name), email));

		criteria.select(history).orderBy(cb.asc(history.get("currentStatusCreationDate"))).where(cb.equal(history.get("workPackage").get("workPackageID"), workpackageid));
		return em.createQuery(criteria).getResultList();
	}

	public WorkPackage insertWorkpackage(String caseNumber, 
			String siteID, String rfEngineer, String 
			sectorName, String rfEngineerNum, Technology tech, 
			  String frequency, String engineeringZone, 
			String uplinkChFreq, String troubleTicketNumber,
			String rSSI, User user, String notes, Company company, Product product, Status status, GroupType group, Priority priority)
			throws Exception {

		WorkPackage wp = new WorkPackage();// CreateWorkPackage(rce, company, product, user, tech, status, group, priority);
		try {
		
			Site site = utilityRepository.findSiteById(new Long(siteID));
			if(site == null)
				throw new Exception(String.format("site:%s not found in database", siteID));
			
			HierarchyValue hierarchy = utilityRepository.findHierarchyByCompanyProductSite(company, product, site);
			if(hierarchy == null)
				throw new Exception(String.format("Hierarchy Value not found in database for site:%s", site.getSiteName()));
			
			HierarchyValue parentHierarchyValue = utilityRepository.findHierarchyByHierarchyId(hierarchy.getParentHierarchyID());
			if(parentHierarchyValue == null)
				throw new Exception(String.format("No parent hierarch found for site:%s", site.getSiteName()));
			
			wp.setHierarchyReportID(parentHierarchyValue.getHierarchyID());
			wp.setOpsTurf(parentHierarchyValue.getHierarchyName());
//			wp.setAssignedUser(user);
			wp.setCaseNumber(caseNumber);
			wp.setCompany(company);
			wp.setCreationDate(getToday());
			wp.setEngineeringZone(engineeringZone);
			wp.setFrequency(frequency);
			wp.setGroupType(group);
//			SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yyyy");
//			String date = sdf.format(new Date()); 
//			if (notes != null && notes.length() > 0)
//				wp.setEngNotes(date +" -- " + user.getUserEmail() +"\r\n" + notes);
			
			//			wp.setIehNotes(notes);
			wp.setPriority(priority);
			wp.setProduct(product);
			wp.setRfEngineer(rfEngineer);
			wp.setRfEngineerNum(rfEngineerNum);
			wp.setrSSI(new Long(rSSI));
			wp.setSite(site);
			wp.setStatus(status);
			wp.setTechnology(tech);
			wp.setUplinkChFreq(uplinkChFreq);
			wp.setSector(utilityRepository.findSectorBySectorName(sectorName));

			try
			{
				insertWorkPackage(wp);
				if(notes != null && notes.length() > 0)
					addWorkpackageNote(wp.getWorkPackageID(), user.getApikey(), notes);
			}
			catch(Exception e)
			{
				throw new Exception(String.format("Failed to insert work package for case number - %s\r\n%s", caseNumber, e.getMessage()));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
			
			return wp;		
	}
	
	private Timestamp getToday() {
		// TODO Auto-generated method stub
		return new java.sql.Timestamp(java.lang.System.currentTimeMillis());
		
	}	
	
	public Attachment insertAttachmentsByWorkPackage(long workpackageid, String dir, String name, String docLabel, boolean isImage) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WorkPackage> criteria = cb.createQuery(WorkPackage.class);
		Root<WorkPackage> workpackage = criteria.from(WorkPackage.class);
		// Swap criteria statements if you would like to try out type-safe criteria queries, a new
		// feature in JPA 2.0
		// criteria.select(member).where(cb.equal(member.get(Member_.name), email));
		criteria.select(workpackage).where(cb.equal(workpackage.get("workPackageID"), workpackageid));

		Attachment a = new Attachment();
		a.setCreationDate(new Timestamp(new Date().getTime()));
		//a.setDocLocation("http://hawkeye.flukenetworks.com/data");

		if (!isImage){
			a.setDocType("other");
		} else {
			a.setDocType("image");
		}
		a.setDocLocation(dir);
		a.setDocName(name);
		a.setDocLabel(docLabel);					
		a.setLastModifiedDate(new Timestamp(new Date().getTime()));
		a.setWorkPackage(em.createQuery(criteria).getSingleResult());
		try {
			em.persist(a);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			a = null;
		}
		
		return a;
	}
	
	
	public boolean attachmentExists(long workpackageid, String name)
	{
		return findAttachment(workpackageid, name) != null;
	}
	
	private Attachment findAttachment(long workpackageid, String name)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Attachment> criteria = cb.createQuery(Attachment.class);
		Root<Attachment> attachment = criteria.from(Attachment.class);
		// Swap criteria statements if you would like to try out type-safe criteria queries, a new
		// feature in JPA 2.0
		// criteria.select(member).where(cb.equal(member.get(Member_.name), email));
		criteria.select(attachment).where(cb.equal(attachment.get("workPackage").get("workPackageID"), workpackageid), cb.equal(attachment.get("docName"), name));
		
		List<Attachment> lResults = em.createQuery(criteria).getResultList(); //will not throw exception if none found
		if(lResults.size() > 0)
			return lResults.get(0);
		else
			return null;
	}
	
	public Attachment updateAttachment(long workpackageid, String name, String docLabel) 
	{
		Attachment lAttachment = findAttachment(workpackageid, name);
		
		try
		{
			if(lAttachment == null)
				throw new Exception("No matching attachment found.");
			
			lAttachment.setDocLabel(docLabel);
			lAttachment.setLastModifiedDate(new Timestamp(new Date().getTime()));
			
			em.persist(lAttachment);
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			lAttachment = null;
		}
		
		return lAttachment;
	}

	public List<Note> findNotesByWorkPackage(long workpackageid) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Note> criteria = cb.createQuery(Note.class);
		Root<Note> note = criteria.from(Note.class);
		criteria.select(note).where(cb.equal(note.get("workPackage").get("workPackageID"), workpackageid)).orderBy(cb.desc(note.get("creationDate")));
		return em.createQuery(criteria).getResultList();
	}

	public void addWorkpackageNote(long workpackageid, String apikey,
			String note) {
		try{
			WorkPackage wp = em.find(WorkPackage.class, workpackageid);
			User usr = userRepository.findByApiKey(apikey);

			SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yyyy");
			String date = sdf.format(new Date()); 
			Note noteRow = new Note();
			
			if (note != null && note != "")
			{
				noteRow.setNote(note);
				noteRow.setCompany(usr.getCompany());
				noteRow.setProduct(usr.getProduct());
				noteRow.setUser(usr);
				noteRow.setWorkPackage(wp);
				noteRow.setCreationDate(getToday());

				//add a history record --------------
				History hist = addHistory(wp, usr, null, null, "Note", "Notes added");
				//-----------------------------------
			}
			em.persist(noteRow);
			System.out.println("Process completed");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void addWorkpackageNote(WorkPackage wp, User usr,
			String note) {
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yyyy");
			String date = sdf.format(new Date()); 
			Note noteRow = new Note();
			
			if (note != null && note != "")
			{
				noteRow.setNote(note);
				noteRow.setCompany(usr.getCompany());
				noteRow.setProduct(usr.getProduct());
				noteRow.setUser(usr);
				noteRow.setWorkPackage(wp);
				noteRow.setCreationDate(getToday());

				//add a history record --------------
				History hist = addHistory(wp, usr, null, null, "Note", "Notes added");
				//-----------------------------------
			}
			em.persist(noteRow);
			System.out.println("Add note process completed");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<WorkPackageScore> findScoreByWorkPackageID(Integer workpackageid) {

		List<WorkPackageScore> result = new ArrayList<WorkPackageScore>();
		if (workpackageid == 0)
			workpackageid = -1;

		try {
			String resultString = "";
			Session session = em.unwrap(org.hibernate.Session.class);

			Query query = session.createSQLQuery("CALL getScoreCard2(:workPackageID)")
					.setResultTransformer(Transformers.aliasToBean(WorkPackageScore.class))
					.setParameter("workPackageID", workpackageid);
			result = query.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return result;
	}

//	public List<WorkPackagePlusScore> findWorkPackageHotList(String apikey) {
//		CriteriaBuilder cb = em.getCriteriaBuilder();
//		CriteriaQuery<WorkPackage> criteria = cb.createQuery(WorkPackage.class);
//		Root<WorkPackage> workpackage = criteria.from(WorkPackage.class);
//		// Swap criteria statements if you would like to try out type-safe criteria queries, a new
//		// feature in JPA 2.0
//		// criteria.select(member).where(cb.equal(member.get(Member_.name), email));
//
//		User usr = userRepository.findByApiKey(apikey);
//		
//		criteria.select(workpackage).where(
//				cb.equal(workpackage.get("company").get("companyID"), 
//						usr.getCompany().getCompanyID()), 
//				cb.equal(workpackage.get("product").get("productID"),
//						usr.getProduct().getProductID()));
//
//		List<WorkPackage> items = em.createQuery(criteria).getResultList();
//		List<WorkPackagePlusScore> results = new ArrayList<WorkPackagePlusScore>();
//		ExecutorService executor;
//		for (WorkPackage _package : items){
//			
//			
//			 Runnable worker = new WorkPackageScoreLoader(_package);
//		     Future<?> f = executor.submit(worker);
//		     if (f.isDone()){
//		    	 results.add(new WorkPackagePlusScore((f.get()))
//		     }
//
//			results.add();
//			
//		}
//		return results;
//		return null;
//	}		
	
	
	
}
