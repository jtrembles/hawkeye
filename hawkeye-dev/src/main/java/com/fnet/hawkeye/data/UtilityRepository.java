/**
 * JBoss, Home of Professional Open Source
 * Copyright 2012, Red Hat, Inc., and individual contributors
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fnet.hawkeye.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.fnet.hawkeye.model.Company;
import com.fnet.hawkeye.model.Dashboard;
import com.fnet.hawkeye.model.GroupType;
import com.fnet.hawkeye.model.HierarchyIdHolder;
import com.fnet.hawkeye.model.HierarchyValue;
import com.fnet.hawkeye.model.Priority;
import com.fnet.hawkeye.model.Product;
import com.fnet.hawkeye.model.Resolution;
import com.fnet.hawkeye.model.Score;
import com.fnet.hawkeye.model.Sector;
import com.fnet.hawkeye.model.SimpleSite;
import com.fnet.hawkeye.model.Site;
import com.fnet.hawkeye.model.Status;
import com.fnet.hawkeye.model.Technology;
import com.fnet.hawkeye.model.User;
import com.fnet.hawkeye.model.VTechnologyCompanyProduct;

@ApplicationScoped
public class UtilityRepository {

	@Inject
	private EntityManager em;

	public List<Status> findByCompanyProduct(long companyid, long productid) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Status> criteria = cb.createQuery(Status.class);
		Root<Status> status = criteria.from(Status.class);
		try {
			criteria.select(status).where(cb.equal(status.get("company").get("companyID"), companyid), cb.equal(status.get("product").get("productID"), productid),  cb.equal(status.get("showUI"), 1));
		} catch (Exception e) {

			e.printStackTrace();
			return null;
		}
		return em.createQuery(criteria).getResultList();

	}

	public Status findByCompanyProductId(long companyid, long productid,
			long id) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Status> criteria = cb.createQuery(Status.class);
		Root<Status> status = criteria.from(Status.class);
		// Swap criteria statements if you would like to try out type-safe criteria queries, a new
		// feature in JPA 2.0
		// criteria.select(member).where(cb.equal(member.get(Member_.name), email));
		criteria.select(status).where(cb.equal(status.get("company").get("companyID"), companyid), cb.equal(status.get("product").get("productID"), productid), cb.equal(status.get("statusID"), id));
		return em.createQuery(criteria).getSingleResult();
	}

	public List<Score> findScoreByWorkPackageID(long workpackageid) {

		List<Score> result = new ArrayList<Score>();

		if (workpackageid == 0)
			workpackageid = -1;

		try {
			String resultString = "";
			Session session = em.unwrap(org.hibernate.Session.class);

			Query query = session.createSQLQuery("CALL getScoreCard(:workPackageID)")
					.setResultTransformer(Transformers.aliasToBean(Score.class))
					.setParameter("workPackageID", workpackageid);
			result = query.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<Dashboard> findDashboardByCompanyProductDateRange(
			long companyID, long productID, Date startDate, Date endDate) {

		List<Dashboard> result = new ArrayList<Dashboard>();

		try {
			String resultString = "";
			Session session = em.unwrap(org.hibernate.Session.class);

			Query query = session.createSQLQuery("CALL getPmDashboard(:companyID, :productID, :startDate, :endDate)")
					.setResultTransformer(Transformers.aliasToBean(Dashboard.class))
					.setParameter("companyID", companyID)
					.setParameter("productID", productID)
					.setParameter("startDate", startDate)
					.setParameter("endDate", endDate);
			result = query.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return result;
	}


	public Status findStatusByCompanyProductStatusName(Company company, Product product, String statusName)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Status> criteria = cb.createQuery(Status.class);
		Root<Status> status = criteria.from(Status.class);
		criteria.select(status).where(cb.equal(status.get("statusName"), statusName), cb.equal(status.get("company"), company), cb.equal(status.get("product"), product));

		List<Status> resultList = em.createQuery(criteria).getResultList();
		if(resultList != null && resultList.size() > 0)
			return resultList.get(0);

		return null;
	}
	

	public GroupType findStatusTypeByCompanyProductGroupName(long companyid, long productid, String groupName)
	{
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<GroupType> criteria = cb.createQuery(GroupType.class);
			Root<GroupType> groupType = criteria.from(GroupType.class);
			criteria.select(groupType).where(cb.equal(groupType.get("groupType"), "status"), cb.equal(groupType.get("company").get("companyID"), companyid), 
					cb.equal(groupType.get("product").get("productID"), productid), cb.equal(groupType.get("groupName"), groupName));
			List<GroupType> resultList = em.createQuery(criteria).getResultList();
			if(resultList != null && resultList.size() > 0)
				return resultList.get(0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	

	public Sector findSectorBySectorName(String name)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Sector> criteria = cb.createQuery(Sector.class);
		Root<Sector> sector = criteria.from(Sector.class);
		criteria.select(sector).where(cb.equal(sector.get("sectorName"), name));

		List<Sector> resultList = em.createQuery(criteria).getResultList();
		if(resultList != null && resultList.size() > 0)
			return resultList.get(0);

		return null;
	}

	public Site findSiteByCompanyProductSiteName(Company company, Product product, String siteName)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Site> criteria = cb.createQuery(Site.class);
		Root<Site> site = criteria.from(Site.class);
		criteria.select(site).where(cb.equal(site.get("siteName"), siteName), cb.equal(site.get("company").get("companyID"), company.getCompanyID()), cb.equal(site.get("product").get("productID"), product.getProductID()));

		List<Site> resultList = em.createQuery(criteria).getResultList();
		if(resultList != null && resultList.size() > 0)
			return resultList.get(0);

		return null;
	}

	public List<Technology> findOLDTechologiesByCompanyProduct(long companyid, long productid)
	{
		List<Technology> techList = null;
		try {
			List<Technology> technologyList = null;
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Technology> criteria = cb.createQuery(Technology.class);
			Root<Technology> technology = criteria.from(Technology.class);
			criteria.select(technology);

			techList = em.createQuery(criteria).getResultList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return techList;
	}

	public List<Technology> findTechologiesByCompanyProduct(long companyid, long productid)
	{
		List<VTechnologyCompanyProduct> techList = null;
		List<Technology> newList = new ArrayList<Technology>();
		try {
			//List<VTechnologyCompanyProduct> technologyList = null;
			//			CriteriaBuilder cb = em.getCriteriaBuilder();
			//			CriteriaQuery<VTechnologyCompanyProduct> criteria = cb.createQuery(VTechnologyCompanyProduct.class);
			//			Root<VTechnologyCompanyProduct> technology = criteria.from(VTechnologyCompanyProduct.class);
			//			criteria.select(technology).where(cb.equal(technology.get("companyID"), companyid), cb.equal(technology.get("productID"), productid));

			//select t1.companyID AS companyID,t1.productID AS productID,t2.technologyID AS technologyID,t2.technologyName AS technologyName,t2.type AS type,t2.bandwidth AS bandwidth,t2.creationDate AS creationDate,t2.lastModifiedDate AS lastModifiedDate from (technologyCompanyProduct t1 join technology t2) where (t1.technologyID = t2.technologyID)
			String sql = "select t1.companyID AS companyID,t1.productID AS productID," +
					"t2.technologyID AS technologyID,t2.technologyName AS technologyName," +
					"t2.type AS type,t2.bandwidth AS bandwidth " +
					"from technologyCompanyProduct t1 left join technology t2 " +
					"ON (t1.technologyID = t2.technologyID) where companyID = :companyId AND " +
					" productID = :productId";

			String resultString = "";
			Session session = em.unwrap(org.hibernate.Session.class);
			//					.setParameter("workPackageID", workpackageid);

			Query query = session.createSQLQuery(sql).setParameter("companyId", companyid).setParameter("productId", productid)
					.setResultTransformer(Transformers.aliasToBean(VTechnologyCompanyProduct.class));
			techList = query.list();


			for (VTechnologyCompanyProduct v : techList){
				if (v.getTechnologyID() != null)
				{
					Technology t = findTechnologyById(v.getTechnologyID().longValue());
					newList.add(t);
				}
			}
			//			javax.persistence.Query technologyList = em.createNativeQuery(sql, VTechnologyCompanyProduct.class);
			//			techList = technologyList.getResultList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return newList;
	}

	public List<Technology> findTechologiesByCompanyProduct(Company company, Product product)
	{

		return findTechologiesByCompanyProduct(company.getCompanyID(), product.getProductID());

		//		return technologyList;
	}

	public GroupType findJobTypeGroupByCompanyProductGroupName(Company company, Product product, String name)
	{
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<GroupType> criteria = cb.createQuery(GroupType.class);
			Root<GroupType> groupType = criteria.from(GroupType.class);
			criteria.select(groupType).where(cb.equal(groupType.get("groupType"), "jobType"), cb.equal(groupType.get("groupName"), name), cb.equal(groupType.get("company").get("companyID"), company.getCompanyID()), cb.equal(groupType.get("product").get("productID"), product.getProductID()));

			List<GroupType> resultList = em.createQuery(criteria).getResultList();
			if(resultList != null && resultList.size() > 0)
				return resultList.get(0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public List<GroupType> findJobTypeByCompanyProductGroupName(long companyid, long productid)
	{
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<GroupType> criteria = cb.createQuery(GroupType.class);
			Root<GroupType> groupType = criteria.from(GroupType.class);
			criteria.select(groupType).where(cb.equal(groupType.get("groupType"), "jobType"), cb.equal(groupType.get("company").get("companyID"), companyid), cb.equal(groupType.get("product").get("productID"), productid));
			List<GroupType> resultList = em.createQuery(criteria).getResultList();
			if(resultList != null && resultList.size() > 0)
				return resultList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Priority findPriorityByCompanyProductPriorityName(Company company, Product product, String name)
	{
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Priority> criteria = cb.createQuery(Priority.class);
			Root<Priority> priority = criteria.from(Priority.class);
			criteria.select(priority).where(cb.equal(priority.get("priorityName"), name), cb.equal(priority.get("company").get("companyID"), company.getCompanyID()), cb.equal(priority.get("product").get("productID"), product.getProductID()));

			List<Priority> resultList = em.createQuery(criteria).getResultList();
			if(resultList != null && resultList.size() > 0)
				return resultList.get(0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public Product findProductById(Long productId)
	{
		return em.find(Product.class, productId);
	}

	public Company findCompanyById(Long companyId)
	{
		return em.find(Company.class, companyId);
	}

	public HierarchyValue findHierarchyByCompanyProductSite(Company company, Product product, Site site)
	{
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<HierarchyValue> criteria = cb.createQuery(HierarchyValue.class);
			Root<HierarchyValue> hierarchy = criteria.from(HierarchyValue.class);
			criteria.select(hierarchy).where(cb.equal(hierarchy.get("hierarchyName"), site.getSiteName()), cb.equal(hierarchy.get("companyID"), company.getCompanyID()), cb.equal(hierarchy.get("productID"), product.getProductID()));

			List<HierarchyValue> resultList = em.createQuery(criteria).getResultList();
			if(resultList != null && resultList.size() > 0)
				return resultList.get(0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;	
	}

	public HierarchyValue findHierarchyByHierarchyId(Long id)
	{
		return em.find(HierarchyValue.class, id);
	}

	public Site findSiteById(Long id)
	{
		return em.find(Site.class, id);
	}
	
	public List<Site> findSiteByCompanyProduct(long companyid, long productid) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Site> criteria = cb.createQuery(Site.class);
		Root<Site> site = criteria.from(Site.class);
		try {
			criteria.select(site).where(cb.equal(site.get("company").get("companyID"), companyid), cb.equal(site.get("product").get("productID"), productid));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return em.createQuery(criteria).getResultList();
	}
	
	public List<SimpleSite> findSitesByHierarchyId(Long hierarchyId)
	{
		List<HierarchyIdHolder> result = null; //will not actually be full objects

		try {
			String resultString = "";
			Session session = em.unwrap(org.hibernate.Session.class);

			Query query = session.createSQLQuery("CALL getChildrenFromHierarchyID(:rootID, 'Site')")
					.setResultTransformer(Transformers.aliasToBean(HierarchyIdHolder.class))
					.setParameter("rootID", hierarchyId);
			result = query.list();
			
			List<Long> lHierarchyIdList = new ArrayList<Long>();
			for(HierarchyIdHolder n : result)
			{
				lHierarchyIdList.add(new Long(n.getHierarchyID().longValue()));
			}
			
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<SimpleSite> criteria = cb.createQuery(SimpleSite.class);
			Root<SimpleSite> site = criteria.from(SimpleSite.class);
			criteria.select(site).where(site.get("hierarchyID").in(lHierarchyIdList));
			
			List<SimpleSite> resultList = em.createQuery(criteria).getResultList();
			
			if(resultList != null && resultList.size() > 0)
				return resultList;
			
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<SimpleSite> findSitesByHierarchyValue(HierarchyValue hierarchy)
	{
		return findSitesByHierarchyId(hierarchy.getHierarchyID());
	}

	

	public Technology findTechnologyById(Long techId)
	{
		return em.find(Technology.class, techId);
	}

	public List<Site> findSiteByCompanyProductSearchTerm(long companyid,
			long productid, String searchTerm) {


		searchTerm += "%";
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Site> criteria = cb.createQuery(Site.class);
		Root<Site> site = criteria.from(Site.class);

		try {

			ParameterExpression<String> p = cb.parameter(String.class, "siteName");

			criteria.select(site).distinct(true).where(cb.equal(site.get("company").get("companyID"), companyid), 
					cb.equal(site.get("product").get("productID"), productid), cb.like(site.<String>get("siteName"), p) );
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return em.createQuery(criteria).setParameter("siteName", searchTerm).getResultList();
	}

	public GroupType findJobTypeById(long jobtypeid) {
		return em.find(GroupType.class, jobtypeid);
	}

	public List<Resolution> findResolutionsByProduct(long productid) {
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Resolution> criteria = cb.createQuery(Resolution.class);
			Root<Resolution> resolution = criteria.from(Resolution.class);
			criteria.select(resolution).where(cb.equal(resolution.get("product").get("productID"), productid));
			List<Resolution> resultList = em.createQuery(criteria).getResultList();
			if(resultList != null && resultList.size() > 0)
				return resultList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;	
	}

	public List<Priority> findPrioritiesByCompanyProductGroupName(
			long companyid, long productid) {

		try { 
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Priority> criteria = cb.createQuery(Priority.class);
			Root<Priority> priority = criteria.from(Priority.class);
			criteria.select(priority).where(cb.equal(priority.get("company").get("companyID"), companyid), cb.equal(priority.get("product").get("productID"), productid));
			List<Priority> resultList = em.createQuery(criteria).getResultList();
			if(resultList != null && resultList.size() > 0)
				return resultList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Priority findPriorityById(long priorityid) {
		return em.find(Priority.class, priorityid);
	}
	public List<SimpleSite> findSiteByHierarchyRootSearchTerm(List<Long> hierarchyIDs, String searchTerm) {
		List<SimpleSite> lFilteredSiteList = new ArrayList<SimpleSite>();
		
		try {
			if(hierarchyIDs != null && hierarchyIDs.size() > 0)
			{
				for(Long id : hierarchyIDs)
				{
					List<SimpleSite> lSites = findSitesByHierarchyId(id);
					for(SimpleSite lSite : lSites)
					{
						if(lSite.getSiteName().contains(searchTerm))
							lFilteredSiteList.add(lSite);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lFilteredSiteList;
	}

	public List<SimpleSite> findSiteByUserGeoGroupsSearchTerm(Set<HierarchyValue> userGeoGroups, String searchTerm) {
		List<SimpleSite> lFilteredSiteList = new ArrayList<SimpleSite>();
		
		try {
			if(userGeoGroups != null && userGeoGroups.size() > 0)
			{
				for(HierarchyValue h : userGeoGroups)
				{
					List<SimpleSite> lSites = findSitesByHierarchyId(h.getHierarchyID());
					for(SimpleSite lSite : lSites)
					{
						if(lSite.getSiteName().contains(searchTerm))
							lFilteredSiteList.add(lSite);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lFilteredSiteList;
	}
	
	public GroupType findHistoryTypeById(long historyTypeId)
	{
		return em.find(GroupType.class, historyTypeId);
	}
	
	public List<GroupType> findHistoryTypeByCompanyProduct(long companyid, long productid)
	{
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<GroupType> criteria = cb.createQuery(GroupType.class);
			Root<GroupType> groupType = criteria.from(GroupType.class);
			criteria.select(groupType).where(cb.equal(groupType.get("groupType"), "history"), cb.equal(groupType.get("company").get("companyID"), companyid), cb.equal(groupType.get("product").get("productID"), productid));
			List<GroupType> resultList = em.createQuery(criteria).getResultList();
			if(resultList != null && resultList.size() > 0)
				return resultList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public List<GroupType> findHistoryTypeByCompanyProduct(Company company, Product product)
	{
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<GroupType> criteria = cb.createQuery(GroupType.class);
			Root<GroupType> groupType = criteria.from(GroupType.class);
			criteria.select(groupType).where(cb.equal(groupType.get("groupType"), "history"), cb.equal(groupType.get("company").get("companyID"), company.getCompanyID()), cb.equal(groupType.get("product").get("productID"), product.getProductID()));
			List<GroupType> resultList = em.createQuery(criteria).getResultList();
			if(resultList != null && resultList.size() > 0)
				return resultList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public GroupType findHistoryTypeByCompanyProductGroupName(long companyid, long productid, String groupName)
	{
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<GroupType> criteria = cb.createQuery(GroupType.class);
			Root<GroupType> groupType = criteria.from(GroupType.class);
			criteria.select(groupType).where(cb.equal(groupType.get("groupType"), "history"), cb.equal(groupType.get("company").get("companyID"), companyid), 
					cb.equal(groupType.get("product").get("productID"), productid), cb.equal(groupType.get("groupName"), groupName));
			List<GroupType> resultList = em.createQuery(criteria).getResultList();
			if(resultList != null && resultList.size() > 0)
				return resultList.get(0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public GroupType findHistoryTypeByCompanyProductGroupName(Company company, Product product, String groupName)
	{
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<GroupType> criteria = cb.createQuery(GroupType.class);
			Root<GroupType> groupType = criteria.from(GroupType.class);
			criteria.select(groupType).where(cb.equal(groupType.get("groupType"), "history"), cb.equal(groupType.get("company").get("companyID"), company.getCompanyID()), 
					cb.equal(groupType.get("product").get("productID"), product.getProductID()), cb.equal(groupType.get("groupName"), groupName));
			List<GroupType> resultList = em.createQuery(criteria).getResultList();
			if(resultList != null && resultList.size() > 0)
				return resultList.get(0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}