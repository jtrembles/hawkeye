package com.fnet.hawkeye.data;

import java.io.Serializable;

import javax.inject.Inject;

import com.fnet.hawkeye.model.WorkPackage;
import com.fnet.hawkeye.model.WorkPackagePlusScore;

public class WorkPackageScoreLoader implements Runnable,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6721008866170168003L;

	@Inject
	private WorkpackageRepository workPackageRepository;
	
	 private final WorkPackage workpackage;
	 WorkPackageScoreLoader(WorkPackage _package) {
		 workpackage = _package;
	 }
	    @Override
	    public void run() {
	        System.out.println("Performing Score Lookup");  
	        new WorkPackagePlusScore(workpackage, workPackageRepository.findScoreByWorkPackageID(workpackage.getWorkPackageID().intValue()).get(0));
	    }
}
