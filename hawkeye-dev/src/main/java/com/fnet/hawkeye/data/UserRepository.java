/**
 * JBoss, Home of Professional Open Source
 * Copyright 2012, Red Hat, Inc., and individual contributors
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fnet.hawkeye.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.fnet.hawkeye.model.GroupType;
import com.fnet.hawkeye.model.HierarchyIdHolder;
import com.fnet.hawkeye.model.HierarchyValue;
import com.fnet.hawkeye.model.IdHolder;
import com.fnet.hawkeye.model.SafeUser;
import com.fnet.hawkeye.model.SafeUserFirstNameComparator;
import com.fnet.hawkeye.model.SimpleSite;
import com.fnet.hawkeye.model.User;

@ApplicationScoped
public class UserRepository {

    @Inject
    private EntityManager em;

    public User findByEmailAndPassword(String email, String password) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> criteria = cb.createQuery(User.class);
        Root<User> member = criteria.from(User.class);
        // Swap criteria statements if you would like to try out type-safe criteria queries, a new
        // feature in JPA 2.0
        // criteria.select(member).where(cb.equal(member.get(Member_.name), email));
        criteria.select(member).where(cb.equal(member.get("userEmail"), email), cb.equal(member.get("password"), password));
        return em.createQuery(criteria).getSingleResult();
    }

    public List<SafeUser> findByCompanyProduct(Long companyid, Long productid) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SafeUser> criteria = cb.createQuery(SafeUser.class);
        Root<SafeUser> member = criteria.from(SafeUser.class);
        // Swap criteria statements if you would like to try out type-safe criteria queries, a new
        // feature in JPA 2.0
        // criteria.select(member).where(cb.equal(member.get(Member_.name), email));
        criteria.select(member).where(cb.equal(member.get("company").get("companyID"), companyid), cb.equal(member.get("product").get("productID"), productid), cb.equal(member.get("active"), true));
        return em.createQuery(criteria).getResultList();
    
    }

    public List<SafeUser> findByCompanyProductUserGeo(User requestor) {		
		try {
			HashMap<Long, SafeUser> resultList = new HashMap<Long, SafeUser>();
			Session session = em.unwrap(org.hibernate.Session.class);
			for(HierarchyValue lUserGeo : requestor.getGeoGroupHierarchyValues())
			{
				Query query = session.createSQLQuery("CALL findAllUsersInGeoArea(:geoHierarchyId)")
						.setResultTransformer(Transformers.aliasToBean(IdHolder.class))
						.setParameter("geoHierarchyId", lUserGeo.getHierarchyID());
				List<IdHolder> result = query.list();
				
				List<Long> lUserIdList = new ArrayList<Long>();
				for(IdHolder n : result)
				{
					lUserIdList.add(new Long(n.getFoundID().longValue()));
				}
				
				CriteriaBuilder cb = em.getCriteriaBuilder();
				CriteriaQuery<SafeUser> criteria = cb.createQuery(SafeUser.class);
				Root<SafeUser> user = criteria.from(SafeUser.class);
				criteria.select(user).where(user.get("userID").in(lUserIdList), cb.equal(user.get("active"), true)).distinct(true);//.orderBy(cb.asc(user.get("firstName")));
				
				List<SafeUser> lTmp = em.createQuery(criteria).getResultList();
				for(SafeUser lUser : lTmp)
				{
					resultList.put(lUser.getUserID(), lUser);
				}
			}
			
			if(resultList.size() > 0)
			{
				List<SafeUser> lSortedList = new ArrayList<SafeUser>(resultList.values());
				Collections.sort(lSortedList, new SafeUserFirstNameComparator());
				return lSortedList;
			}			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
    }

    public User findByCompanyProductUser(Long companyid, Long productid, Long userid) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> criteria = cb.createQuery(User.class);
        Root<User> member = criteria.from(User.class);
        // Swap criteria statements if you would like to try out type-safe criteria queries, a new
        // feature in JPA 2.0
        // criteria.select(member).where(cb.equal(member.get(Member_.name), email));
        criteria.select(member).where(cb.equal(member.get("company").get("companyID"), companyid), cb.equal(member.get("product").get("productID"), productid),
        		cb.equal(member.get("userID"), userid));
        return em.createQuery(criteria).getSingleResult();
    
    }
    
    public User findById(Long id) {
        return em.find(User.class, id);
    }
    
    public User findByApiKey(String apiKey)
    {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> criteria = cb.createQuery(User.class);
        Root<User> member = criteria.from(User.class);
    	
    	criteria.select(member).where(cb.equal(member.get("apikey"), apiKey));
    	
    	List<User> lFoundUserList = null;
   		lFoundUserList = em.createQuery(criteria).getResultList();

   		if(lFoundUserList != null && lFoundUserList.size() > 0)
   			return lFoundUserList.get(0);
   		
   		return null;
    }
    
    public User findByFirstLastName(String first, String last, Long companyID, Long productID)
    {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> criteria = cb.createQuery(User.class);
        Root<User> member = criteria.from(User.class);
        Expression<String> f = member.get("firstName");
        Expression<String> l = member.get("lastName");
    	
    	criteria.select(member).where(
    		cb.equal(
    			cb.lower(f), first.toLowerCase()
    		), cb.equal(
        			cb.lower(l), last.toLowerCase()
    	    ), cb.equal(member.get("company").get("companyID"), companyID),
    	    cb.equal(member.get("product").get("productID"), productID)
    	);
    	
    	List<User> lFoundUserList = null;
   		lFoundUserList = em.createQuery(criteria).getResultList();

   		if(lFoundUserList != null && lFoundUserList.size() > 0)
   			return lFoundUserList.get(0);
   		
   		return null;
    }

    public User findByEmail(String email) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> criteria = cb.createQuery(User.class);
        Root<User> member = criteria.from(User.class);
        // Swap criteria statements if you would like to try out type-safe criteria queries, a new
        // feature in JPA 2.0
        criteria.select(member).where(cb.equal(member.get("userEmail"), email));
        return em.createQuery(criteria).getSingleResult();
    }
    
    public List<User> findAllOrderedByName() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> criteria = cb.createQuery(User.class);
        Root<User> member = criteria.from(User.class);
        // Swap criteria statements if you would like to try out type-safe criteria queries, a new
        // feature in JPA 2.0
        // criteria.select(member).orderBy(cb.asc(member.get(Member_.name)));
        criteria.select(member).orderBy(cb.asc(member.get("lastName")));
        
        List<User> users = em.createQuery(criteria).getResultList();
        return users;
    }

    public void logout(long userid){
    	try {
			User user = em.find(User.class, userid);
			user.setApikey("unavailable");
			em.merge(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    public Boolean validateAPIKey(long userid, String apikey){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> criteria = cb.createQuery(User.class);
        Root<User> member = criteria.from(User.class);
        // Swap criteria statements if you would like to try out type-safe criteria queries, a new
        // feature in JPA 2.0
        // criteria.select(member).orderBy(cb.asc(member.get(Member_.name)));
        criteria.select(member).where(cb.equal(member.get("userID"), userid), cb.equal(member.get("apikey"), apikey));

        List<User> users = em.createQuery(criteria).getResultList();
        if (users.size() > 0)
        	return true;
        else
        	return false;
    }
    
    public String generateAPIKey() {
    	return UUID.randomUUID().toString();
    }
    
    public void updateUser(User user) throws IllegalArgumentException {
    	em.merge(user);
    }

	public List<GroupType> findJobTypeByCompanyProductUser(long companyid, long productid, long id) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		
		ArrayList<GroupType> groups = new ArrayList<GroupType>();
		User usr = findByCompanyProductUser(companyid, productid, id);
		if (usr.getActive()){
	        CriteriaQuery<GroupType> criteria = cb.createQuery(GroupType.class);
	        Root<GroupType> group = criteria.from(GroupType.class);
	        // Swap criteria statements if you would like to try out type-safe criteria queries, a new
	        // feature in JPA 2.0
	        // criteria.select(member).where(cb.equal(member.get(Member_.name), email));
	        groups.addAll(groups);
	        return groups;
		}
		return null;
	}

	public SafeUser findSafeUserByApiKey(String apiKey) {
		   
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<SafeUser> criteria = cb.createQuery(SafeUser.class);
		Root<SafeUser> member = criteria.from(SafeUser.class);

		criteria.select(member).where(cb.equal(member.get("apikey"), apiKey));

		List<SafeUser> lFoundUserList = null;
		lFoundUserList = em.createQuery(criteria).getResultList();

		if(lFoundUserList != null && lFoundUserList.size() > 0)
			return lFoundUserList.get(0);
		return null;
	}
	
//	public List<UserGeoGroup> getUserGeoGroups(long id)
//	{
//        CriteriaBuilder cb = em.getCriteriaBuilder();
//        CriteriaQuery<UserGeoGroup> criteria = cb.createQuery(UserGeoGroup.class);
//        Root<UserGeoGroup> member = criteria.from(UserGeoGroup.class);
//        // Swap criteria statements if you would like to try out type-safe criteria queries, a new
//        // feature in JPA 2.0
//        // criteria.select(member).orderBy(cb.asc(member.get(Member_.name)));
//        criteria.select(member).where(cb.equal(member.get("userGeoGroupPK").get("userID"), id));
//        
//        List<UserGeoGroup> userGeoGroups = em.createQuery(criteria).getResultList();
//        return userGeoGroups;
//		
//	}
//	
//	public List<Long> getUserGeoGroupHierarchyIDs(long id)
//	{
//        List<UserGeoGroup> userGeoGroups = getUserGeoGroups(id);
//        
//        List<Long> lHierarchyIdList = new ArrayList<Long>();
//        for(UserGeoGroup ugg : userGeoGroups)
//        {
//        	lHierarchyIdList.add(ugg.getUserGeoGroupPK().getHierarchyID());
//        }
//        return lHierarchyIdList;
//		
//	}
}
